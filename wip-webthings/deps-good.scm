;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings deps-good)
  #:use-module (gnu packages)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages python-web)
  #:use-module ((gnu packages web)
                #:select ((esbuild . upstream:esbuild)))
  #:use-module (srfi srfi-1)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:))

;;;
;;; Python.
;;;

(define-public python-singleton-decorator
  (package
    (name "python-singleton-decorator")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "singleton-decorator" version))
              (sha256
               (base32
                "01zf7ghcbqbv48lc3cf63cys9m65fzbgsry1r68yb2vkia5av40s"))))
    (build-system python-build-system)
    (home-page "https://github.com/Kemaweyan/singleton_decorator")
    (synopsis "Create singleton objects that support easy unit testing")
    (description "A testable @code{@@singleton} decorator allows you easily to
create singleton objects but also easily to write unit tests for those
classes.  It uses a separate wrapper object for each decorated class and holds
the decorated class within the @code{__wrapped__} attribute so you can access
it directly in your unit tests.")
    (license (list license:gpl3))))

(define-public python-websocket-client-0.57
  (package
    (inherit (strip-python2-variant python-websocket-client))
    (version "0.57.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "websocket_client" version))
       (sha256
        (base32
         "04108mpz6yjcvjwinmkg6mrn6pwf4ghcka7jh6hsd4hndlfvjdfp"))))
    (arguments `())))

;;;
;;; esbuild
;;;

(define-public esbuild
  ;; See also node-esbuild, below.
  ;; One reason to have two packages, rather than multiple outputs,
  ;; is that we use esbuild in the bootstrapping process for Node.js itself.
  (package
    (inherit upstream:esbuild)
    (version "0.14.1")
    (source
     (origin
       (inherit (package-source upstream:esbuild))
       (uri (git-reference
             (inherit (origin-uri (package-source upstream:esbuild)))
             (commit (string-append "v" version))))
       (file-name (git-file-name "esbuild" version))
       (sha256
        (base32 "0i621vfncvj6cvlk183073nys9qmnvvy3a3r8zp0ahwhgqk84l12"))
       (snippet
        ;; Remove prebuilt binaries, but keep the package skeletons.
        ;; (There don't seem to be prebuilt binaries in the origin
        ;; any more, but perhaps they might come back some day.)
        #~(for-each delete-file-recursively
                    (find-files "npm" "^bin$" #:directories? #t)))))
    (description
     "The @code{esbuild} tool provides a unified bundler, transpiler and
minifier.  It packages up JavaScript and TypeScript code, along with JSON
and other data, for distribution on the web.

This package provides the @code{esbuild} command-line tool.  For more
complex use cases and plugin support, consider @code{node-esbuild}.")))

(define-public node-esbuild
  (package
    (name "node-esbuild")
    (version (package-version esbuild))
    (source (package-source esbuild))
    (inputs (list esbuild))
    (native-inputs (list esbuild))
    (build-system node-build-system)
    (arguments
     `(#:modules
       ((guix build node-build-system)
        (guix build json)
        (srfi srfi-1)
        (srfi srfi-26)
        (ice-9 match)
        (ice-9 textual-ports)
        (guix build utils))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'prepare-npm-src
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (let* ((->esbuild (cut search-input-file <> "bin/esbuild"))
                    (esbuild (->esbuild (or native-inputs inputs)))
                    (esbuild-for-target (->esbuild inputs)))
               ;; adapted from scripts/esbuild.js
               (mkdir-p "npm/esbuild/bin")
               (mkdir-p "npm/esbuild/lib")
               (substitute* "lib/npm/node-platform.ts"
                 (("env\\.ESBUILD_BINARY_PATH [|][|] ESBUILD_BINARY_PATH")
                  (string-append "env.ESBUILD_BINARY_PATH || '"
                                 esbuild-for-target
                                 "'")))
               ;; Skip "npm/esbuild/install.js"
               ;; Generate "npm/esbuild/lib/main.js"
               (invoke esbuild
                       "lib/npm/node.ts"
                       "--outfile=npm/esbuild/lib/main.js"
                       "--bundle"
                       "--target=node10"
                       "--define:WASM=false"
                       (format #f "--define:ESBUILD_VERSION='~a'"
                               (string-trim-both
                                (call-with-ascii-input-file
                                 "version.txt" get-string-all)))
                       "--external:esbuild"
                       "--platform=node"
                       "--log-level=warning")
               ;; Generate "npm/esbuild/bin/esbuild"
               (invoke esbuild
                       "lib/npm/node-shim.ts"
                       "--outfile=npm/esbuild/bin/esbuild"
                       "--bundle"
                       "--target=node10"
                       "--external:esbuild"
                       "--platform=node"
                       "--log-level=warning")
               ;; Generate "npm/esbuild/lib/main.d.ts"
               (copy-file "lib/shared/types.ts" "npm/esbuild/lib/main.d.ts")
               ;; go to the package directory
               (chdir "npm/esbuild")
               (with-atomic-json-file-replacement "package.json"
                 (match-lambda
                   (('@ . alist)
                    (cons '@ (filter
                              (match-lambda
                                ;; optionalDependencies used via scripts
                                ;; to install pre-built binaries
                                (((or "scripts" "optionalDependencies") . _)
                                 #f)
                                (_
                                 #t))
                              alist)))))))))))
    (home-page (package-home-page esbuild))
    (synopsis "JavaScript API for @code{esbuild}")
    (description "The JavaScript wrapper for @code{esbuild} facilitates
writing complex scripts and adds support for plugins, which may be written in
JavaScript or Go.  This is the officially recomended way to install
@code{esbuild}.")
    ;; See: https://esbuild.github.io/getting-started/#other-ways-to-install
    (license (package-license esbuild))))

;;;
;;; Node.
;;;

(define-public node-mkdirp
  (package
    (name "node-mkdirp")
    (version "1.0.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/isaacs/node-mkdirp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c9zmgnhldrrwim644qjlrfw4hcdvb6b2bawyhqh649gxpnkzb5m"))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("tap"
                                    "require-inject")))))
       #:tests? #f))
    (home-page "https://github.com/isaacs/node-mkdirp")
    (synopsis "Like @code{mkdir -p}, but in Node.js")
    (description "Like mkdir -p, but in Node.js!")
    (license license:expat)))

(define-public node-ws
  (package
    (name "node-ws")
    (version "8.3.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/websockets/ws")
             (commit version)))
       (sha256
        (base32 "15x2y463wzf3h79cfn7n5z9sxh7mwmcc6xrr52dhhrans3akfk0h"))
       (file-name (git-file-name name version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies
              `("benchmark"
                "eslint"
                "eslint-config-prettier"
                "eslint-plugin-prettier"
                "mocha"
                "nyc"
                "prettier"
                ;; TODO: enable bufferutil and/or utf-8-validate addons
                ;; for increased performance
                ;; https://www.npmjs.com/package/ws#opt-in-for-performance
                "bufferutil"
                "utf-8-validate")))))
       #:tests? #f))
    (home-page "https://github.com/websockets/ws")
    (synopsis
     "WebSocket client and server for Node.js")
    (description
     "Simple to use, blazing fast and thoroughly tested websocket client and server for Node.js")
    (license license:expat)))

(define-public node-punycode
  (package
    (name "node-punycode")
    (version "2.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mathiasbynens/punycode.js")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0blc6rdhwhiy9p5gvm26gkzkga73gmyd61kf3f6534wyacb4bisn"))
       (file-name (git-file-name name version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("codecov"
                                    "istanbul"
                                    "mocha")))))
       #:tests? #f))
    (home-page "https://mths.be/punycode")
    (synopsis
     "Robust, portable Punycode converter in JavaScript")
    (description
     "This package provides a robust Punycode converter that fully complies
to RFC 3492 and RFC 5891, and works on nearly all JavaScript platforms.")
    (license license:expat)))

(define-public node-json-schema-traverse
  (package
    (name "node-json-schema-traverse")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/epoberezkin/json-schema-traverse")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1dng1g8lh2gd2m0n5bz8zsmm01gzp0km05izc8my39f2ym1218y9"))
       (file-name (git-file-name name version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("eslint"
                                    "mocha"
                                    "nyc"
                                    "pre-commit")))))
       #:tests? #f))
    (home-page "https://github.com/epoberezkin/json-schema-traverse#readme")
    (synopsis "Traverse JSON Schema passing each schema object to callback")
    (description "Traverse JSON Schema passing each schema object to callback")
    (license license:expat)))

(define-public node-require-from-string
  (package
    (name "node-require-from-string")
    (version "2.0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/floatdrop/require-from-string")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1xlz07l7rk7zfalppmcs15b4lq7im8r6aidlksllj6hbs77rxlnb"))
       (file-name (git-file-name name version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("mocha")))))
       #:tests? #f))
    (home-page "https://github.com/floatdrop/require-from-string")
    (synopsis "Require module from string")
    (description "Require module from string")
    (license license:expat)))

(define-public node-uri-js
  (package
    (name "node-uri-js")
    (version "4.4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/garycourt/uri-js")
             (commit
              ;; Unlike other releases, 4.4.1 is not (yet?) tagged:
              ;; see <https://github.com/garycourt/uri-js/issues/68>.
              "9a328873a21262651c3790505b24c9e318a0e12d")))
       (sha256
        (base32 "15gsmvmw436a19rwgliz27zv77aqr1cfdmc41rifgz4lvf1nk0lq"))
       (file-name (git-file-name name version))))
    (native-inputs
     (list esbuild))
    (inputs
     (list node-punycode))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("babel-cli"
                                    "babel-plugin-external-helpers"
                                    "babel-preset-latest"
                                    "mocha"
                                    "mocha-qunit-ui"
                                    "rollup"
                                    "rollup-plugin-babel"
                                    "rollup-plugin-node-resolve"
                                    "sorcery"
                                    "typescript"
                                    "uglify-js"))))
         (add-after 'unpack 'unbundle-pre-build
           (lambda args
             ;; Because we don't have `tsc`, we need to make some
             ;; compromises. Currently, we delete all of the pre-built
             ;; files, and we build only "dist/es5/uri.all.js" (and the
             ;; corresponding ".map"), which is the "main" entry in
             ;; "package.json" and enough for all known clients.
             ;; It isn't clear that the esnext files would be recognized
             ;; as ES6 modules on Node.js, anyway, but they could certainly
             ;; be built, and "package.json"'s "main" could be patched.
             ;; It wouldn't be too horrible to keep the ".d.ts" files,
             ;; since don't change the generated code, but, presumably,
             ;; by the time we need them we'll be able to generate them.
             ;; Building "uri.all.min.js" seems unlikely to be useful.
             (delete-file-recursively "dist")))
         (replace 'build
           (lambda* (#:key native-inputs inputs #:allow-other-keys)
             (mkdir-p "dist/es5")
             (invoke (string-append
                      (assoc-ref (or native-inputs inputs) "esbuild")
                      "/bin/esbuild")
                     "--bundle"
                     "--platform=node"
                     "--format=cjs"
                     "--target=node10" ;; better supported than esbuild's es5
                     "--external:punycode"
                     "--sourcemap"
                     "src/index.ts"
                     "--outfile=dist/es5/uri.all.js"))))
       #:tests? #f))
    (home-page "https://github.com/garycourt/uri-js")
    (synopsis
     "URI/IRI parsing/validating/resolving library for JavaScript")
    (description
     "An RFC 3986/3987 compliant, scheme extendable URI/IRI
parsing/validating/resolving library for JavaScript.")
    (license license:bsd-2)))

(define-public node-dot
  (package
    (name "node-dot")
    (version "1.1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/olado/doT")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1443rrgw212zvszdsnxmbrw3kal28j3nc27x6dqwqjyw8qqsgfx2"))
       (file-name (git-file-name name version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies
              ;; only devDependencies
              `("commander"
                "coveralls"
                "eslint"
                "if-node-version"
                "jshint"
                "mkdirp"
                "mocha"
                "nyc"
                "pre-commit"
                "uglify-js")))))
       #:tests? #f))
    (home-page "https://github.com/olado/doT")
    (synopsis "Concise and fast javascript templating")
    (description "Created in search of the fastest and most concise JavaScript
templating function with emphasis on performance under V8 and Node.js.  It
shows great performance for both Node.js and browsers.  @code{doT} is fast,
small and has no dependencies.")
    (license license:expat)))

(define-public node-fast-deep-equal
  (package
    (name "node-fast-deep-equal")
    (version "3.1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/epoberezkin/fast-deep-equal")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0f9mg5l3xzh0a64cajy7wfi541yxr9h4kil45i68isrm8bjzdh6n"))
       (file-name (git-file-name name version))))
    (inputs
     (list node-dot))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("coveralls"
                                    "eslint"
                                    "mocha"
                                    "nyc"
                                    "pre-commit"
                                    "react"
                                    "react-test-renderer"
                                    "sinon"
                                    "typescript")))))
       #:tests? #f))
    (home-page "https://github.com/epoberezkin/fast-deep-equal")
    (synopsis "Fast deep equal")
    (description "Fast deep equal")
    (license license:expat)))

(define-public node-ajv-validator-config
  (package
    (name "node-ajv-validator-config")
    (version "0.3.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ajv-validator/config")
             (commit (string-append "v" version))))
       (sha256
        (base32 "02s8brb9xmfi22r05ww232xihna7xwkj5ar70fd77n7yh64kkykf"))
       (file-name (git-file-name "v" version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("@typescript-eslint/eslint-plugin"
                                    "@typescript-eslint/parser"
                                    "eslint"
                                    "eslint-config-prettier"
                                    "husky"
                                    "lint-staged"
                                    "prettier"
                                    "typescript")))))
       #:tests? #f))
    (home-page "https://github.com/ajv-validator/config")
    (synopsis "Shared configuration for @code{node-ajv}")
    (description "Shared typescript, eslint, and prettier configuration for
@code{node-ajv}.")
    (license license:expat)))

(define-public node-ajv
  ;; This is "good" except for the significant fact that
  ;; it doesn't actually work
  (package
    (name "node-ajv")
    (version "8.8.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ajv-validator/ajv")
             (commit (string-append "v" version))))
       (sha256
        (base32 "11pljc3xbjak911nn924bgyml8dzk2l5ix6a5nzpkjbfdqwvgjbc"))
       (file-name (git-file-name "v" version))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("@rollup/plugin-commonjs"
                                    "@rollup/plugin-json"
                                    "@rollup/plugin-node-resolve"
                                    "@rollup/plugin-typescript"
                                    "@types/chai"
                                    "@types/mocha"
                                    "@types/node"
                                    "@types/require-from-string"
                                    "@typescript-eslint/eslint-plugin"
                                    "@typescript-eslint/parser"
                                    "ajv-formats"
                                    "browserify"
                                    "chai"
                                    "cross-env"
                                    "dayjs"
                                    "dayjs-plugin-utc"
                                    "eslint"
                                    "eslint-config-prettier"
                                    "glob"
                                    "husky"
                                    "if-node-version"
                                    "jimp"
                                    "js-beautify"
                                    "json-schema-test"
                                    "karma"
                                    "karma-chrome-launcher"
                                    "karma-mocha"
                                    "lint-staged"
                                    "mocha"
                                    "node-fetch"
                                    "nyc"
                                    "prettier"
                                    "re2"
                                    "rollup"
                                    "rollup-plugin-terser"
                                    "ts-node"
                                    "tsify"
                                    "typescript"))))
         (replace 'build
           (lambda* (#:key native-inputs inputs #:allow-other-keys)
             ;; workarounds for esModuleInterop and
             ;; lack of isolatedModules
             (substitute* "lib/compile/resolve.ts"
               (("import \\* as traverse")
                "import traverse")
               (("import \\* as equal")
                "import equal"))
             (substitute* "lib/standalone/instance.ts"
               (("import \\* as requireFromString")
                "import requireFromString"))
             (mkdir-p "dist")
             (apply invoke
                    (search-input-file (or native-inputs inputs) "bin/esbuild")
                    ;; see in particular @ajv-validator/config
                    "--platform=node"
                    "--format=cjs"
                    "--target=es2018"
                    "--sourcemap"
                    "--outdir=dist"
                    (find-files "lib" "\\.(ts|js|json)$"))
             (copy-recursively "lib/refs" "dist/refs")
             (for-each delete-file
                       `("dist/refs/json-schema-2019-09/index.ts"
                         "dist/refs/json-schema-2020-12/index.ts"
                         "dist/refs/jtd-schema.ts"))
             #t)))
       #:tests? #f))
    (native-inputs
     (list esbuild))
    (inputs
     (list node-ajv-validator-config node-uri-js node-require-from-string
           node-json-schema-traverse node-fast-deep-equal))
    (home-page "https://ajv.js.org")
    (synopsis "Another JSON Schema Validator")
    (description "Another JSON Schema Validator")
    (license license:expat)))
