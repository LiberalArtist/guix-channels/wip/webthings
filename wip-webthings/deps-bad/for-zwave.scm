;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings deps-bad for-zwave)
  #:use-module (gnu packages)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (srfi srfi-1)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (wip-webthings deps-good)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public node-color-convert
  (package
    (name "node-color-convert")
    (version "2.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/color-convert/-/color-convert-2.0.1.tgz")
       (sha256
        (base32 "1qbw9rwfzcp7y0cpa8gmwlj7ccycf9pwn15zvf2s06f070ss83wj"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'build))))
    (inputs (list node-color-name))
    (home-page "https://github.com/Qix-/color-convert#readme")
    (synopsis "Plain color conversion functions")
    (description "Plain color conversion functions")
    (license license:expat)))


(define-public node-is-arrayish
  (package
    (name "node-is-arrayish")
    (version "0.3.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/is-arrayish/-/is-arrayish-0.3.2.tgz")
       (sha256
        (base32 "0cpajzzj5d2f69hbmvyk1aw1ajafi4724hx33lm6a6argr4nxmh1"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'build))))
    (home-page "https://github.com/qix-/node-is-arrayish")
    (synopsis "Determines if an object can be used as an array")
    (description "Determines if an object can be used as an array")
    (license license:expat)))

(define-public node-simple-swizzle
  (package
    (name "node-simple-swizzle")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/simple-swizzle/-/simple-swizzle-0.2.2.tgz")
       (sha256
        (base32 "0gv8qfgsh62r76kbdzg7r10f353n00qqh8hd9rm611xbvgrlazsw"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'build))))
    (inputs (list node-is-arrayish))
    (home-page "https://github.com/qix-/node-simple-swizzle#readme")
    (synopsis "Simply swizzle your arguments")
    (description "Simply swizzle your arguments")
    (license license:expat)))

(define-public node-color-string
  (package
    (name "node-color-string")
    (version "1.6.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/color-string/-/color-string-1.6.0.tgz")
       (sha256
        (base32 "1xxq5gvm9xd08n9rpgrkp33ml1fgk3wbq5ihi79q2q9mj8dmjhsd"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-simple-swizzle node-color-name))
    (home-page "https://github.com/Qix-/color-string#readme")
    (synopsis "Parser and generator for CSS color strings")
    (description "Parser and generator for CSS color strings")
    (license license:expat)))

(define-public node-color
  (package
    (name "node-color")
    (version "4.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/color/-/color-4.0.1.tgz")
       (sha256
        (base32 "10429sj7lsnjyn6nvbh5l4br5x01wxwq8is923anjkg9baj4l2l3"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'build))))
    (inputs
     (list node-color-string node-color-convert))
    (home-page "https://github.com/Qix-/color#readme")
    (synopsis "Color conversion and manipulation with CSS string support")
    (description "Color conversion and manipulation with CSS string support")
    (license license:expat)))
