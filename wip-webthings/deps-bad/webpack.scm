;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings deps-bad webpack)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages zwave)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages web)
  #:use-module (wip-webthings deps-good)
  #:use-module (wip-webthings deps-bad for-zwave)
  #:use-module (wip-webthings deps-bad for-gateway)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public node-types-eslint
  (package
    (name "node-types-eslint")
    (version "8.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@types/eslint/-/eslint-8.2.0.tgz")
       (sha256
        (base32 "1az9pzkdv55n83h3d1yjsz10c2jchib0hzzjf66c2ljnzl8bs754"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-types-json-schema node-types-estree))
    (home-page
     "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/eslint")
    (synopsis "TypeScript definitions for eslint")
    (description "TypeScript definitions for eslint")
    (license license:expat)))

(define-public node-types-eslint-scope
  (package
    (name "node-types-eslint-scope")
    (version "3.7.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@types/eslint-scope/-/eslint-scope-3.7.1.tgz")
       (sha256
        (base32 "0my95hhmzqksbs815c6cz52ckfx9visz4g5z37ip5x9cz0byb93g"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-types-estree node-types-eslint))
    (home-page
     "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/eslint-scope")
    (synopsis "TypeScript definitions for eslint-scope")
    (description "TypeScript definitions for eslint-scope")
    (license license:expat)))

(define-public node-types-estree
  (package
    (name "node-types-estree")
    (version "0.0.50")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@types/estree/-/estree-0.0.50.tgz")
       (sha256
        (base32 "1xyr6kbq2jmgv4syxxb15dlplbv9kh4kq8w5yrlcdjg7yicqd2b4"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/estree")
    (synopsis "TypeScript definitions for ESTree AST specification")
    (description "TypeScript definitions for ESTree AST specification")
    (license license:expat)))

(define-public node-webassemblyjs-helper-wasm-section
  (package
    (name "node-webassemblyjs-helper-wasm-section")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/helper-wasm-section/-/helper-wasm-section-1.11.1.tgz")
       (sha256
        (base32 "18qbzfs68xrfdah5yvhdn2yal0gfplkz0ya2pv9zy3jnmv4zyw9q"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webassemblyjs-wasm-gen
           node-webassemblyjs-helper-wasm-bytecode
           node-webassemblyjs-helper-buffer node-webassemblyjs-ast))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public node-webassemblyjs-helper-buffer
  (package
    (name "node-webassemblyjs-helper-buffer")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/helper-buffer/-/helper-buffer-1.11.1.tgz")
       (sha256
        (base32 "1bk39gqdg7nm8qlc2z2a2pdvpx4lzaxf9h2n3nl813x5xx82nqlp"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "Buffer manipulation utility")
    (description "Buffer manipulation utility")
    (license license:expat)))

(define-public node-webassemblyjs-wasm-gen
  (package
    (name "node-webassemblyjs-wasm-gen")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/wasm-gen/-/wasm-gen-1.11.1.tgz")
       (sha256
        (base32 "0fksbfl9g3p33sjyb426v5c163k03kjfix70ak9xmj2vm5ard2ci"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webassemblyjs-utf8 node-webassemblyjs-leb128
           node-webassemblyjs-ieee754 node-webassemblyjs-helper-wasm-bytecode
           node-webassemblyjs-ast))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "WebAssembly binary format printer")
    (description "WebAssembly binary format printer")
    (license license:expat)))

(define-public node-webassemblyjs-wasm-opt
  (package
    (name "node-webassemblyjs-wasm-opt")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/wasm-opt/-/wasm-opt-1.11.1.tgz")
       (sha256
        (base32 "15y5fpiy3f3wmv69723qrh8k06r3ihaxl3rch73m8qzpy1asggzl"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webassemblyjs-wasm-parser node-webassemblyjs-wasm-gen
           node-webassemblyjs-helper-buffer node-webassemblyjs-ast))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public node-webassemblyjs-wast-printer
  (package
    (name "node-webassemblyjs-wast-printer")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/wast-printer/-/wast-printer-1.11.1.tgz")
       (sha256
        (base32 "1vjdviahh73wwcy2dki8xpkmx4m8y99hj08lpnv38lkw3hzmrhnw"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-xtuc-long node-webassemblyjs-ast))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "WebAssembly text format printer")
    (description "WebAssembly text format printer")
    (license license:expat)))

(define-public node-webassemblyjs-wasm-edit
  (package
    (name "node-webassemblyjs-wasm-edit")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/wasm-edit/-/wasm-edit-1.11.1.tgz")
       (sha256
        (base32 "03f49si5anw9g2fi035rxx3lq5kg4w3pkz4irljdi625isk20iax"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webassemblyjs-wast-printer
           node-webassemblyjs-wasm-parser
           node-webassemblyjs-wasm-opt
           node-webassemblyjs-wasm-gen
           node-webassemblyjs-helper-wasm-section
           node-webassemblyjs-helper-wasm-bytecode
           node-webassemblyjs-helper-buffer
           node-webassemblyjs-ast))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "> Rewrite a WASM binary")
    (description "> Rewrite a WASM binary")
    (license license:expat)))

(define-public node-webassemblyjs-floating-point-hex-parser
  (package
    (name "node-webassemblyjs-floating-point-hex-parser")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/floating-point-hex-parser/-/floating-point-hex-parser-1.11.1.tgz")
       (sha256
        (base32 "0hwyw07ivsfvjha6cmb8vilavgff3i0x4h720wpl6xia7m0crsc3"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis
     "A function to parse floating point hexadecimal strings as defined by the WebAssembly specification")
    (description
     "A function to parse floating point hexadecimal strings as defined by the WebAssembly specification")
    (license license:expat)))

(define-public node-webassemblyjs-helper-numbers
  (package
    (name "node-webassemblyjs-helper-numbers")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/helper-numbers/-/helper-numbers-1.11.1.tgz")
       (sha256
        (base32 "03m7wa6nspps1pn5yv9sp31p1ly2vhvhanm73k0kk4rwl56drn68"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-xtuc-long node-webassemblyjs-helper-api-error
           node-webassemblyjs-floating-point-hex-parser))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "Number parsing utility")
    (description "Number parsing utility")
    (license license:expat)))

(define-public node-webassemblyjs-ast
  (package
    (name "node-webassemblyjs-ast")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/ast/-/ast-1.11.1.tgz")
       (sha256
        (base32 "16qy6479a30fj9fv3j74vnqkv37ww9x1i0sfp150jqypl7hzvx2v"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webassemblyjs-helper-wasm-bytecode
           node-webassemblyjs-helper-numbers))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "AST utils for webassemblyjs")
    (description "AST utils for webassemblyjs")
    (license license:expat)))

(define-public node-webassemblyjs-helper-api-error
  (package
    (name "node-webassemblyjs-helper-api-error")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/helper-api-error/-/helper-api-error-1.11.1.tgz")
       (sha256
        (base32 "040lxh6vlcdr00inlk2lxib4n94qlzlm4x5imqwagrq79m2ax98v"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "Common API errors")
    (description "Common API errors")
    (license license:expat)))

(define-public node-webassemblyjs-helper-wasm-bytecode
  (package
    (name "node-webassemblyjs-helper-wasm-bytecode")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/helper-wasm-bytecode/-/helper-wasm-bytecode-1.11.1.tgz")
       (sha256
        (base32 "18ld4d4573n48gk5dk8ha088igykk83kwjhgws0c57ycw3p3sjgc"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "WASM's Bytecode constants")
    (description "WASM's Bytecode constants")
    (license license:expat)))

(define-public node-xtuc-ieee754
  (package
    (name "node-xtuc-ieee754")
    (version "1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@xtuc/ieee754/-/ieee754-1.2.0.tgz")
       (sha256
        (base32 "1v7d8f25im0dw94maw41c87lb4qfrafxyw717k2ahz8rgck1mjs2"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/feross/ieee754#readme")
    (synopsis
     "Read/write IEEE754 floating point numbers from/to a Buffer or array-like object")
    (description
     "Read/write IEEE754 floating point numbers from/to a Buffer or array-like object")
    (license license:bsd-3)))

(define-public node-webassemblyjs-ieee754
  (package
    (name "node-webassemblyjs-ieee754")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/ieee754/-/ieee754-1.11.1.tgz")
       (sha256
        (base32 "082sfczprf2gwz54yhsmwgxd4m0kbrggiamkxkc7ywg9g6fcrcxb"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-xtuc-ieee754))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "IEEE754 decoder and encoder")
    (description "IEEE754 decoder and encoder")
    (license license:expat)))

(define-public node-xtuc-long
  (package
    (name "node-xtuc-long")
    (version "4.2.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@xtuc/long/-/long-4.2.2.tgz")
       (sha256
        (base32 "0q7n92kvf4k3gp6appy7gssjk1hn9j7spnwgxgvqramrn1nd52vw"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/dcodeIO/long.js#readme")
    (synopsis
     "A Long class for representing a 64-bit two's-complement integer value.")
    (description
     "A Long class for representing a 64-bit two's-complement integer value.")
    (license license:asl2.0)))

(define-public node-webassemblyjs-leb128
  (package
    (name "node-webassemblyjs-leb128")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/leb128/-/leb128-1.11.1.tgz")
       (sha256
        (base32 "1z3sh5s001dwibwws164yq65p3qdsnkf13450rgxif44ddyw8x9d"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-xtuc-long))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "LEB128 decoder and encoder")
    (description "LEB128 decoder and encoder")
    (license license:asl2.0)))

(define-public node-webassemblyjs-utf8
  (package
    (name "node-webassemblyjs-utf8")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/utf8/-/utf8-1.11.1.tgz")
       (sha256
        (base32 "0s7nl9r0lvsabd3dxwf5q2v8hv9jk19bhfw4qfv4rr5m460kzyla"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "UTF8 encoder/decoder for WASM")
    (description "UTF8 encoder/decoder for WASM")
    (license license:expat)))

(define-public node-webassemblyjs-wasm-parser
  (package
    (name "node-webassemblyjs-wasm-parser")
    (version "1.11.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webassemblyjs/wasm-parser/-/wasm-parser-1.11.1.tgz")
       (sha256
        (base32 "1zicrd5vd8dfdsl9wzq2hy4gi6b9l13kbdysmxjy52qjsxvw241s"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webassemblyjs-utf8
           node-webassemblyjs-leb128
           node-webassemblyjs-ieee754
           node-webassemblyjs-helper-wasm-bytecode
           node-webassemblyjs-helper-api-error
           node-webassemblyjs-ast))
    (home-page "https://github.com/xtuc/webassemblyjs#readme")
    (synopsis "WebAssembly binary format parser")
    (description "WebAssembly binary format parser")
    (license license:expat)))

(define-public node-acorn
  (package
    (name "node-acorn")
    (version "8.6.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/acorn/-/acorn-8.6.0.tgz")
       (sha256
        (base32 "1qxqbc82ja1wiihpl761ck0rjyw7hck646wpp29lhw6cg46dpzh8"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/acornjs/acorn")
    (synopsis "ECMAScript parser")
    (description "ECMAScript parser")
    (license license:expat)))

(define-public node-acorn-import-assertions
  (package
    (name "node-acorn-import-assertions")
    (version "1.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/acorn-import-assertions/-/acorn-import-assertions-1.8.0.tgz")
       (sha256
        (base32 "1pxwkvs6ky2d7cs8lg6haj9b0f5rxrqgx9vbcvv9qfz2m0izpnny"))))
    (build-system node-build-system)
    (inputs
     (list node-acorn))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/xtuc/acorn-import-assertions#readme")
    (synopsis "Support for import assertions in acorn")
    (description "Support for import assertions in acorn")
    (license license:expat)))

(define-public node-caniuse-lite
  (package
    (name "node-caniuse-lite")
    (version "1.0.30001284")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/caniuse-lite/-/caniuse-lite-1.0.30001284.tgz")
       (sha256
        (base32 "1fis6bb4w9pb9hhmm43fssa2bl3klmx4iz67fi8dpw9q6qqgaamv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/browserslist/caniuse-lite#readme")
    (synopsis "A smaller version of caniuse-db, with only the essentials!")
    (description "A smaller version of caniuse-db, with only the essentials!")
    (license license:cc-by4.0)))

(define-public node-electron-to-chromium
  (package
    (name "node-electron-to-chromium")
    (version "1.4.10")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/electron-to-chromium/-/electron-to-chromium-1.4.10.tgz")
       (sha256
        (base32 "1i4q50q6ikkslqcgyy83irc3n07mpgdki505xplv8yscxz9vpir0"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/kilian/electron-to-chromium#readme")
    (synopsis "Provides a list of electron-to-chromium version mappings")
    (description "Provides a list of electron-to-chromium version mappings")
    (license license:isc)))

(define-public node-escalade
  (package
    (name "node-escalade")
    (version "3.1.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/escalade/-/escalade-3.1.1.tgz")
       (sha256
        (base32 "17d7icirlarj6mjx321z04klpvjsxwga8c8svai2547bid131y30"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/lukeed/escalade#readme")
    (synopsis
     "A tiny (183B to 210B) and fast utility to ascend parent directories")
    (description
     "A tiny (183B to 210B) and fast utility to ascend parent directories")
    (license license:expat)))

(define-public node-node-releases
  (package
    (name "node-node-releases")
    (version "2.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/node-releases/-/node-releases-2.0.1.tgz")
       (sha256
        (base32 "13xkwrdj00bwln1fsfnnp5l3k0x5kg4s39x13a68a310bnlsfa0k"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/chicoxyzzy/node-releases#readme")
    (synopsis "Node.js releases data")
    (description "Node.js releases data")
    (license license:expat)))

(define-public node-picocolors
  (package
    (name "node-picocolors")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/picocolors/-/picocolors-1.0.0.tgz")
       (sha256
        (base32 "10zk2pciqiyxjapg6yp7n02nbvvyy00a6k8sz7jibsh6lhmyqqk0"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/alexeyraspopov/picocolors#readme")
    (synopsis
     "The tiniest and the fastest library for terminal output formatting with ANSI colors")
    (description
     "The tiniest and the fastest library for terminal output formatting with ANSI colors")
    (license license:isc)))

(define-public node-browserslist
  (package
    (name "node-browserslist")
    (version "4.18.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/browserslist/-/browserslist-4.18.1.tgz")
       (sha256
        (base32 "0lz2aw2wldfd7a4sm3976ym0v3klm495sc58ygn7w6bx7ffh65fr"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-picocolors node-node-releases node-escalade
           node-electron-to-chromium node-caniuse-lite))
    (home-page "https://github.com/browserslist/browserslist#readme")
    (synopsis
     "Share target browsers between different front-end tools, like Autoprefixer, Stylelint and babel-env-preset")
    (description
     "Share target browsers between different front-end tools, like Autoprefixer, Stylelint and babel-env-preset")
    (license license:expat)))

(define-public node-chrome-trace-event
  (package
    (name "node-chrome-trace-event")
    (version "1.0.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/chrome-trace-event/-/chrome-trace-event-1.0.3.tgz")
       (sha256
        (base32 "0khww2f29cl0197417vflsqncdf3wv0j388m1gyvbrry3z7gcmcr"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/samccone/chrome-trace-event#readme")
    (synopsis
     "A library to create a trace of your node app per Google's Trace Event format.")
    (description
     "A library to create a trace of your node app per Google's Trace Event format.")
    (license license:expat)))

(define-public node-enhanced-resolve
  (package
    (name "node-enhanced-resolve")
    (version "5.8.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/enhanced-resolve/-/enhanced-resolve-5.8.3.tgz")
       (sha256
        (base32 "05gzhvl4mhsqc88xab02p77x4q5zdzgr0x0ri7jhiaq6ng111806"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-tapable node-graceful-fs))
    (home-page "http://github.com/webpack/enhanced-resolve")
    (synopsis
     "Offers a async require.resolve function. It's highly configurable.")
    (description
     "Offers a async require.resolve function. It's highly configurable.")
    (license license:expat)))

(define-public node-es-module-lexer
  (package
    (name "node-es-module-lexer")
    (version "0.9.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/es-module-lexer/-/es-module-lexer-0.9.3.tgz")
       (sha256
        (base32 "1kyxvlb7wz5qilmikdq5v8bl0sxbrfzsiykdg5p9kgx03g1a3wxd"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/guybedford/es-module-lexer#readme")
    (synopsis "Lexes ES modules returning their import/export metadata")
    (description "Lexes ES modules returning their import/export metadata")
    (license license:expat)))

(define-public node-esrecurse
  (package
    (name "node-esrecurse")
    (version "4.3.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/esrecurse/-/esrecurse-4.3.0.tgz")
       (sha256
        (base32 "0q9vg6dmzdcy4mmm6dnmz1d9dfrm1gvi32n8f2slfsr9sxq97kry"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-estraverse))
    (home-page "https://github.com/estools/esrecurse")
    (synopsis "ECMAScript AST recursive visitor")
    (description "ECMAScript AST recursive visitor")
    (license #f)))

(define-public node-estraverse
  (package
    (name "node-estraverse")
    (version "5.3.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/estraverse/-/estraverse-5.3.0.tgz")
       (sha256
        (base32 "19pxf86qwp2xl1i8k5w6q81aaldmspmgys0ksnla91c0bgd4b39y"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/estools/estraverse")
    (synopsis "ECMAScript JS AST traversal functions")
    (description "ECMAScript JS AST traversal functions")
    (license #f)))

(define-public node-eslint-scope
  (package
    (name "node-eslint-scope")
    (version "5.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/eslint-scope/-/eslint-scope-5.1.0.tgz")
       (sha256
        (base32 "06645adsm9yq6f46krrqh2czks03kwf64yilvxmr7m5vskpvc6gi"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-estraverse node-esrecurse))
    (home-page "http://github.com/eslint/eslint-scope")
    (synopsis "ECMAScript scope analyzer for ESLint")
    (description "ECMAScript scope analyzer for ESLint")
    (license #f)))

(define-public node-events
  (package
    (name "node-events")
    (version "3.3.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/events/-/events-3.3.0.tgz")
       (sha256
        (base32 "0jsk7gxx98816wg0nl4wfwljscbg7iv8ys68mbgrnaaq1dixsc1c"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/Gozala/events#readme")
    (synopsis "Node's event emitter for all engines.")
    (description "Node's event emitter for all engines.")
    (license license:expat)))

(define-public node-json-parse-better-errors
  (package
    (name "node-json-parse-better-errors")
    (version "1.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/json-parse-better-errors/-/json-parse-better-errors-1.0.2.tgz")
       (sha256
        (base32 "1j89a2mmkghaan76ilpcjr437gwdgbi65aab9i6amsr84pb6bg02"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/zkat/json-parse-better-errors#readme")
    (synopsis "JSON.parse with context information on error")
    (description "JSON.parse with context information on error")
    (license license:expat)))

(define-public node-loader-runner
  (package
    (name "node-loader-runner")
    (version "4.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/loader-runner/-/loader-runner-4.2.0.tgz")
       (sha256
        (base32 "0szkdj9c6naf4bj3wspnh3kgc0r5d924fgskckn9vics728g6v99"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/webpack/loader-runner#readme")
    (synopsis "Runs (webpack) loaders")
    (description "Runs (webpack) loaders")
    (license license:expat)))

(define-public node-tapable
  (package
    (name "node-tapable")
    (version "2.2.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/tapable/-/tapable-2.2.1.tgz")
       (sha256
        (base32 "1qj630zbahlc3pxp2fiwgbwjz17kavfccpwhz0lf5rqr7n8zxgmk"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/webpack/tapable")
    (synopsis "Just a little module for plugins.")
    (description "Just a little module for plugins.")
    (license license:expat)))

(define-public node-types-node
  (package
    (name "node-types-node")
    (version "16.11.11")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@types/node/-/node-16.11.11.tgz")
       (sha256
        (base32 "16aaa3md17fzclp8ydm7md2mjczqrf3p99rqhp31wql1jvn7q1k0"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node")
    (synopsis "TypeScript definitions for Node.js")
    (description "TypeScript definitions for Node.js")
    (license license:expat)))

(define-public node-merge-stream
  (package
    (name "node-merge-stream")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/merge-stream/-/merge-stream-2.0.0.tgz")
       (sha256
        (base32 "0kknjzanvs927jfff7ggdpga1j2in91xm05435sycrbhnadvxhr4"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/grncdr/merge-stream#readme")
    (synopsis "Create a stream that emits events from multiple other streams")
    (description
     "Create a stream that emits events from multiple other streams")
    (license license:expat)))

(define-public node-supports-color
  (package
    (name "node-supports-color")
    (version "9.2.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/supports-color/-/supports-color-9.2.1.tgz")
       (sha256
        (base32 "0j7yzp6x4l9rs6nfb8zyrndmh3qilkm8nz3182kn8l67mp8bjgmz"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/chalk/supports-color#readme")
    (synopsis "Detect whether a terminal supports color")
    (description "Detect whether a terminal supports color")
    (license license:expat)))

(define-public node-jest-worker
  (package
    (name "node-jest-worker")
    (version "27.4.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/jest-worker/-/jest-worker-27.4.2.tgz")
       (sha256
        (base32 "18qsrvbg08wdpvmzna5nw705liz2ld3dwgfwfxm1gbpwnr4wyzxh"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-supports-color node-merge-stream node-types-node))
    (home-page "https://github.com/facebook/jest#readme")
    (synopsis
     "Module for executing heavy tasks under forked processes in parallel, by providing a `Promise` based interface, minimum overhead, and bound workers.")
    (description
     "Module for executing heavy tasks under forked processes in parallel, by providing a `Promise` based interface, minimum overhead, and bound workers.")
    (license license:expat)))

(define-public node-types-json-schema
  (package
    (name "node-types-json-schema")
    (version "7.0.9")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@types/json-schema/-/json-schema-7.0.9.tgz")
       (sha256
        (base32 "0ysawqpirs581skvcdbl0wdnl00ssfawb0ikls51a4gq003xq8p3"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/json-schema")
    (synopsis "TypeScript definitions for json-schema 4.0, 6.0 and")
    (description "TypeScript definitions for json-schema 4.0, 6.0 and")
    (license license:expat)))

(define-public node-ajv-keywords
  (package
    (name "node-ajv-keywords")
    (version "5.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/ajv-keywords/-/ajv-keywords-5.1.0.tgz")
       (sha256
        (base32 "0ld37xxpp4lxinh9ysn28jy5sf6jqjkr1riz8f9jnr4jgx6a8srl"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-fast-deep-equal node-ajv-for-webpack))
    (home-page "https://github.com/epoberezkin/ajv-keywords#readme")
    (synopsis "Additional JSON-Schema keywords for Ajv JSON validator")
    (description "Additional JSON-Schema keywords for Ajv JSON validator")
    (license license:expat)))

(define-public node-ajv-formats
  (package
    (name "node-ajv-formats")
    (version "2.1.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/ajv-formats/-/ajv-formats-2.1.1.tgz")
       (sha256
        (base32 "1ixxyw3rcw9lw86m257yfvpq3gvfxs85dc4nnvqpfq2dzcf0c85j"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-ajv-for-webpack))
    (home-page "https://github.com/ajv-validator/ajv-formats#readme")
    (synopsis "Format validation for Ajv v7+")
    (description "Format validation for Ajv v7+")
    (license license:expat)))

(define-public node-ajv-for-webpack
  (package
    (name "node-ajv-for-webpack")
    (version "8.8.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/ajv/-/ajv-8.8.2.tgz")
       (sha256
        (base32 "1w82x2a4xvcgvk46c5a6drviycaip78qz4xgkl9qnxmp0xbmhpmj"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'build))))
    (inputs
     (list node-uri-js node-require-from-string node-json-schema-traverse
           node-fast-deep-equal))
    (home-page "https://ajv.js.org")
    (synopsis "Another JSON Schema Validator")
    (description "Another JSON Schema Validator")
    (license license:expat)))


(define-public node-schema-utils
  ;; FIXME
  (package
    (name "node-schema-utils")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/schema-utils/-/schema-utils-4.0.0.tgz")
       (sha256
        (base32 "1c9c919yh58f3xcacl00r92cd55yb3axpk28iggsjjbzzswny424"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-ajv-formats node-ajv-keywords node-ajv-for-webpack
           node-types-json-schema))
    (home-page "https://github.com/webpack/schema-utils")
    (synopsis "webpack Validation Utils")
    (description "webpack Validation Utils")
    (license license:expat)))

(define-public node-randombytes
  (package
    (name "node-randombytes")
    (version "2.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/randombytes/-/randombytes-2.1.0.tgz")
       (sha256
        (base32 "1amws6pwpznpl6d58rsi3aa2cl779gkb1jysscm925r8agkd79mq"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-safe-buffer))
    (home-page "https://github.com/crypto-browserify/randombytes")
    (synopsis "random bytes from browserify stand alone")
    (description "random bytes from browserify stand alone")
    (license license:expat)))

(define-public node-serialize-javascript
  (package
    (name "node-serialize-javascript")
    (version "6.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/serialize-javascript/-/serialize-javascript-6.0.0.tgz")
       (sha256
        (base32 "1z021rj552fz9rqckmg44s6z0bz3jyzmck9gqcdj8hnk2xv92k98"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-randombytes))
    (home-page "https://github.com/yahoo/serialize-javascript")
    (synopsis
     "Serialize JavaScript to a superset of JSON that includes regular expressions and functions.")
    (description
     "Serialize JavaScript to a superset of JSON that includes regular expressions and functions.")
    (license license:bsd-3)))

(define-public node-commander
  (package
    (name "node-commander")
    (version "8.3.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/commander/-/commander-8.3.0.tgz")
       (sha256
        (base32 "02y195wsd1nac283z4vdv9sa67mwwdc1l12xr9h41a19j9gycf0v"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/tj/commander.js#readme")
    (synopsis "the complete solution for node.js command-line programs")
    (description "the complete solution for node.js command-line programs")
    (license license:expat)))

(define-public node-buffer-from
  (package
    (name "node-buffer-from")
    (version "1.1.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/buffer-from/-/buffer-from-1.1.2.tgz")
       (sha256
        (base32 "0hz3cbll0m805g22c5pnwdgpi1xavmrp5q1734x4d3yakvah6aww"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/LinusU/buffer-from#readme")
    (synopsis
     "A [ponyfill](https://ponyfill.com) for `Buffer.from`, uses native implementation if available.")
    (description
     "A [ponyfill](https://ponyfill.com) for `Buffer.from`, uses native implementation if available.")
    (license license:expat)))

(define-public node-source-map-support
  (package
    (name "node-source-map-support")
    (version "0.5.21")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/source-map-support/-/source-map-support-0.5.21.tgz")
       (sha256
        (base32 "0dpnahsipxckan03b5w5qrmlibkz8amh7k6gj77zs9387vph96sx"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-source-map node-buffer-from))
    (home-page "https://github.com/evanw/node-source-map-support#readme")
    (synopsis "Fixes stack traces for files with source maps")
    (description "Fixes stack traces for files with source maps")
    (license license:expat)))

(define-public node-terser
  (package
    (name "node-terser")
    (version "5.10.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/terser/-/terser-5.10.0.tgz")
       (sha256
        (base32 "1v435spr0naypa9nv7hr9jv3p2cwm7cxin0zwmgpq1day4i0kzvh"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-acorn node-source-map-support node-source-map
           node-commander))
    (home-page "https://terser.org")
    (synopsis
     "JavaScript parser, mangler/compressor and beautifier toolkit for ES6+")
    (description
     "JavaScript parser, mangler/compressor and beautifier toolkit for ES6+")
    (license #f)))

(define-public node-terser-webpack-plugin
  (package
    (name "node-terser-webpack-plugin")
    (version "5.2.5")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/terser-webpack-plugin/-/terser-webpack-plugin-5.2.5.tgz")
       (sha256
        (base32 "05gqxx1ry9gw53r81xf6qqdhidyy4fxqsrzp4433ldpga3bgyg3r"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       
       #:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("webpack"))))
         (delete 'configure) (delete 'build))))
    (inputs
     (list node-terser
           node-acorn ;; hack
           node-source-map
           node-serialize-javascript
           node-schema-utils
           node-jest-worker))
    (home-page "https://github.com/webpack-contrib/terser-webpack-plugin")
    (synopsis "Terser plugin for webpack")
    (description "Terser plugin for webpack")
    (license license:expat)))

(define-public node-watchpack
  (package
    (name "node-watchpack")
    (version "2.3.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/watchpack/-/watchpack-2.3.0.tgz")
       (sha256
        (base32 "15w8w166gghml41cd4d7a933rfzc0w1qnzsa95salnyjfgwjkw51"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-graceful-fs node-glob-to-regexp))
    (home-page "https://github.com/webpack/watchpack")
    (synopsis "Wrapper library for directory and file watching.")
    (description "Wrapper library for directory and file watching.")
    (license license:expat)))

(define-public node-webpack-sources
  (package
    (name "node-webpack-sources")
    (version "3.2.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/webpack-sources/-/webpack-sources-3.2.2.tgz")
       (sha256
        (base32 "05k62ld5vrzf8zk7jjbqkabdnvi3kv9h3z92lxmyzv77c0x2j9fp"))))
    (build-system node-build-system)
    (inputs
     (list node-acorn)) ;; hack
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/webpack/webpack-sources#readme")
    (synopsis "Source code handling classes for webpack")
    (description "Source code handling classes for webpack")
    (license license:expat)))

(define-public node-webpack
  (package
    (name "node-webpack")
    (version "5.64.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/webpack/-/webpack-5.64.4.tgz")
       (sha256
        (base32 "0pzrvjka3qcb2sgc6am4bag4kn072y5cgr1zax428aslinyhir2l"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webpack-sources
           node-watchpack
           node-terser-webpack-plugin
           node-tapable
           node-schema-utils
           node-neo-async
           node-mime-types
           node-loader-runner
           node-json-parse-better-errors
           node-graceful-fs
           node-glob-to-regexp
           node-events
           node-eslint-scope
           node-es-module-lexer
           node-enhanced-resolve
           node-chrome-trace-event
           node-browserslist
           node-acorn-import-assertions
           node-acorn
           node-webassemblyjs-wasm-parser
           node-webassemblyjs-wasm-edit
           node-webassemblyjs-ast
           node-types-estree
           node-types-eslint-scope))
    (home-page "https://github.com/webpack/webpack")
    (synopsis
     "Packs CommonJs/AMD modules for the browser. Allows to split your codebase into multiple bundles, which can be loaded on demand. Support loaders to preprocess files, i.e. json, jsx, es7, css, less, ... and your custom stuff.")
    (description
     "Packs CommonJs/AMD modules for the browser. Allows to split your codebase into multiple bundles, which can be loaded on demand. Support loaders to preprocess files, i.e. json, jsx, es7, css, less, ... and your custom stuff.")
    (license license:expat)))

(define-public node-types-html-minifier-terser
  (package
    (name "node-types-html-minifier-terser")
    (version "6.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@types/html-minifier-terser/-/html-minifier-terser-6.1.0.tgz")
       (sha256
        (base32 "0wvby2bhghzkd7gi49zm9xflp15jiqw1bk29lfqjwsz9pva0881a"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/html-minifier-terser")
    (synopsis "TypeScript definitions for html-minifier-terser")
    (description "TypeScript definitions for html-minifier-terser")
    (license license:expat)))

(define-public node-pascal-case
  (package
    (name "node-pascal-case")
    (version "3.1.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/pascal-case/-/pascal-case-3.1.2.tgz")
       (sha256
        (base32 "15r35n1jpavykyh5am10ngsjmmssica03w0s4wl989hbzzfmpd8d"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-tslib node-no-case))
    (home-page
     "https://github.com/blakeembrey/change-case/tree/master/packages/pascal-case#readme")
    (synopsis
     "Transform into a string of capitalized words without separators")
    (description
     "Transform into a string of capitalized words without separators")
    (license license:expat)))

(define-public node-camel-case
  (package
    (name "node-camel-case")
    (version "4.1.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/camel-case/-/camel-case-4.1.2.tgz")
       (sha256
        (base32 "1h86b9r585cxq433cy74skn6rglg94xf56d3383w878xarw9f358"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-tslib node-pascal-case))
    (home-page
     "https://github.com/blakeembrey/change-case/tree/master/packages/camel-case#readme")
    (synopsis
     "Transform into a string with the separator denoted by the next word capitalized")
    (description
     "Transform into a string with the separator denoted by the next word capitalized")
    (license license:expat)))

(define-public node-clean-css
  (package
    (name "node-clean-css")
    (version "5.2.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/clean-css/-/clean-css-5.2.2.tgz")
       (sha256
        (base32 "095klh9r92q9mvff6r1m8p7n14njv1ch6xvr11nxsyqmwz3bmvaf"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-source-map))
    (home-page "https://github.com/clean-css/clean-css")
    (synopsis "A well-tested CSS minifier")
    (description "A well-tested CSS minifier")
    (license license:expat)))

(define-public node-he
  (package
    (name "node-he")
    (version "1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/he/-/he-1.2.0.tgz")
       (sha256
        (base32 "0s0v3k8xgrcdch31p0ig12qkdfzdbk66s4rqhsz0nv0v1h1pqykp"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://mths.be/he")
    (synopsis
     "A robust HTML entities encoder/decoder with full Unicode support.")
    (description
     "A robust HTML entities encoder/decoder with full Unicode support.")
    (license license:expat)))

(define-public node-lower-case
  (package
    (name "node-lower-case")
    (version "2.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/lower-case/-/lower-case-2.0.2.tgz")
       (sha256
        (base32 "0cghksa0pvk28iipmx759cb2jr71mbyj86rh25j6n6gzvxn9ciay"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-tslib))
    (home-page
     "https://github.com/blakeembrey/change-case/tree/master/packages/lower-case#readme")
    (synopsis "Transforms the string to lower case")
    (description "Transforms the string to lower case")
    (license license:expat)))

(define-public node-no-case
  (package
    (name "node-no-case")
    (version "3.0.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/no-case/-/no-case-3.0.4.tgz")
       (sha256
        (base32 "16f6iznap8zhzsj2mlbjfb8dvihgnxnp9wp7wmdy7a9zz3nfqgrh"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-tslib node-lower-case))
    (home-page
     "https://github.com/blakeembrey/change-case/tree/master/packages/no-case#readme")
    (synopsis "Transform into a lower cased string with spaces between words")
    (description
     "Transform into a lower cased string with spaces between words")
    (license license:expat)))

(define-public node-dot-case
  (package
    (name "node-dot-case")
    (version "3.0.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/dot-case/-/dot-case-3.0.4.tgz")
       (sha256
        (base32 "1sysb2mhxxwh1qb53p1q3gk262m1qj1zp2mjx9cmpl09plfrnyc2"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-tslib node-no-case))
    (home-page
     "https://github.com/blakeembrey/change-case/tree/master/packages/dot-case#readme")
    (synopsis "Transform into a lower case string with a period between words")
    (description
     "Transform into a lower case string with a period between words")
    (license license:expat)))

(define-public node-tslib
  (package
    (name "node-tslib")
    (version "2.3.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/tslib/-/tslib-2.3.1.tgz")
       (sha256
        (base32 "1z4qh4mi0vh3pv8201rhl4g1fz1ls7x8xblj7grpdi34zpxzwx0p"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://www.typescriptlang.org/")
    (synopsis "Runtime library for TypeScript helper functions")
    (description "Runtime library for TypeScript helper functions")
    (license license:bsd-0)))

(define-public node-param-case
  (package
    (name "node-param-case")
    (version "3.0.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/param-case/-/param-case-3.0.4.tgz")
       (sha256
        (base32 "0z1v8ph0hvhmiwaz080k0dvn3h8zriz6n2c1mi3bvk6sh56fi68s"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-tslib node-dot-case))
    (home-page
     "https://github.com/blakeembrey/change-case/tree/master/packages/param-case#readme")
    (synopsis "Transform into a lower cased string with dashes between words")
    (description
     "Transform into a lower cased string with dashes between words")
    (license license:expat)))

(define-public node-relateurl
  (package
    (name "node-relateurl")
    (version "0.2.7")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/relateurl/-/relateurl-0.2.7.tgz")
       (sha256
        (base32 "0r1d6p5gdah2x30kaaf0vy4bpf81wfxcy97vvg7f2k99qj81ja38"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/stevenvachon/relateurl")
    (synopsis "Minify URLs by converting them from absolute to relative.")
    (description "Minify URLs by converting them from absolute to relative.")
    (license license:expat)))

(define-public node-html-minifier-terser
  (package
    (name "node-html-minifier-terser")
    (version "6.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/html-minifier-terser/-/html-minifier-terser-6.1.0.tgz")
       (sha256
        (base32 "12lyfrhaa9vh85x748jqvnbcdam5bvbffcdabyppcfggfxzcprzq"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-terser
           node-acorn ;; hack
           node-relateurl
           node-param-case
           node-he
           node-commander
           node-clean-css
           node-camel-case))
    (home-page "https://terser.org/html-minifier-terser/")
    (synopsis
     "Highly configurable, well-tested, JavaScript-based HTML minifier.")
    (description
     "Highly configurable, well-tested, JavaScript-based HTML minifier.")
    (license license:expat)))

(define-public node-css-what
  (package
    (name "node-css-what")
    (version "5.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/css-what/-/css-what-5.1.0.tgz")
       (sha256
        (base32 "0j8nwsjqj6gl732piisj3lzkww7vf6l4skhdr649cddqqh407hi2"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/fb55/css-what#readme")
    (synopsis "a CSS selector parser")
    (description "a CSS selector parser")
    (license #f)))

(define-public node-boolbase
  (package
    (name "node-boolbase")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/boolbase/-/boolbase-1.0.0.tgz")
       (sha256
        (base32 "1a6aqq33c0srw34mb1qkmrdn286mk3d2j3sax90gjprc17w8yqk9"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/fb55/boolbase")
    (synopsis "two functions: One that returns true, one that returns false")
    (description
     "two functions: One that returns true, one that returns false")
    (license license:isc)))

(define-public node-nth-check
  (package
    (name "node-nth-check")
    (version "2.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/nth-check/-/nth-check-2.0.1.tgz")
       (sha256
        (base32 "12wbbpwhhf50xc6nf28fpscxfnvpx63zcvnbprzwja606pbxhjwd"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-boolbase))
    (home-page "https://github.com/fb55/nth-check")
    (synopsis
     "Parses and compiles CSS nth-checks to highly optimized functions.")
    (description
     "Parses and compiles CSS nth-checks to highly optimized functions.")
    (license #f)))

(define-public node-css-select
  (package
    (name "node-css-select")
    (version "4.1.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/css-select/-/css-select-4.1.3.tgz")
       (sha256
        (base32 "18vbw7ps3ps0ryna7hbszsi38dsxkf0zaa4dv69qa7b0gr3qdh19"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-nth-check node-domutils node-domhandler node-css-what
           node-boolbase))
    (home-page "https://github.com/fb55/css-select#readme")
    (synopsis "a CSS selector compiler/engine")
    (description "a CSS selector compiler/engine")
    (license #f)))

(define-public node-utila
  (package
    (name "node-utila")
    (version "0.5.0-dev.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/utila/-/utila-0.5.0-dev.1.tgz")
       (sha256
        (base32 "0zf781rysshx95wfn1mmm3c719bzdcggyvk06vqca6bjyqgx2bc6"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/AriaMinaei/utila")
    (synopsis "notareplacementforunderscore")
    (description "notareplacementforunderscore")
    (license license:expat)))

(define-public node-dom-converter
  (package
    (name "node-dom-converter")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/dom-converter/-/dom-converter-0.2.0.tgz")
       (sha256
        (base32 "0syvigxhr6avjyiiq7v5vqqqm0hanydaw9zichsdi2bdinjclgdm"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-utila))
    (home-page "https://github.com/AriaMinaei/dom-converter#readme")
    (synopsis "converts bare objects to DOM objects or xml representations")
    (description "converts bare objects to DOM objects or xml representations")
    (license license:expat)))

(define-public node-dom-serializer
  (package
    (name "node-dom-serializer")
    (version "1.3.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/dom-serializer/-/dom-serializer-1.3.2.tgz")
       (sha256
        (base32 "1xzzzi24z2ynnx2m8djbqq7bcxld290lhmbv84by78x7naxzj6vp"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-entities node-domhandler node-domelementtype))
    (home-page "https://github.com/cheeriojs/dom-renderer#readme")
    (synopsis "render domhandler DOM nodes to a string")
    (description "render domhandler DOM nodes to a string")
    (license license:expat)))

(define-public node-domelementtype
  (package
    (name "node-domelementtype")
    (version "2.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/domelementtype/-/domelementtype-2.2.0.tgz")
       (sha256
        (base32 "0nvs65cdb1pg0j62vis6jg4yvb8qv46pyr3i8l3kd0b16c1hx5jr"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/fb55/domelementtype#readme")
    (synopsis "all the types of nodes in htmlparser2's dom")
    (description "all the types of nodes in htmlparser2's dom")
    (license #f)))

(define-public node-domhandler
  (package
    (name "node-domhandler")
    (version "4.3.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/domhandler/-/domhandler-4.3.0.tgz")
       (sha256
        (base32 "01zrb9q09gahs3q7ggm74v2jjvn7mkyrbbmrxkax5fp13754wxfv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-domelementtype))
    (home-page "https://github.com/fb55/domhandler#readme")
    (synopsis "Handler for htmlparser2 that turns pages into a dom")
    (description "Handler for htmlparser2 that turns pages into a dom")
    (license #f)))

(define-public node-domutils
  (package
    (name "node-domutils")
    (version "2.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/domutils/-/domutils-2.8.0.tgz")
       (sha256
        (base32 "0kk4nliisampi7wwidl8a4xaj3ica65lkcp31nacmk2agp2gq24c"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-domhandler node-domelementtype node-dom-serializer))
    (home-page "https://github.com/fb55/domutils#readme")
    (synopsis "Utilities for working with htmlparser2's dom")
    (description "Utilities for working with htmlparser2's dom")
    (license #f)))

(define-public node-entities
  (package
    (name "node-entities")
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/entities/-/entities-3.0.1.tgz")
       (sha256
        (base32 "1xixnv7ydywnn3lnf387fchlpsddxwa8nqcj24pannym246cmpdx"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/fb55/entities#readme")
    (synopsis "Encode & decode XML and HTML entities with ease")
    (description "Encode & decode XML and HTML entities with ease")
    (license #f)))

(define-public node-htmlparser2
  (package
    (name "node-htmlparser2")
    (version "7.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/htmlparser2/-/htmlparser2-7.2.0.tgz")
       (sha256
        (base32 "12nx5g9ya4y2sm18gfk0wk28r9q30g8djcznh1h2ciaq4iwfk0p6"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-entities node-domutils node-domhandler
           node-domelementtype))
    (home-page "https://github.com/fb55/htmlparser2#readme")
    (synopsis "Fast & forgiving HTML/XML parser")
    (description "Fast & forgiving HTML/XML parser")
    (license license:expat)))

(define-public node-lodash
  (package
    (name "node-lodash")
    (version "4.17.21")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/lodash/-/lodash-4.17.21.tgz")
       (sha256
        (base32 "017qragyfl5ifajdx48lvz46wr0jc1llikgvc2fhqakhwp4pl23a"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis "Lodash modular utilities.")
    (description "Lodash modular utilities.")
    (license license:expat)))

(define-public node-ansi-regex-5
  ;; avoid esm
  (package
    (inherit node-ansi-regex)
    (version "5.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/ansi-regex/-/ansi-regex-5.0.1.tgz")
       (sha256
        (base32
         "1ng0r2k4mcz7b2bfr6g1dschnxm0vifaslsvv2smv06smb6ss3hf"))))))
(define-public node-strip-ansi
  ;; avoid esm
  (package
    (name "node-strip-ansi")
    (version "6.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/strip-ansi/-/strip-ansi-6.0.1.tgz")
       (sha256
        (base32 "1jh81jj6cn1lli1c7m6xi0ynra9zdghb1g63v1nib7zlpz87bnwv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-ansi-regex-5))
    (home-page "https://github.com/chalk/strip-ansi#readme")
    (synopsis "Strip ANSI escape codes from a string")
    (description "Strip ANSI escape codes from a string")
    (license license:expat)))

(define-public node-renderkid
  (package
    (name "node-renderkid")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/renderkid/-/renderkid-3.0.0.tgz")
       (sha256
        (base32 "0hy2dvhwx5a7dqdp086wg2x969fkqjnb2n9h145lsp6clymlcc7l"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-strip-ansi node-lodash node-htmlparser2
           node-dom-converter node-css-select))
    (home-page "https://github.com/AriaMinaei/RenderKid#readme")
    (synopsis "Stylish console.log for node")
    (description "Stylish console.log for node")
    (license license:expat)))

(define-public node-pretty-error
  (package
    (name "node-pretty-error")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/pretty-error/-/pretty-error-4.0.0.tgz")
       (sha256
        (base32 "0881rzzjbh7x3634vcad4qw9b2qfcq6j7axy1a67p8797ln78rai"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-renderkid node-lodash))
    (home-page "https://github.com/AriaMinaei/pretty-error#readme")
    (synopsis "See nodejs errors with less clutter")
    (description "See nodejs errors with less clutter")
    (license license:expat)))

(define-public node-html-webpack-plugin
  (package
    (name "node-html-webpack-plugin")
    (version "5.5.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/html-webpack-plugin/-/html-webpack-plugin-5.5.0.tgz")
       (sha256
        (base32 "0ngx2hr8x0si7xzlgbhd7dp97ra8b1323vm384i6l68v3br9k6ss"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webpack
           node-tapable
           node-acorn ;; hack
           node-pretty-error
           node-lodash
           node-html-minifier-terser
           node-types-html-minifier-terser))
    (home-page "https://github.com/jantimon/html-webpack-plugin")
    (synopsis
     "Simplifies creation of HTML files to serve your webpack bundles")
    (description
     "Simplifies creation of HTML files to serve your webpack bundles")
    (license license:expat)))

(define-public node-array-union
  (package
    (name "node-array-union")
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/array-union/-/array-union-3.0.1.tgz")
       (sha256
        (base32 "14isk8f5dxag4z41kjjlawag7kmr4swyl71gq069cllyi2qx734i"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/array-union#readme")
    (synopsis
     "Create an array of unique values, in order, from the input arrays")
    (description
     "Create an array of unique values, in order, from the input arrays")
    (license license:expat)))

(define-public node-path-type
  (package
    (name "node-path-type")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/path-type/-/path-type-4.0.0.tgz")
       (sha256
        (base32 "15wvcgwg053hr2h11ja5swvdz3vvxciqq5aad0ara9qmzgwfh9f0"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/path-type#readme")
    (synopsis "Check if a path is a file, directory, or symlink")
    (description "Check if a path is a file, directory, or symlink")
    (license license:expat)))

(define-public node-dir-glob
  (package
    (name "node-dir-glob")
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/dir-glob/-/dir-glob-3.0.1.tgz")
       (sha256
        (base32 "0wj53iqp275dlsg7v36kxv97fid5pan2girgnhbdqw0vj8qplmkp"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-path-type))
    (home-page "https://github.com/kevva/dir-glob#readme")
    (synopsis "Convert directories to glob compatible strings")
    (description "Convert directories to glob compatible strings")
    (license license:expat)))

(define-public node-nodelib-fs-stat
  (package
    (name "node-nodelib-fs-stat")
    (version "2.0.5")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@nodelib/fs.stat/-/fs.stat-2.0.5.tgz")
       (sha256
        (base32 "0sqkaapvl86zldyw00j920hv4yncwb14nbgwnf3wl6pja4sm7y6q"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://www.npmjs.com/package/node-nodelib-fs-stat")
    (synopsis "Get the status of a file with some features")
    (description "Get the status of a file with some features")
    (license license:expat)))

(define-public node-queue-microtask
  (package
    (name "node-queue-microtask")
    (version "1.2.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/queue-microtask/-/queue-microtask-1.2.3.tgz")
       (sha256
        (base32 "1kcyybqa9jqb339mr9i9fxa7b6pn7fl5fzm278a5x1h1l8h0mbzz"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/feross/queue-microtask")
    (synopsis "fast, tiny `queueMicrotask` shim for modern engines")
    (description "fast, tiny `queueMicrotask` shim for modern engines")
    (license license:expat)))

(define-public node-run-parallel
  (package
    (name "node-run-parallel")
    (version "1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/run-parallel/-/run-parallel-1.2.0.tgz")
       (sha256
        (base32 "1j3syw7nnhr98sr9jngzmgqj33khjnl5rimhgbpih2vy6zsk38kb"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-queue-microtask))
    (home-page "https://github.com/feross/run-parallel")
    (synopsis "Run an array of functions in parallel")
    (description "Run an array of functions in parallel")
    (license license:expat)))

(define-public node-nodelib-fs-scandir
  (package
    (name "node-nodelib-fs-scandir")
    (version "2.1.5")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@nodelib/fs.scandir/-/fs.scandir-2.1.5.tgz")
       (sha256
        (base32 "0k7r1kjscdfbm2ckdgvq13zgycd4mci1admxn3dqp3n72yivy959"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-run-parallel node-nodelib-fs-stat))
    (home-page "https://www.npmjs.com/package/node-nodelib-fs-scandir")
    (synopsis "List files and directories inside the specified directory")
    (description "List files and directories inside the specified directory")
    (license license:expat)))

(define-public node-reusify
  (package
    (name "node-reusify")
    (version "1.0.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/reusify/-/reusify-1.0.4.tgz")
       (sha256
        (base32 "1i1kl423618nfp3rjalyl810v7sxz2x04smrmfpafbzs2zahql5a"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/mcollina/reusify#readme")
    (synopsis "Reuse objects and functions with style")
    (description "Reuse objects and functions with style")
    (license license:expat)))

(define-public node-fastq
  (package
    (name "node-fastq")
    (version "1.13.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/fastq/-/fastq-1.13.0.tgz")
       (sha256
        (base32 "1qy2wl0x9iakx3fd6sydj6c21lwvamm8v58632rznp2d41fm8bqa"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-reusify))
    (home-page "https://github.com/mcollina/fastq#readme")
    (synopsis "Fast, in memory work queue")
    (description "Fast, in memory work queue")
    (license license:isc)))

(define-public node-nodelib-fs-walk
  (package
    (name "node-nodelib-fs-walk")
    (version "1.2.8")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@nodelib/fs.walk/-/fs.walk-1.2.8.tgz")
       (sha256
        (base32 "0gbxfa920a6ykrl8a4phhvlwgybvivm2z10yyybww8mqd4gn5yfb"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-fastq node-nodelib-fs-scandir))
    (home-page "https://www.npmjs.com/package/node-nodelib-fs-walk")
    (synopsis "A library for efficiently walking a directory recursively")
    (description "A library for efficiently walking a directory recursively")
    (license license:expat)))

(define-public node-is-extglob
  (package
    (name "node-is-extglob")
    (version "2.1.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/is-extglob/-/is-extglob-2.1.1.tgz")
       (sha256
        (base32 "06dwa2xzjx6az40wlvwj11vican2w46710b9170jzmka2j344pcc"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/jonschlinkert/is-extglob")
    (synopsis "Returns true if a string has an extglob.")
    (description "Returns true if a string has an extglob.")
    (license license:expat)))

(define-public node-is-glob
  (package
    (name "node-is-glob")
    (version "4.0.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/is-glob/-/is-glob-4.0.3.tgz")
       (sha256
        (base32 "1imyq6pjl716cjc1ypmmnn0574rh28av3pq50mpqzd9v37xm7r1z"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-is-extglob))
    (home-page "https://github.com/micromatch/is-glob")
    (synopsis
     "Returns `true` if the given string looks like a glob pattern or an extglob pattern. This makes it easy to create code that only uses external modules like node-glob when necessary, resulting in much faster code execution and initialization time, and a bet")
    (description
     "Returns `true` if the given string looks like a glob pattern or an extglob pattern. This makes it easy to create code that only uses external modules like node-glob when necessary, resulting in much faster code execution and initialization time, and a bet")
    (license license:expat)))

(define-public node-glob-parent
  (package
    (name "node-glob-parent")
    (version "6.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/glob-parent/-/glob-parent-6.0.2.tgz")
       (sha256
        (base32 "0rjhim72pkv230y79xdahhpcnhy4y8fxr7dhr8sf47xyyd1bjnrl"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-is-glob))
    (home-page "https://github.com/gulpjs/glob-parent#readme")
    (synopsis "Extract the non-magic parent path from a glob string.")
    (description "Extract the non-magic parent path from a glob string.")
    (license license:isc)))

(define-public node-fast-glob
  (package
    (name "node-fast-glob")
    (version "3.2.7")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/fast-glob/-/fast-glob-3.2.7.tgz")
       (sha256
        (base32 "1877iaiajf0d9bq1g6ic4ibpzr6ddb2l22h3hjrg6k9n2x78mr3x"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-micromatch node-merge2 node-glob-parent
           node-nodelib-fs-walk node-nodelib-fs-stat))
    (home-page "https://github.com/mrmlnc/fast-glob#readme")
    (synopsis "It's a very fast and efficient glob library for Node.js")
    (description "It's a very fast and efficient glob library for Node.js")
    (license license:expat)))

(define-public node-ignore
  (package
    (name "node-ignore")
    (version "5.1.9")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/ignore/-/ignore-5.1.9.tgz")
       (sha256
        (base32 "1qvzfi7xq11baj01gkblsph8jc965z9dha0663imsx1xd7gjvjmk"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/kaelzhang/node-ignore#readme")
    (synopsis
     "Ignore is a manager and filter for .gitignore rules, the one used by eslint, gitbook and many others.")
    (description
     "Ignore is a manager and filter for .gitignore rules, the one used by eslint, gitbook and many others.")
    (license license:expat)))

(define-public node-merge2
  (package
    (name "node-merge2")
    (version "1.4.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/merge2/-/merge2-1.4.1.tgz")
       (sha256
        (base32 "10bq7m23r366fk3r6j058i1l4jz6vn5xlxcfnp2mkj5kr68i4f5q"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/teambition/merge2")
    (synopsis
     "Merge multiple streams into one stream in sequence or parallel.")
    (description
     "Merge multiple streams into one stream in sequence or parallel.")
    (license license:expat)))

(define-public node-globby
  (package
    (name "node-globby")
    (version "12.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/globby/-/globby-12.0.2.tgz")
       (sha256
        (base32 "17yb9kja3m64rr3lmy86fzzpg1klghr635g0p79dxfqd91nl8p8l"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-slash
           node-merge2
           node-ignore
           node-fast-glob
           node-dir-glob
           node-array-union))
    (home-page "https://github.com/sindresorhus/globby#readme")
    (synopsis "User-friendly glob matching")
    (description "User-friendly glob matching")
    (license license:expat)))

(define-public node-copy-webpack-plugin
  (package
    (name "node-copy-webpack-plugin")
    (version "10.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/copy-webpack-plugin/-/copy-webpack-plugin-10.0.0.tgz")
       (sha256
        (base32 "1a9jnfh7l8njvb1yhhl8pslsg6qj2kfdkl289kxsicqppas7blsa"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-serialize-javascript
           node-webpack
           node-acorn ;; hack
           node-schema-utils
           node-normalize-path
           node-globby
           node-glob-parent
           node-fast-glob))
    (home-page "https://github.com/webpack-contrib/copy-webpack-plugin")
    (synopsis "Copy files && directories with webpack")
    (description "Copy files && directories with webpack")
    (license license:expat)))

(define-public node-mini-css-extract-plugin
  (package
    (name "node-mini-css-extract-plugin")
    (version "2.4.5")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/mini-css-extract-plugin/-/mini-css-extract-plugin-2.4.5.tgz")
       (sha256
        (base32 "1vwcgp25d68y1zq8h1qx8zdf3d4ldpbylnv7wlw0xc4jyf80193x"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-acorn ;; hack
                  node-webpack node-schema-utils))
    (home-page "https://github.com/webpack-contrib/mini-css-extract-plugin")
    (synopsis "extracts CSS into separate files")
    (description "extracts CSS into separate files")
    (license license:expat)))



























(define-public node-discoveryjs-json-ext
  (package
    (name "node-discoveryjs-json-ext")
    (version "0.5.6")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@discoveryjs/json-ext/-/json-ext-0.5.6.tgz")
       (sha256
        (base32 "0llqw2fbrbfz7khsc12n2j09m6lj0j5lvpv1c1yljf6zfacm02ra"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/discoveryjs/json-ext#readme")
    (synopsis "A set of utilities that extend the use of JSON")
    (description "A set of utilities that extend the use of JSON")
    (license license:expat)))

(define-public node-webpack-cli-configtest
  (package
    (name "node-webpack-cli-configtest")
    (version "1.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webpack-cli/configtest/-/configtest-1.1.0.tgz")
       (sha256
        (base32 "0w1126sdi7gx1pbj06hbnwspqwf0g68acxclcjfb3s0s155yfisn"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies '("webpack"
                                    "webpack-cli"))))
         (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/webpack/webpack-cli/tree/master/packages/configtest")
    (synopsis "Validate a webpack configuration.")
    (description "Validate a webpack configuration.")
    (license license:expat)))

(define-public node-envinfo
  (package
    (name "node-envinfo")
    (version "7.8.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/envinfo/-/envinfo-7.8.1.tgz")
       (sha256
        (base32 "05d52y5z85fd5nkjxbhj0n7rbh5jgk5lxv7vdr1lim2h1808hbgv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/tabrindle/envinfo#readme")
    (synopsis "Info about your dev environment for debugging purposes")
    (description "Info about your dev environment for debugging purposes")
    (license license:expat)))

(define-public node-webpack-cli-info
  (package
    (name "node-webpack-cli-info")
    (version "1.4.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webpack-cli/info/-/info-1.4.0.tgz")
       (sha256
        (base32 "1rjcf9rbzgaj8gmwy0ig7mk19q2xah9cdyj97dgn9953a4ffzcdb"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies '("webpack"
                                    "webpack-cli"))))
         (delete 'configure) (delete 'build))))
    (inputs (list node-envinfo))
    (home-page
     "https://github.com/webpack/webpack-cli/tree/master/packages/info")
    (synopsis "Outputs info about system and webpack config")
    (description "Outputs info about system and webpack config")
    (license license:expat)))

(define-public node-webpack-cli-serve
  (package
    (name "node-webpack-cli-serve")
    (version "1.6.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/@webpack-cli/serve/-/serve-1.6.0.tgz")
       (sha256
        (base32 "0inr92yc90haslv781xjv69v9g0x9ipj9zd2fqyla8q3xgrjlpiw"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies '("webpack"
                                    "webpack-cli"))))
         (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/webpack/webpack-cli/tree/master/packages/serve")
    (synopsis "[![NPM Downloads][downloads]][downloads-url]")
    (description "[![NPM Downloads][downloads]][downloads-url]")
    (license license:expat)))

(define-public node-colorette
  (package
    (name "node-colorette")
    (version "2.0.16")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/colorette/-/colorette-2.0.16.tgz")
       (sha256
        (base32 "1n53axiidhfr6a7102m0dgc0dxm4gw3qkg3yckrxgfwz9x9i5i44"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/jorgebucaran/colorette#readme")
    (synopsis "?\x9f\x8c\x88Easily set your terminal text color & styles.")
    (description "?\x9f\x8c\x88Easily set your terminal text color & styles.")
    (license license:expat)))

(define-public node-shebang-regex
  (package
    (name "node-shebang-regex")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/shebang-regex/-/shebang-regex-4.0.0.tgz")
       (sha256
        (base32 "0iz89c5qyg9kyz5jqfkk3fdjnckwb2nbwl43lrw6j09mf6vvlj6d"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/shebang-regex#readme")
    (synopsis "Regular expression for matching a shebang line")
    (description "Regular expression for matching a shebang line")
    (license license:expat)))

(define-public node-shebang-command
  (package
    (name "node-shebang-command")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/shebang-command/-/shebang-command-2.0.0.tgz")
       (sha256
        (base32 "0vjmdpwcz23glkhlmxny8hc3x01zyr6hwf4qb3grq7m532ysbjws"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-shebang-regex))
    (home-page "https://github.com/kevva/shebang-command#readme")
    (synopsis "Get the command from a shebang")
    (description "Get the command from a shebang")
    (license license:expat)))

(define-public node-isexe
  (package
    (name "node-isexe")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/isexe/-/isexe-2.0.0.tgz")
       (sha256
        (base32 "0nc3rcqjgyb9yyqajwlzzhfcqmsb682z7zinnx9qrql8w1rfiks7"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/isaacs/isexe#readme")
    (synopsis "Minimal module to check if a file is executable.")
    (description "Minimal module to check if a file is executable.")
    (license license:isc)))

(define-public node-which
  (package
    (name "node-which")
    (version "2.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/which/-/which-2.0.2.tgz")
       (sha256
        (base32 "1p2fkm4lr36s85gdjxmyr6wh86dizf0iwmffxmarcxpbvmgxyfm1"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-isexe))
    (home-page "https://github.com/isaacs/node-which#readme")
    (synopsis
     "Like which(1) unix command. Find the first instance of an executable in the PATH.")
    (description
     "Like which(1) unix command. Find the first instance of an executable in the PATH.")
    (license license:isc)))

(define-public node-cross-spawn
  (package
    (name "node-cross-spawn")
    (version "7.0.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/cross-spawn/-/cross-spawn-7.0.3.tgz")
       (sha256
        (base32 "01bj9b7khakchhfl8dfbss3xm0w677h2hk1bzbpy65q214a8ii8i"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-which node-shebang-command node-path-key))
    (home-page "https://github.com/moxystudio/node-cross-spawn")
    (synopsis "Cross platform child_process#spawn and child_process#spawnSync")
    (description
     "Cross platform child_process#spawn and child_process#spawnSync")
    (license license:expat)))

(define-public node-get-stream
  (package
    (name "node-get-stream")
    (version "6.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/get-stream/-/get-stream-6.0.1.tgz")
       (sha256
        (base32 "08hnwmxp2jbbs6y4xb9ay6sdcjqnjdzvlvrxjsc62qpnf943fawk"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/get-stream#readme")
    (synopsis "Get a stream as a string, buffer, or array")
    (description "Get a stream as a string, buffer, or array")
    (license license:expat)))

(define-public node-human-signals
  (package
    (name "node-human-signals")
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/human-signals/-/human-signals-3.0.1.tgz")
       (sha256
        (base32 "0k32hiwx7p2xvys8yn6nxvmjf4mcaq8wg31k0fhfjanlfqdzcvap"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://git.io/JeluP")
    (synopsis "Human-friendly process signals")
    (description "Human-friendly process signals")
    (license license:asl2.0)))

(define-public node-is-stream
  (package
    (name "node-is-stream")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/is-stream/-/is-stream-3.0.0.tgz")
       (sha256
        (base32 "1wxryda8821l0bd8n2mdzjh97pd3jdnhsd4f2gq619pcd503jbyx"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/is-stream#readme")
    (synopsis "Check if something is a Node.js stream")
    (description "Check if something is a Node.js stream")
    (license license:expat)))

(define-public node-path-key
  (package
    (name "node-path-key")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/path-key/-/path-key-4.0.0.tgz")
       (sha256
        (base32 "19s9y2rp8b9wvbccpawvx895d69pam72r1vdvgma51h9k8n9m8mf"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/path-key#readme")
    (synopsis "Get the PATH environment variable key cross-platform")
    (description "Get the PATH environment variable key cross-platform")
    (license license:expat)))

(define-public node-npm-run-path
  (package
    (name "node-npm-run-path")
    (version "5.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/npm-run-path/-/npm-run-path-5.0.1.tgz")
       (sha256
        (base32 "1dsc7d30rak4a3ndp2bak166azln89p65k4r7vx5r67gqfzsgavd"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-path-key))
    (home-page "https://github.com/sindresorhus/npm-run-path#readme")
    (synopsis "Get your PATH prepended with locally installed binaries")
    (description "Get your PATH prepended with locally installed binaries")
    (license license:expat)))

(define-public node-mimic-fn
  (package
    (name "node-mimic-fn")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/mimic-fn/-/mimic-fn-4.0.0.tgz")
       (sha256
        (base32 "1ackkffccs3kalg201faz5k2chpzi03vjk6w8i8kq9q0q6c75jm3"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/mimic-fn#readme")
    (synopsis "Make a function mimic another one")
    (description "Make a function mimic another one")
    (license license:expat)))

(define-public node-onetime
  (package
    (name "node-onetime")
    (version "6.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/onetime/-/onetime-6.0.0.tgz")
       (sha256
        (base32 "0kj3wzfk6rwsbclc0cgj0xfqvi3v8gnivadzm5n75rwdisccfbm9"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-mimic-fn))
    (home-page "https://github.com/sindresorhus/onetime#readme")
    (synopsis "Ensure a function is only called once")
    (description "Ensure a function is only called once")
    (license license:expat)))

(define-public node-signal-exit
  (package
    (name "node-signal-exit")
    (version "3.0.6")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/signal-exit/-/signal-exit-3.0.6.tgz")
       (sha256
        (base32 "026d54xzqxbjgy6v9043y45szh6d146z8b6vgmb3822is4sa7fjg"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/tapjs/signal-exit")
    (synopsis "when you want to fire an event no matter how a process exits.")
    (description
     "when you want to fire an event no matter how a process exits.")
    (license license:isc)))

(define-public node-strip-final-newline
  (package
    (name "node-strip-final-newline")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/strip-final-newline/-/strip-final-newline-3.0.0.tgz")
       (sha256
        (base32 "11d5m2l0waiaadbiv9alh7k5wrlj52scl72zwj4xs2kff0mjjmy3"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/strip-final-newline#readme")
    (synopsis "Strip the final newline character from a string/buffer")
    (description "Strip the final newline character from a string/buffer")
    (license license:expat)))

(define-public node-execa
  (package
    (name "node-execa")
    (version "6.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/execa/-/execa-6.0.0.tgz")
       (sha256
        (base32 "0ah3a3gn65zbnwikd2xmcs1gfa4xrpk57vg82npgxsciiqz24aj9"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-strip-final-newline
           node-signal-exit
           node-onetime
           node-npm-run-path
           node-merge-stream
           node-is-stream
           node-human-signals
           node-get-stream
           node-cross-spawn))
    (home-page "https://github.com/sindresorhus/execa#readme")
    (synopsis "Process execution for humans")
    (description "Process execution for humans")
    (license license:expat)))

(define-public node-fastest-levenshtein
  (package
    (name "node-fastest-levenshtein")
    (version "1.0.12")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/fastest-levenshtein/-/fastest-levenshtein-1.0.12.tgz")
       (sha256
        (base32 "1zibx85wm9vjzc5xic2m8qqnjmjayvh2g6sq7jmy4ph9mgh813gn"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/ka-weihe/fastest-levenshtein#README")
    (synopsis "Fastest Levenshtein distance implementation in JS.")
    (description "Fastest Levenshtein distance implementation in JS.")
    (license license:expat)))

(define-public node-yocto-queue
  ;; avoid esm
  (package
    (name "node-yocto-queue")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/yocto-queue/-/yocto-queue-0.1.0.tgz")
       (sha256
        (base32 "1w99c4y6vl1q5dmw9hnkbiqxnmaz72x22qdc13mg084glpnbqwn7"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/yocto-queue#readme")
    (synopsis "Tiny queue data structure")
    (description "Tiny queue data structure")
    (license license:expat)))

(define-public node-p-limit
  ;; avoid esm
  (package
    (name "node-p-limit")
    (version "3.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/p-limit/-/p-limit-3.1.0.tgz")
       (sha256
        (base32 "1q69dflz7cr8j18l0va73jyg2nrzd0vq3jhvvica3yna6sbm3rin"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-yocto-queue))
    (home-page "https://github.com/sindresorhus/p-limit#readme")
    (synopsis
     "Run multiple promise-returning & async functions with limited concurrency")
    (description
     "Run multiple promise-returning & async functions with limited concurrency")
    (license license:expat)))

(define-public node-p-locate
  ;; avoid ERR_REQUIRE_ESM
  (package
    (name "node-p-locate")
    (version "5.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/p-locate/-/p-locate-5.0.0.tgz")
       (sha256
        (base32 "1z136h1a3c6a20dzsi5qc24ajk2gs8r59gj1n59sfr89m6019h9f"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-p-limit))
    (home-page "https://github.com/sindresorhus/p-locate#readme")
    (synopsis
     "Get the first fulfilled promise that satisfies the provided testing function")
    (description
     "Get the first fulfilled promise that satisfies the provided testing function")
    (license license:expat)))

(define-public node-locate-path
  ;; avoid ERR_REQUIRE_ESM
  (package
    (name "node-locate-path")
    (version "6.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/locate-path/-/locate-path-6.0.0.tgz")
       (sha256
        (base32 "0al3dmihydmwr89wc4ivqnkww7gm9p2ysq57j5qrj6sq8ckhplki"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-p-locate))
    (home-page "https://github.com/sindresorhus/locate-path#readme")
    (synopsis "Get the first path that exists on disk of multiple paths")
    (description "Get the first path that exists on disk of multiple paths")
    (license license:expat)))

(define-public node-path-exists
  ;; avoid esm
  (package
    (name "node-path-exists")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/path-exists/-/path-exists-4.0.0.tgz")
       (sha256
        (base32 "0p3pzdvfy2il8p0dvpp1l688in68bh2zzqzcfzvv7s9c634kbdfv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/path-exists#readme")
    (synopsis "Check if a path exists")
    (description "Check if a path exists")
    (license license:expat)))

(define-public node-find-up
  ;; avoid ERR_REQUIRE_ESM
  (package
    (name "node-find-up")
    (version "5.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/find-up/-/find-up-5.0.0.tgz")
       (sha256
        (base32 "1d3mr9ifkyz3dljjm6gr6av87a3s7zyn66fq72bc0s9pb3xlx9pl"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-path-exists node-locate-path))
    (home-page "https://github.com/sindresorhus/find-up#readme")
    (synopsis "Find a file or directory by walking up parent directories")
    (description "Find a file or directory by walking up parent directories")
    (license license:expat)))

(define-public node-pkg-dir
  ;; avoid ERR_REQUIRE_ESM
  (package
    (name "node-pkg-dir")
    (version "5.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/pkg-dir/-/pkg-dir-5.0.0.tgz")
       (sha256
        (base32 "04n9i5w3dabb73yxy1g3qgvj6v1apr4aimm35pxslx9j30hpc6y3"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-find-up))
    (home-page "https://github.com/sindresorhus/pkg-dir#readme")
    (synopsis "Find the root directory of a Node.js project or npm package")
    (description "Find the root directory of a Node.js project or npm package")
    (license license:expat)))

(define-public node-resolve-from
  (package
    (name "node-resolve-from")
    (version "5.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/resolve-from/-/resolve-from-5.0.0.tgz")
       (sha256
        (base32 "1qr59wyinki96pas0gn88d5xwdsl3hlrnfg97x1wy4xdrkzczxzx"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/resolve-from#readme")
    (synopsis
     "Resolve the path of a module like `require.resolve()` but from a given path")
    (description
     "Resolve the path of a module like `require.resolve()` but from a given path")
    (license license:expat)))

(define-public node-resolve-cwd
  (package
    (name "node-resolve-cwd")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/resolve-cwd/-/resolve-cwd-3.0.0.tgz")
       (sha256
        (base32 "19lc2l9w1b0y5krv3zsarccdh47x3rnmbzrwfnfk8n09wwqrind9"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-resolve-from))
    (home-page "https://github.com/sindresorhus/resolve-cwd#readme")
    (synopsis
     "Resolve the path of a module like `require.resolve()` but from the current working directory")
    (description
     "Resolve the path of a module like `require.resolve()` but from the current working directory")
    (license license:expat)))

(define-public node-import-local
  (package
    (name "node-import-local")
    (version "3.0.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/import-local/-/import-local-3.0.3.tgz")
       (sha256
        (base32 "0s9m299c7x8ixny5kif7k9cpwri4q3bldq7ni34arjrd5h7fcvsr"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-resolve-cwd node-pkg-dir))
    (home-page "https://github.com/sindresorhus/import-local#readme")
    (synopsis
     "Let a globally installed package use a locally installed version of itself if available")
    (description
     "Let a globally installed package use a locally installed version of itself if available")
    (license license:expat)))

(define-public node-interpret
  (package
    (name "node-interpret")
    (version "2.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/interpret/-/interpret-2.2.0.tgz")
       (sha256
        (base32 "1sfa3qhiafc0g8742g47g4fs0cmrqsn1jrkqrrjkhnrrbk93jgnv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/gulpjs/interpret#readme")
    (synopsis "A dictionary of file extensions and associated module loaders.")
    (description
     "A dictionary of file extensions and associated module loaders.")
    (license license:expat)))

(define-public node-is-core-module
  (package
    (name "node-is-core-module")
    (version "2.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/is-core-module/-/is-core-module-2.8.0.tgz")
       (sha256
        (base32 "07ivnq02fw4qc5x22h3f13hlvirvzx2mzgnj6q4rxfabc0ahra6a"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-has))
    (home-page "https://github.com/inspect-js/is-core-module")
    (synopsis "Is this specifier a node.js core module?")
    (description "Is this specifier a node.js core module?")
    (license license:expat)))

(define-public node-path-parse
  (package
    (name "node-path-parse")
    (version "1.0.7")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/path-parse/-/path-parse-1.0.7.tgz")
       (sha256
        (base32 "18vkai53yyiv1c1rimsh2whiymxnz9xj6a39c6b65097ly61jym0"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/jbgutierrez/path-parse#readme")
    (synopsis "Node.js path.parse() ponyfill")
    (description "Node.js path.parse() ponyfill")
    (license license:expat)))

(define-public node-resolve
  (package
    (name "node-resolve")
    (version "1.20.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/resolve/-/resolve-1.20.0.tgz")
       (sha256
        (base32 "12x15vnr7yf5l0mr5ga28w0rsszm036832mmdp706drn8imgnhfl"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-path-parse node-is-core-module))
    (home-page "https://github.com/browserify/resolve#readme")
    (synopsis
     "resolve like require.resolve() on behalf of files asynchronously and synchronously")
    (description
     "resolve like require.resolve() on behalf of files asynchronously and synchronously")
    (license license:expat)))

(define-public node-rechoir
  (package
    (name "node-rechoir")
    (version "0.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/rechoir/-/rechoir-0.8.0.tgz")
       (sha256
        (base32 "0rwdg8dp7bxx1gs029mvcivkc0jzp56jp094g06infk9qbvz36ka"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-resolve))
    (home-page "https://github.com/gulpjs/rechoir#readme")
    (synopsis
     "Prepare a node environment to require files with different extensions.")
    (description
     "Prepare a node environment to require files with different extensions.")
    (license license:expat)))

(define-public node-is-plain-object
  (package
    (name "node-is-plain-object")
    (version "5.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/is-plain-object/-/is-plain-object-5.0.0.tgz")
       (sha256
        (base32 "1lsvs8i1zimizbpf5i814aasrj1ifdi5xhpj0qlp525rj2ngsps0"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/jonschlinkert/is-plain-object")
    (synopsis
     "Returns true if an object was created by the `Object` constructor, or Object.create(null).")
    (description
     "Returns true if an object was created by the `Object` constructor, or Object.create(null).")
    (license license:expat)))

(define-public node-kind-of
  (package
    (name "node-kind-of")
    (version "6.0.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/kind-of/-/kind-of-6.0.3.tgz")
       (sha256
        (base32 "1nk31q65n9hcmbp16mbn55siqnf44wn1x71rrqyjv9bcbcxl893c"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/jonschlinkert/kind-of")
    (synopsis "Get the native type of a value.")
    (description "Get the native type of a value.")
    (license license:expat)))

(define-public node-shallow-clone
  (package
    (name "node-shallow-clone")
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/shallow-clone/-/shallow-clone-3.0.1.tgz")
       (sha256
        (base32 "0cxafqsg8mbvs4cc7z3xjpnmdpx8svmldwcg1w9r73fyi8hjvrpq"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-kind-of))
    (home-page "https://github.com/jonschlinkert/shallow-clone")
    (synopsis "Creates a shallow clone of any JavaScript value.")
    (description "Creates a shallow clone of any JavaScript value.")
    (license license:expat)))

(define-public node-clone-deep
  (package
    (name "node-clone-deep")
    (version "4.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/clone-deep/-/clone-deep-4.0.1.tgz")
       (sha256
        (base32 "0k1szrzsy3d3k3akxy4r6mg1kzj74h81097598lwndw65pvr92pd"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-shallow-clone node-kind-of node-is-plain-object))
    (home-page "https://github.com/jonschlinkert/clone-deep")
    (synopsis
     "Recursively (deep) clone JavaScript native types, like Object, Array, RegExp, Date as well as primitives.")
    (description
     "Recursively (deep) clone JavaScript native types, like Object, Array, RegExp, Date as well as primitives.")
    (license license:expat)))

(define-public node-wildcard
  (package
    (name "node-wildcard")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/wildcard/-/wildcard-2.0.0.tgz")
       (sha256
        (base32 "1v9ff0pfngh7b5wh4ipq5rb7ga728dndcl5vkmhyi2i6n2s43acx"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/DamonOehlman/wildcard#readme")
    (synopsis "Wildcard matching tools")
    (description "Wildcard matching tools")
    (license license:expat)))

(define-public node-webpack-merge
  (package
    (name "node-webpack-merge")
    (version "5.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/webpack-merge/-/webpack-merge-5.8.0.tgz")
       (sha256
        (base32 "12hcfbnhjpmn3m5wq172pnp5h078lsch69ixv1hdck22zrqld8j9"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-wildcard node-clone-deep))
    (home-page "https://github.com/survivejs/webpack-merge")
    (synopsis "Variant of merge that's useful for webpack configuration")
    (description "Variant of merge that's useful for webpack configuration")
    (license license:expat)))

(define-public node-webpack-cli
  (package
    (name "node-webpack-cli")
    (version "4.9.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/webpack-cli/-/webpack-cli-4.9.1.tgz")
       (sha256
        (base32 "1zslrmiydpb2n5484zfhc6sy1pshlavs0z13ywq7r7q1526z700f"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webpack
           node-webpack-merge
           node-acorn ;; hack
           node-rechoir
           node-interpret
           node-import-local
           node-fastest-levenshtein
           node-execa
           node-commander
           node-colorette
           node-webpack-cli-serve
           node-webpack-cli-info
           node-webpack-cli-configtest
           node-discoveryjs-json-ext))
    (home-page
     "https://github.com/webpack/webpack-cli/tree/master/packages/webpack-cli")
    (synopsis "CLI for webpack & friends")
    (description "CLI for webpack & friends")
    (license license:expat)))




(define-public node-make-dir
  (package
    (name "node-make-dir")
    (version "3.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/make-dir/-/make-dir-3.1.0.tgz")
       (sha256
        (base32 "1p3larbzfz9nny2m83x9isf5sng6gdc0x1vf6kyn24abln5anm7c"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs (list node-semver))
    (home-page "https://github.com/sindresorhus/make-dir#readme")
    (synopsis "Make a directory and its parents if needed - Think `mkdir -p`")
    (description
     "Make a directory and its parents if needed - Think `mkdir -p`")
    (license license:expat)))
(define-public node-find-cache-dir
  (package
    (name "node-find-cache-dir")
    (version "3.3.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/find-cache-dir/-/find-cache-dir-3.3.2.tgz")
       (sha256
        (base32 "01723jvhx129qw025n71kxcayynpc2qzsaairfp7y00skswapq2b"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-pkg-dir node-make-dir node-commondir))
    (home-page "https://github.com/avajs/find-cache-dir#readme")
    (synopsis "Finds the common standard cache directory")
    (description "Finds the common standard cache directory")
    (license license:expat)))

(define-public node-commondir
  (package
    (name "node-commondir")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/commondir/-/commondir-1.0.1.tgz")
       (sha256
        (base32 "1c2iqabm89b368173v6lik5zzll3b0b162ij90z956gpwhdqb2av"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/substack/node-commondir")
    (synopsis "compute the closest common parent for file paths")
    (description "compute the closest common parent for file paths")
    (license license:expat)))

(define-public node-loader-utils
  (package
    (name "node-loader-utils")
    (version "2.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/loader-utils/-/loader-utils-2.0.2.tgz")
       (sha256
        (base32 "0cyjnv34n9q6g2knz16947z0gkx0s88916zi29qm30w2vqjwszbm"))))
    (inputs
     (list node-json5 node-big-js node-emojis-list))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/webpack/loader-utils#readme")
    (synopsis "utils for webpack loaders")
    (description "utils for webpack loaders")
    (license license:expat)))


(define-public node-babel-loader
  (package
    (name "node-babel-loader")
    (version "8.2.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/babel-loader/-/babel-loader-8.2.3.tgz")
       (sha256
        (base32 "014sy4lmln5h7h38lqz0z63sh8vn9cf0rpkfkfcix7ss59xwb273"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-schema-utils node-make-dir node-loader-utils
           node-find-cache-dir))
    (home-page "https://github.com/babel/babel-loader")
    (synopsis "babel module loader for webpack")
    (description "babel module loader for webpack")
    (license license:expat)))




(define-public node-joycon
  (package
    (name "node-joycon")
    (version "3.1.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/joycon/-/joycon-3.1.1.tgz")
       (sha256
        (base32 "00wn20qxivls9i01mslw0sgbq1qmg0zcrki8mg93mc5zj8x0yghz"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/egoist/joycon#readme")
    (synopsis "Load config with ease.")
    (description "Load config with ease.")
    (license license:expat)))

(define-public node-type-fest
  (package
    (name "node-type-fest")
    (version "2.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/type-fest/-/type-fest-2.8.0.tgz")
       (sha256
        (base32 "17a93gwlb59hpbqyl3zzap2qfb1vyl676sv3pqc7pzqw9lrrl6yb"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/sindresorhus/type-fest#readme")
    (synopsis "A collection of essential TypeScript types")
    (description "A collection of essential TypeScript types")
    (license #f)))

(define-public node-esbuild-loader
  (package
    (name "node-esbuild-loader")
    (version "2.16.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/esbuild-loader/-/esbuild-loader-2.16.0.tgz")
       (sha256
        (base32 "0cx2p0qwimyyhby8many27cbidgl6q6yzari2xc1g3iajdjxqan4"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-webpack-sources
           node-webpack
           node-type-fest
           node-tapable
           node-loader-utils
           node-json5
           node-joycon
           node-esbuild))
    (home-page "https://github.com/privatenumber/esbuild-loader#readme")
    (synopsis "Speed up your Webpack build with esbuild")
    (description "Speed up your Webpack build with esbuild")
    (license license:expat)))


(define-public node-nanoid
  (package
    (name "node-nanoid")
    (version "3.1.30")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/nanoid/-/nanoid-3.1.30.tgz")
       (sha256
        (base32 "10bk58fcrl1hyjzfgazg6w3mpb70fmwhyi73x857f4a7xijn7if2"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/ai/nanoid#readme")
    (synopsis
     "A tiny (130 bytes), secure URL-friendly unique string ID generator")
    (description
     "A tiny (130 bytes), secure URL-friendly unique string ID generator")
    (license license:expat)))

(define-public node-source-map-js
  (package
    (name "node-source-map-js")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/source-map-js/-/source-map-js-1.0.1.tgz")
       (sha256
        (base32 "1kpv7s2mk59aaqz29z11n45p01dqy47vv406hzaccblvxkjkmv4j"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/7rulnik/source-map")
    (synopsis "Generates and consumes source maps")
    (description "Generates and consumes source maps")
    (license license:bsd-3)))

(define-public node-postcss
  (package
    (name "node-postcss")
    (version "8.4.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss/-/postcss-8.4.4.tgz")
       (sha256
        (base32 "0fdsgknynq36s12lkplzxn3h7gq10v3lhr10zy78yk86yhrjmd73"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-source-map-js node-picocolors node-nanoid))
    (home-page "https://postcss.org/")
    (synopsis "Tool for transforming styles with JS plugins")
    (description "Tool for transforming styles with JS plugins")
    (license license:expat)))

(define-public node-postcss-modules-extract-imports
  (package
    (name "node-postcss-modules-extract-imports")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss-modules-extract-imports/-/postcss-modules-extract-imports-3.0.0.tgz")
       (sha256
        (base32 "1kj3r5raryxdvhr520i7gfwrrrg2997csy6ksnzyax8dpi0zsqs5"))))
    (inputs
     (list node-postcss))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page
     "https://github.com/css-modules/postcss-modules-extract-imports")
    (synopsis
     "A CSS Modules transform to extract local aliases for inline imports")
    (description
     "A CSS Modules transform to extract local aliases for inline imports")
    (license license:isc)))

(define-public node-postcss-modules-local-by-default
  (package
    (name "node-postcss-modules-local-by-default")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss-modules-local-by-default/-/postcss-modules-local-by-default-4.0.0.tgz")
       (sha256
        (base32 "194p4mkn2rkvkva9mqn7d0394micv260949l0hg5c3iywp8q4j49"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-postcss node-postcss-value-parser
           node-postcss-selector-parser node-icss-utils))
    (home-page
     "https://github.com/css-modules/postcss-modules-local-by-default#readme")
    (synopsis "A CSS Modules transform to make local scope the default")
    (description "A CSS Modules transform to make local scope the default")
    (license license:expat)))

(define-public node-cssesc
  (package
    (name "node-cssesc")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/cssesc/-/cssesc-3.0.0.tgz")
       (sha256
        (base32 "0hqp21y2h4089cwrfk6c2hszimk7k4nn74ciwx2hhmk4nwirb99i"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://mths.be/cssesc")
    (synopsis
     "A JavaScript library for escaping CSS strings and identifiers while generating the shortest possible ASCII-only output.")
    (description
     "A JavaScript library for escaping CSS strings and identifiers while generating the shortest possible ASCII-only output.")
    (license license:expat)))

(define-public node-postcss-selector-parser
  (package
    (name "node-postcss-selector-parser")
    (version "6.0.6")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss-selector-parser/-/postcss-selector-parser-6.0.6.tgz")
       (sha256
        (base32 "1am2ssvxkxama35nnmci9f3hc4viybglr8qqhsjhvyxic6bbnh55"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    
    (inputs
     (list node-postcss node-util-deprecate node-cssesc))
    (home-page "https://github.com/postcss/postcss-selector-parser")
    (synopsis
     "> Selector parser with built in methods for working with selector strings.")
    (description
     "> Selector parser with built in methods for working with selector strings.")
    (license license:expat)))

(define-public node-postcss-modules-scope
  (package
    (name "node-postcss-modules-scope")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss-modules-scope/-/postcss-modules-scope-3.0.0.tgz")
       (sha256
        (base32 "1awfrnjzx8vcwdn5hhpc6wdakadd17qmlhviqqf90mz6vifygnw9"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    
    (inputs
     (list node-postcss node-postcss-selector-parser))
    (home-page "https://github.com/css-modules/postcss-modules-scope")
    (synopsis
     "A CSS Modules transform to extract export statements from local-scope classes")
    (description
     "A CSS Modules transform to extract export statements from local-scope classes")
    (license license:isc)))

(define-public node-icss-utils
  (package
    (name "node-icss-utils")
    (version "5.1.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/icss-utils/-/icss-utils-5.1.0.tgz")
       (sha256
        (base32 "141swy7kcv0myjpnaqs2020vw6di8ih6a0rd3j48g61nfl586840"))))
    (inputs
     (list node-postcss))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/css-modules/icss-utils#readme")
    (synopsis "ICSS utils for postcss ast")
    (description "ICSS utils for postcss ast")
    (license license:isc)))

(define-public node-postcss-modules-values
  (package
    (name "node-postcss-modules-values")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss-modules-values/-/postcss-modules-values-4.0.0.tgz")
       (sha256
        (base32 "0b3nxqr0fa5iblxzpp5plrdry83vg1s3lg3rm1y185kkv5g8iabk"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-postcss node-icss-utils))
    (home-page "https://github.com/css-modules/postcss-modules-values#readme")
    (synopsis
     "PostCSS plugin for CSS Modules to pass arbitrary values between your module files")
    (description
     "PostCSS plugin for CSS Modules to pass arbitrary values between your module files")
    (license license:isc)))

(define-public node-postcss-value-parser
  (package
    (name "node-postcss-value-parser")
    (version "4.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/postcss-value-parser/-/postcss-value-parser-4.2.0.tgz")
       (sha256
        (base32 "19whda1if07mb56pyqw7y750xclqnaqld82qn92a6rjvz4n3gr9x"))))
    (inputs
     (list node-postcss))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/TrySound/postcss-value-parser")
    (synopsis "Transforms css values and at-rule params into the tree")
    (description "Transforms css values and at-rule params into the tree")
    (license license:expat)))

(define-public node-css-loader
  (package
    (name "node-css-loader")
    (version "6.5.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/css-loader/-/css-loader-6.5.1.tgz")
       (sha256
        (base32 "1xwlw3s8hlq9j1s728rs6ql5xqy0s9wcxdpsizxp78vnz2wwk9hv"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-semver
           node-webpack
           node-postcss-value-parser
           node-postcss-modules-values
           node-postcss-modules-scope
           node-postcss-modules-local-by-default
           node-postcss-modules-extract-imports
           node-postcss
           node-icss-utils))
    (home-page "https://github.com/webpack-contrib/css-loader")
    (synopsis "css loader module for webpack")
    (description "css loader module for webpack")
    (license license:expat)))


(define-public node-emojis-list
  (package
    (name "node-emojis-list")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/emojis-list/-/emojis-list-3.0.0.tgz")
       (sha256
        (base32 "1s6sqvi0w9yrd46kdnjl15nw0khgyz7bivn3kx6a3j7xnv2wx0wk"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://nidecoc.io/Kikobeats/emojis-list")
    (synopsis "Complete list of standard emojis.")
    (description "Complete list of standard emojis.")
    (license license:expat)))

(define-public node-big-js
  (package
    (name "node-big-js")
    (version "5.2.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/big.js/-/big.js-5.2.2.tgz")
       (sha256
        (base32 "09v47lh5b9h2x2aazlpf7ggwbmfcskxj2rq0360pvxk6ispadf0p"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/MikeMcl/big.js#readme")
    (synopsis
     "A small, fast, easy-to-use library for arbitrary-precision decimal arithmetic")
    (description
     "A small, fast, easy-to-use library for arbitrary-precision decimal arithmetic")
    (license license:expat)))
#;
(define-public node-html-minifier-terser
(package
(name "node-html-minifier-terser")
(version "7.0.0-alpha.0")
(source
(origin
(method url-fetch)
(uri "https://registry.npmjs.org/html-minifier-terser/-/html-minifier-terser-7.0.0-alpha.0.tgz")
(sha256
(base32 "129m5c3wg62n7i28rlb29ghi6gv3d9jv4sgzp359w8k31hh7mrs4"))))
(build-system node-build-system)
(arguments
`(#:tests?
#f
#:phases
(modify-phases %standard-phases (delete 'configure) (delete 'build))))
(inputs
`(("node-terser" ,node-terser)
("node-relateurl" ,node-relateurl)
("node-param-case" ,node-param-case)
("node-entities" ,node-entities)
("node-commander" ,node-commander)
("node-clean-css" ,node-clean-css)
("node-camel-case" ,node-camel-case)))
(home-page "https://terser.org/html-minifier-terser/")
(synopsis
"Highly configurable, well-tested, JavaScript-based HTML minifier.")
(description
"Highly configurable, well-tested, JavaScript-based HTML minifier.")
(license license:expat)))

(define-public node-parse5
  (package
    (name "node-parse5")
    (version "6.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/parse5/-/parse5-6.0.1.tgz")
       (sha256
        (base32 "1x4nysnnip9vfgy5gianq85v7xsgdzsidmwp1jlf7nddkhq2lmjs"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/inikulin/parse5")
    (synopsis "HTML parser and serializer.")
    (description "HTML parser and serializer.")
    (license license:expat)))

(define-public node-html-loader
  (package
    (name "node-html-loader")
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/html-loader/-/html-loader-3.0.1.tgz")
       (sha256
        (base32 "0cm8fk7aimb6v7d0qacflm0a1zg6p8zkk47na5jgsbxkkskyjmd8"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (inputs
     (list node-parse5 node-webpack node-html-minifier-terser))
    (home-page "https://github.com/webpack-contrib/html-loader")
    (synopsis "Html loader module for webpack")
    (description "Html loader module for webpack")
    (license license:expat)))
