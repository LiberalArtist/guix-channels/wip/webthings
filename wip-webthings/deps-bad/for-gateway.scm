;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings deps-bad for-gateway)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages zwave)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (wip-webthings deps-good)
  #:use-module (wip-webthings deps-bad for-zwave)
  #:use-module ((guix licenses)
                #:prefix license:))


(define-public node-fluent-bundle
  (package
    (name "node-fluent-bundle")
    (version "0.16.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@fluent/bundle/-/bundle-0.16.1.tgz")
        (sha256
          (base32
            "0qqnjax41z1xvgq2dcs6v30p9k0s7g4qxl6b0jnihid228nz1zf1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://projectfluent.org")
    (synopsis
      "Localization library for expressive translations.")
    (description
      "Localization library for expressive translations.")
    (license license:asl2.0)))

(define-public node-cached-iterable
  (package
    (name "node-cached-iterable")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/cached-iterable/-/cached-iterable-0.3.0.tgz")
        (sha256
          (base32
            "1cmc61r8qjk82cc2w6hyi6yxa6c0czyhkwakizjsf0akl2cdsyrv"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/projectfluent/cached-iterable#readme")
    (synopsis
      "Iterables which cache the values they yield")
    (description
      "Iterables which cache the values they yield")
    (license license:asl2.0)))

(define-public node-fluent-dom
  (package
    (name "node-fluent-dom")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@fluent/dom/-/dom-0.7.0.tgz")
        (sha256
          (base32
            "0dh58gi65svvz5619k77y4j3wmqjwx0777cb8akifvfkzab9g6hc"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-cached-iterable))
    (home-page
      "https://github.com/projectfluent/fluent.js#readme")
    (synopsis "Fluent bindings for DOM")
    (description "Fluent bindings for DOM")
    (license license:asl2.0)))

(define-public node-babel-helper-validator-identifier
  (package
    (name "node-babel-helper-validator-identifier")
    (version "7.15.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@babel/helper-validator-identifier/-/helper-validator-identifier-7.15.7.tgz")
        (sha256
          (base32
            "1zanxwzg15sfmabwkji8643qkpxxk3i26ffj4lxshli746rm4b78"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://www.npmjs.com/package/node-babel-helper-validator-identifier")
    (synopsis "Validate identifier/keywords name")
    (description "Validate identifier/keywords name")
    (license license:expat)))

(define-public node-js-tokens
  (package
    (name "node-js-tokens")
    (version "7.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/js-tokens/-/js-tokens-7.0.0.tgz")
        (sha256
          (base32
            "02rwhq10vdl2p183p6wzm0kg4bq3yg43hhf8x6m4dixkzg5j3vrk"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/lydell/js-tokens#readme")
    (synopsis "Tiny JavaScript tokenizer.")
    (description "Tiny JavaScript tokenizer.")
    (license license:expat)))

(define-public node-babel-highlight
  (package
    (name "node-babel-highlight")
    (version "7.16.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@babel/highlight/-/highlight-7.16.0.tgz")
        (sha256
          (base32
            "10blk95d5q65wmjivrbr0abmx6z147v9r4r3na10pib5qgxmgjnw"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-js-tokens node-chalk
            node-babel-helper-validator-identifier))
    (home-page
      "https://babel.dev/docs/en/next/babel-highlight")
    (synopsis
      "Syntax highlight JavaScript strings for output in terminals.")
    (description
      "Syntax highlight JavaScript strings for output in terminals.")
    (license license:expat)))

(define-public node-babel-code-frame
  (package
    (name "node-babel-code-frame")
    (version "7.16.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@babel/code-frame/-/code-frame-7.16.0.tgz")
        (sha256
          (base32
            "0spx74lbswnpnaykv2rg3adaakm6hfrap1xali6yfzh0if1h6h18"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-babel-highlight))
    (home-page
      "https://babel.dev/docs/en/next/babel-code-frame")
    (synopsis
      "Generate errors that contain a code frame that point to source locations.")
    (description
      "Generate errors that contain a code frame that point to source locations.")
    (license license:expat)))

(define-public node-types-stack-utils
  (package
    (name "node-types-stack-utils")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/stack-utils/-/stack-utils-2.0.1.tgz")
        (sha256
          (base32
            "1svbjqqwmvaf5gdh8vcka0bqildqf8x5fr8n6kniwqadg167lp6c"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/stack-utils")
    (synopsis
      "TypeScript definitions for stack-utils")
    (description
      "TypeScript definitions for stack-utils")
    (license license:expat)))

(define-public node-is-number
  (package
    (name "node-is-number")
    (version "7.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/is-number/-/is-number-7.0.0.tgz")
        (sha256
          (base32
            "07nmmpplsj1gxzng6fxhnnyfkif9fvhvxa89d5lrgkwqf42w2xbv"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jonschlinkert/is-number")
    (synopsis
      "Returns true if a number or string value is a finite number. Useful for regex matches, parsing, user input, etc.")
    (description
      "Returns true if a number or string value is a finite number. Useful for regex matches, parsing, user input, etc.")
    (license license:expat)))

(define-public node-to-regex-range
  (package
    (name "node-to-regex-range")
    (version "5.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/to-regex-range/-/to-regex-range-5.0.1.tgz")
        (sha256
          (base32
            "1ms2bgz2paqfpjv1xpwx67i3dns5j9gn99il6cx5r4qaq9g2afm6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-is-number))
    (home-page
      "https://github.com/micromatch/to-regex-range")
    (synopsis
      "Pass two numbers, get a regex-compatible source string for matching ranges. Validated against more than 2.78 million test assertions.")
    (description
      "Pass two numbers, get a regex-compatible source string for matching ranges. Validated against more than 2.78 million test assertions.")
    (license license:expat)))

(define-public node-fill-range
  (package
    (name "node-fill-range")
    (version "7.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fill-range/-/fill-range-7.0.1.tgz")
        (sha256
          (base32
            "0wp93mwfgzcddi6ii62qx7gb082jgh0rfq6pgvv2xndjyaygvk98"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-to-regex-range))
    (home-page
      "https://github.com/jonschlinkert/fill-range")
    (synopsis
      "Fill in a range of numbers or letters, optionally passing an increment or `step` to use, or create a regex-compatible range with `options.toRegex`")
    (description
      "Fill in a range of numbers or letters, optionally passing an increment or `step` to use, or create a regex-compatible range with `options.toRegex`")
    (license license:expat)))

(define-public node-braces
  (package
    (name "node-braces")
    (version "3.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/braces/-/braces-3.0.2.tgz")
        (sha256
          (base32
            "1kpaa113m54qc1n2zvs0p1ika4s9dzvcczlw8q66xkyliy982n3k"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-fill-range))
    (home-page
      "https://github.com/micromatch/braces")
    (synopsis
      "Bash-like brace expansion, implemented in JavaScript. Safer than other brace expansion libs, with complete support for the Bash 4.3 braces specification, without sacrificing speed.")
    (description
      "Bash-like brace expansion, implemented in JavaScript. Safer than other brace expansion libs, with complete support for the Bash 4.3 braces specification, without sacrificing speed.")
    (license license:expat)))

(define-public node-micromatch
  (package
    (name "node-micromatch")
    (version "4.0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/micromatch/-/micromatch-4.0.4.tgz")
        (sha256
          (base32
            "01n3m0v97kgw0rlkg7nfb7g60hf4blj47cpggd73kji8g1na3i8r"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-picomatch node-braces))
    (home-page
      "https://github.com/micromatch/micromatch")
    (synopsis
      "Glob matching for javascript/node.js. A replacement and faster alternative to minimatch and multimatch.")
    (description
      "Glob matching for javascript/node.js. A replacement and faster alternative to minimatch and multimatch.")
    (license license:expat)))

(define-public node-ansi-regex
  (package
    (name "node-ansi-regex")
    (version "6.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ansi-regex/-/ansi-regex-6.0.1.tgz")
        (sha256
          (base32
            "1a0lmvr7b8n39nw5mngwhn0616v9inw2rh9c8r4a11rjd62vi1il"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/chalk/ansi-regex#readme")
    (synopsis
      "Regular expression for matching ANSI escape codes")
    (description
      "Regular expression for matching ANSI escape codes")
    (license license:expat)))

(define-public node-react-is
  (package
    (name "node-react-is")
    (version "17.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/react-is/-/react-is-17.0.2.tgz")
        (sha256
          (base32
            "1c9yjrjhipdynvkxq035qhd6b9q3l60i482ah6dqxh2gbg05064v"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://reactjs.org/")
    (synopsis "Brand checking of React Elements.")
    (description "Brand checking of React Elements.")
    (license license:expat)))

(define-public node-pretty-format
  (package
    (name "node-pretty-format")
    (version "27.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/pretty-format/-/pretty-format-27.3.1.tgz")
        (sha256
          (base32
            "0vyv8a3x9zqgd8ia7n3gha917rv1azadpl3vp9fkv9qmipag0c3z"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-react-is node-ansi-styles node-ansi-regex
            node-jest-types))
    (home-page
      "https://github.com/facebook/jest#readme")
    (synopsis "Stringify any JavaScript value.")
    (description "Stringify any JavaScript value.")
    (license license:expat)))

(define-public node-escape-string-regexp
  (package
    (name "node-escape-string-regexp")
    (version "5.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/escape-string-regexp/-/escape-string-regexp-5.0.0.tgz")
        (sha256
          (base32
            "1ri87l4by2953xhr1n5v3wszh0m5hkjndmr7rhp8aaxax2pn5dn8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sindresorhus/escape-string-regexp#readme")
    (synopsis "Escape RegExp special characters")
    (description "Escape RegExp special characters")
    (license license:expat)))

(define-public node-stack-utils
  (package
    (name "node-stack-utils")
    (version "2.0.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/stack-utils/-/stack-utils-2.0.5.tgz")
        (sha256
          (base32
            "141im30pp574i13abn7dwinqwcf2px3qlrw8nzs1p9fyp58w22p9"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-escape-string-regexp))
    (home-page
      "https://github.com/tapjs/stack-utils#readme")
    (synopsis "Captures and cleans stack traces")
    (description "Captures and cleans stack traces")
    (license license:expat)))

(define-public node-jest-message-util
  (package
    (name "node-jest-message-util")
    (version "27.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/jest-message-util/-/jest-message-util-27.3.1.tgz")
        (sha256
          (base32
            "12nrpgkb03aplffia7drkwn67fsfyprvcpfhnmzwz7gry2hh2plm"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-stack-utils
            node-slash
            node-pretty-format
            node-micromatch
            node-graceful-fs
            node-chalk
            node-types-stack-utils
            node-jest-types
            node-babel-code-frame))
    (home-page
      "https://github.com/facebook/jest#readme")
    (synopsis "#<unspecified>")
    (description "#<unspecified>")
    (license license:expat)))

(define-public node-types-istanbul-lib-coverage
  (package
    (name "node-types-istanbul-lib-coverage")
    (version "2.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/istanbul-lib-coverage/-/istanbul-lib-coverage-2.0.3.tgz")
        (sha256
          (base32
            "1q81wdyxhrmcfcf5ia3yzgrnlcv3x9z5284m5fdagqx6qng7m7if"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://www.npmjs.com/package/node-types-istanbul-lib-coverage")
    (synopsis
      "TypeScript definitions for istanbul-lib-coverage")
    (description
      "TypeScript definitions for istanbul-lib-coverage")
    (license license:expat)))

(define-public node-types-istanbul-lib-report
  (package
    (name "node-types-istanbul-lib-report")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/istanbul-lib-report/-/istanbul-lib-report-3.0.0.tgz")
        (sha256
          (base32
            "1bjmj36dpd5p0808vbzimjc9labx25dbqni25nsiqjy728jcqvp1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-types-istanbul-lib-coverage))
    (home-page
      "https://www.npmjs.com/package/node-types-istanbul-lib-report")
    (synopsis
      "TypeScript definitions for istanbul-lib-report")
    (description
      "TypeScript definitions for istanbul-lib-report")
    (license license:expat)))

(define-public node-types-istanbul-reports
  (package
    (name "node-types-istanbul-reports")
    (version "3.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/istanbul-reports/-/istanbul-reports-3.0.1.tgz")
        (sha256
          (base32
            "1ci08y3mi1mcy6gj3fssk2qrk7frqv01hsq7psqhg324cynvdh31"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-types-istanbul-lib-report))
    (home-page
      "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/istanbul-reports")
    (synopsis
      "TypeScript definitions for istanbul-reports")
    (description
      "TypeScript definitions for istanbul-reports")
    (license license:expat)))

(define-public node-types-yargs-parser
  (package
    (name "node-types-yargs-parser")
    (version "20.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/yargs-parser/-/yargs-parser-20.2.1.tgz")
        (sha256
          (base32
            "047hiylf85wph8qpla1w5kcwsp92y6xcrvndsz473148k852rqfg"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/yargs-parser")
    (synopsis
      "TypeScript definitions for yargs-parser")
    (description
      "TypeScript definitions for yargs-parser")
    (license license:expat)))

(define-public node-types-yargs
  (package
    (name "node-types-yargs")
    (version "17.0.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/yargs/-/yargs-17.0.7.tgz")
        (sha256
          (base32
            "1rz31qiyrn9w5kbvss2qrxsbclyl75s1z57m2m02k7c6b01j7h45"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-types-yargs-parser))
    (home-page
      "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/yargs")
    (synopsis "TypeScript definitions for yargs")
    (description "TypeScript definitions for yargs")
    (license license:expat)))

(define-public node-jest-types
  (package
    (name "node-jest-types")
    (version "27.2.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@jest/types/-/types-27.2.5.tgz")
        (sha256
          (base32
            "0hlr5b627pp7fgqhz4mvbqr5n6fy70wzj3x26j8nz57nfrldq65v"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-chalk node-types-yargs node-types-node
            node-types-istanbul-reports node-types-istanbul-lib-coverage))
    (home-page
      "https://github.com/facebook/jest#readme")
    (synopsis "#<unspecified>")
    (description "#<unspecified>")
    (license license:expat)))

(define-public node-types-node
  (package
    (name "node-types-node")
    (version "16.11.10")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@types/node/-/node-16.11.10.tgz")
        (sha256
          (base32
            "1sjx88q32888c9m553b9hlz73hplg52827shsv3mrrc0v7m50299"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node")
    (synopsis "TypeScript definitions for Node.js")
    (description
      "TypeScript definitions for Node.js")
    (license license:expat)))

(define-public node-ansi-styles
  (package
    (name "node-ansi-styles")
    (version "6.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ansi-styles/-/ansi-styles-6.1.0.tgz")
        (sha256
          (base32
            "0ywb49h4i73rpbnr6wjsxnjn2gl64bxbrbnn20dn3qbbgxkid1fj"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/chalk/ansi-styles#readme")
    (synopsis
      "ANSI escape codes for styling strings in the terminal")
    (description
      "ANSI escape codes for styling strings in the terminal")
    (license license:expat)))

(define-public node-supports-color
  (package
    (name "node-supports-color")
    (version "9.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/supports-color/-/supports-color-9.1.0.tgz")
        (sha256
          (base32
            "1pdsxgw6ynnk14czx58da9svm9an1g9mxiqsvypdzwqw51xkd09p"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/chalk/supports-color#readme")
    (synopsis
      "Detect whether a terminal supports color")
    (description
      "Detect whether a terminal supports color")
    (license license:expat)))

(define-public node-chalk
  (package
    (name "node-chalk")
    (version "4.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/chalk/-/chalk-4.1.2.tgz")
        (sha256
          (base32
            "02prgl8d52k2vgxnssx06ha2sjm2vp6v6s6kqgkar1ryllx68k78"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-supports-color node-ansi-styles))
    (home-page
      "https://github.com/chalk/chalk#readme")
    (synopsis "Terminal string styling done right")
    (description
      "Terminal string styling done right")
    (license license:expat)))

(define-public node-ci-info
  (package
    (name "node-ci-info")
    (version "3.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ci-info/-/ci-info-3.3.0.tgz")
        (sha256
          (base32
            "0jl4brm1qn236j8paxrl6hzyk759cqw8ddf50i1c9pj4k3vzrd61"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/watson/ci-info")
    (synopsis
      "Get details about the current Continuous Integration environment")
    (description
      "Get details about the current Continuous Integration environment")
    (license license:expat)))

(define-public node-graceful-fs
  (package
    (name "node-graceful-fs")
    (version "4.2.8")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/graceful-fs/-/graceful-fs-4.2.8.tgz")
        (sha256
          (base32
            "19kdnpgkbf2g2f4jdqc869g60wljdq10qmk4vymvwq7q2fm12sp5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/isaacs/node-graceful-fs#readme")
    (synopsis
      "A drop-in replacement for fs, making various improvements.")
    (description
      "A drop-in replacement for fs, making various improvements.")
    (license license:isc)))

(define-public node-picomatch
  (package
    (name "node-picomatch")
    (version "2.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/picomatch/-/picomatch-2.3.0.tgz")
        (sha256
          (base32
            "15imkbw2d0v1mxa03zai1bf6k58jica22rdyd51qnda6cnrwp0gc"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/micromatch/picomatch")
    (synopsis
      "Blazing fast and accurate glob matcher written in JavaScript, with no dependencies and full support for standard and extended Bash glob features, including braces, extglobs, POSIX brackets, and regular expressions.")
    (description
      "Blazing fast and accurate glob matcher written in JavaScript, with no dependencies and full support for standard and extended Bash glob features, including braces, extglobs, POSIX brackets, and regular expressions.")
    (license license:expat)))

(define-public node-jest-util
  (package
    (name "node-jest-util")
    (version "27.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/jest-util/-/jest-util-27.3.1.tgz")
        (sha256
          (base32
            "1mxffrv9bzl53dhxxw3g3vi5ps8w48rcn4l5lg672p2hnmfpd2h4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-picomatch
            node-graceful-fs
            node-ci-info
            node-chalk
            node-types-node
            node-jest-types))
    (home-page
      "https://github.com/facebook/jest#readme")
    (synopsis "#<unspecified>")
    (description "#<unspecified>")
    (license license:expat)))

(define-public node-slash
  (package
    (name "node-slash")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/slash/-/slash-4.0.0.tgz")
        (sha256
          (base32
            "0i24cshnv1yihgxbjvsj0lriasiipgdcbdwg2wrbnkjj6913jycq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sindresorhus/slash#readme")
    (synopsis
      "Convert Windows backslash paths to slash paths")
    (description
      "Convert Windows backslash paths to slash paths")
    (license license:expat)))

(define-public node-jest-console
  (package
    (name "node-jest-console")
    (version "27.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@jest/console/-/console-27.3.1.tgz")
        (sha256
          (base32
            "0al0xpzx3z71crxgz9rlvxqs4y7xd2qgjln9iwzgr61abjklhq3l"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-slash
            node-jest-util
            node-jest-message-util
            node-chalk
            node-types-node
            node-jest-types))
    (home-page
      "https://github.com/facebook/jest#readme")
    (synopsis "#<unspecified>")
    (description "#<unspecified>")
    (license license:expat)))

(define-public node-follow-redirects
  (package
    (name "node-follow-redirects")
    (version "1.14.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/follow-redirects/-/follow-redirects-1.14.5.tgz")
        (sha256
          (base32
            "0zyh9aq1143l134mmahhv4p3wgnk7wnjqxxn0l9rgz4adfv8g08s"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/follow-redirects/follow-redirects")
    (synopsis
      "HTTP and HTTPS modules that follow redirects.")
    (description
      "HTTP and HTTPS modules that follow redirects.")
    (license license:expat)))

(define-public node-axios
  (package
    (name "node-axios")
    (version "0.24.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/axios/-/axios-0.24.0.tgz")
        (sha256
          (base32
            "1gv98nzv4f86i4hrp27538bzk9j9j6q9jyrl2wdhpj61mlmmf33j"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-follow-redirects))
    (home-page "https://axios-http.com")
    (synopsis
      "Promise based HTTP client for the browser and node.js")
    (description
      "Promise based HTTP client for the browser and node.js")
    (license license:expat)))

(define-public node-backo2
  (package
    (name "node-backo2")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/backo2/-/backo2-1.0.2.tgz")
        (sha256
          (base32
            "04ns0z2j1v55aw7bq5kjm3d2kmzsv50yf9fkcp7wiywk0fq89yv7"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/mokesmokes/backo")
    (synopsis
      "simple backoff based on segmentio/backo")
    (description
      "simple backoff based on segmentio/backo")
    (license license:expat)))

(define-public node-bluebird
  (package
    (name "node-bluebird")
    (version "3.7.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/bluebird/-/bluebird-3.7.2.tgz")
        (sha256
          (base32
            "1p3dh022lvsrmsfyz37nr2x96g85y3356l4pgqkn32gm8hm0p2y6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/petkaantonov/bluebird")
    (synopsis
      "Full featured Promises/A+ implementation with exceptionally good performance")
    (description
      "Full featured Promises/A+ implementation with exceptionally good performance")
    (license license:expat)))

(define-public node-node-forge
  (package
    (name "node-node-forge")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/node-forge/-/node-forge-0.10.0.tgz")
        (sha256
          (base32
            "0gn3n403px32fwyp2wrq73wfg7qdxbag9drljb9a8iiqnils6gi5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/digitalbazaar/forge")
    (synopsis
      "JavaScript implementations of network transports, cryptography, ciphers, PKI, message digests, and various utilities.")
    (description
      "JavaScript implementations of network transports, cryptography, ciphers, PKI, message digests, and various utilities.")
    (license #f)))

(define-public node-acme-client
  (package
    (name "node-acme-client")
    (version "4.1.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/acme-client/-/acme-client-4.1.3.tgz")
        (sha256
          (base32
            "0ayyc2rlxndi1akk14l8mphcf38a7p8s0bf8w2bd8v7vc24c3zpg"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-node-forge node-debug node-bluebird node-backo2
            node-axios))
    (home-page
      "https://github.com/publishlab/node-acme-client")
    (synopsis "Simple and unopinionated ACME client")
    (description
      "Simple and unopinionated ACME client")
    (license license:expat)))

(define-public node-async
  (package
    (name "node-async")
    (version "3.2.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/async/-/async-3.2.2.tgz")
        (sha256
          (base32
            "1k5lh7ngldy863pl6w0pxjx0iq4pa5zcj5rzch0wqwwfgb8kgzkn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://caolan.github.io/async/")
    (synopsis
      "Higher-order functions and common patterns for asynchronous code")
    (description
      "Higher-order functions and common patterns for asynchronous code")
    (license license:expat)))

(define-public node-readdir-glob
  (package
    (name "node-readdir-glob")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/readdir-glob/-/readdir-glob-1.1.1.tgz")
        (sha256
          (base32
            "01422jh83h6wvib4fggc3334im2fv19lf5a2m0j7xf0gxzawsy65"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-minimatch))
    (home-page
      "https://github.com/Yqnn/node-readdir-glob")
    (synopsis
      "Recursive fs.readdir with streaming API and glob filtering.")
    (description
      "Recursive fs.readdir with streaming API and glob filtering.")
    (license license:asl2.0)))

(define-public node-base64-js
  (package
    (name "node-base64-js")
    (version "1.5.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/base64-js/-/base64-js-1.5.1.tgz")
        (sha256
          (base32
            "118a46skxnrgx5bdd68ny9xxjcvyb7b1clj2hf82d196nm2skdxi"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/beatgammit/base64-js")
    (synopsis "Base64 encoding/decoding in pure JS")
    (description
      "Base64 encoding/decoding in pure JS")
    (license license:expat)))

(define-public node-ieee754
  (package
    (name "node-ieee754")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ieee754/-/ieee754-1.2.1.tgz")
        (sha256
          (base32
            "1b4xiyr6fmgl05cjgc8fiyfk2jagf7xq2y5rknw9scvy76dlpwcf"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/feross/ieee754#readme")
    (synopsis
      "Read/write IEEE754 floating point numbers from/to a Buffer or array-like object")
    (description
      "Read/write IEEE754 floating point numbers from/to a Buffer or array-like object")
    (license license:bsd-3)))

(define-public node-buffer
  (package
    (name "node-buffer")
    (version "6.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/buffer/-/buffer-6.0.3.tgz")
        (sha256
          (base32
            "1xaim9bjs0l0any341dijprl7x576k6sgbm9qnrgdz2a494nynsm"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-ieee754 node-base64-js))
    (home-page "https://github.com/feross/buffer")
    (synopsis "Node.js Buffer API, for the browser")
    (description
      "Node.js Buffer API, for the browser")
    (license license:expat)))

(define-public node-bl
  (package
    (name "node-bl")
    (version "5.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/bl/-/bl-5.0.0.tgz")
        (sha256
          (base32
            "19z1lg1sjy1wzg8rbgcrhbhjz5m3brjd2gr38xb5fdrb4c4j5s5x"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream node-inherits node-buffer))
    (home-page "https://github.com/rvagg/bl")
    (synopsis
      "Buffer List: collect buffers and access with a standard readable Buffer interface, streamable too!")
    (description
      "Buffer List: collect buffers and access with a standard readable Buffer interface, streamable too!")
    (license license:expat)))

(define-public node-end-of-stream
  (package
    (name "node-end-of-stream")
    (version "1.4.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/end-of-stream/-/end-of-stream-1.4.4.tgz")
        (sha256
          (base32
            "1d8dvwmq5krcjakr2s7b2dwh8x6zgs8qj8mdwkgxsvx7rgqfr2qb"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-once))
    (home-page
      "https://github.com/mafintosh/end-of-stream")
    (synopsis
      "Call a callback when a readable/writable/duplex stream has completed or failed.")
    (description
      "Call a callback when a readable/writable/duplex stream has completed or failed.")
    (license license:expat)))

(define-public node-fs-constants
  (package
    (name "node-fs-constants")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fs-constants/-/fs-constants-1.0.0.tgz")
        (sha256
          (base32
            "1yn5qyvxf9i3zrfly77wgmi3j9fl61gh1i0jjgamnir43dz6v4z7"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/mafintosh/fs-constants")
    (synopsis
      "Require constants across node and the browser")
    (description
      "Require constants across node and the browser")
    (license license:expat)))

(define-public node-tar-stream
  (package
    (name "node-tar-stream")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/tar-stream/-/tar-stream-2.2.0.tgz")
        (sha256
          (base32
            "0nrrl6sgl5yazgllc8ryxpg083432xwqvqbrlqdl16sfszgq73rs"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream node-inherits node-fs-constants
            node-end-of-stream node-bl))
    (home-page
      "https://github.com/mafintosh/tar-stream")
    (synopsis
      "tar-stream is a streaming tar parser and generator and nothing else. It is streams2 and operates purely using streams which means you can easily extract/parse tarballs without ever hitting the file system.")
    (description
      "tar-stream is a streaming tar parser and generator and nothing else. It is streams2 and operates purely using streams which means you can easily extract/parse tarballs without ever hitting the file system.")
    (license license:expat)))

(define-public node-fs-realpath
  (package
    (name "node-fs-realpath")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fs.realpath/-/fs.realpath-1.0.0.tgz")
        (sha256
          (base32
            "174g5vay9jnd7h5q8hfdw6dnmwl1gdpn4a8sz0ysanhj2f3wp04y"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/isaacs/fs.realpath#readme")
    (synopsis
      "Use node's fs.realpath, but fall back to the JS implementation if the native one fails")
    (description
      "Use node's fs.realpath, but fall back to the JS implementation if the native one fails")
    (license license:isc)))

(define-public node-inflight
  (package
    (name "node-inflight")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/inflight/-/inflight-1.0.6.tgz")
        (sha256
          (base32
            "16w864087xsh3q7f5gm3754s7bpsb9fq3dhknk9nmbvlk3sxr7ss"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-wrappy node-once))
    (home-page "https://github.com/isaacs/inflight")
    (synopsis
      "Add callbacks to requests in flight to avoid async duplication")
    (description
      "Add callbacks to requests in flight to avoid async duplication")
    (license license:isc)))

(define-public node-balanced-match
  (package
    (name "node-balanced-match")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/balanced-match/-/balanced-match-2.0.0.tgz")
        (sha256
          (base32
            "1yf9zm930x9y5hhr3h75yfwa4dhwsxznqm0nx2zbxijsnavhy7jn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/juliangruber/balanced-match")
    (synopsis
      "Match balanced character pairs, like \"{\" and \"}\"")
    (description
      "Match balanced character pairs, like \"{\" and \"}\"")
    (license license:expat)))

(define-public node-brace-expansion
  (package
    (name "node-brace-expansion")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/brace-expansion/-/brace-expansion-2.0.1.tgz")
        (sha256
          (base32
            "0h81v1k9wr5v0xaw62dxxv457cv6xfzcvi41fbwhv98xliyi4r14"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-balanced-match))
    (home-page
      "https://github.com/juliangruber/brace-expansion")
    (synopsis
      "Brace expansion as known from sh/bash")
    (description
      "Brace expansion as known from sh/bash")
    (license license:expat)))

(define-public node-minimatch
  (package
    (name "node-minimatch")
    (version "3.0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/minimatch/-/minimatch-3.0.4.tgz")
        (sha256
          (base32
            "0wgammjc9myx0k0k3n9r9cjnv0r1j33cwqiy2fxx7w5nkgbj8sj2"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-brace-expansion))
    (home-page
      "https://github.com/isaacs/minimatch#readme")
    (synopsis "a glob matcher in javascript")
    (description "a glob matcher in javascript")
    (license license:isc)))

(define-public node-path-is-absolute
  (package
    (name "node-path-is-absolute")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/path-is-absolute/-/path-is-absolute-2.0.0.tgz")
        (sha256
          (base32
            "1l5lpknqz3jqsnkbj6y9l0aij61nyahwdzavlb5y7wf5p4xlx18h"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sindresorhus/path-is-absolute#readme")
    (synopsis
      "Node.js 0.12 path.isAbsolute() ponyfill")
    (description
      "Node.js 0.12 path.isAbsolute() ponyfill")
    (license license:expat)))

(define-public node-glob
  (package
    (name "node-glob")
    (version "7.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/glob/-/glob-7.2.0.tgz")
        (sha256
          (base32
            "06q5g9ja57jfca5qqbnad6lfhd7xv2kziy9xpqf97ipd5zg77vpn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-path-is-absolute
            node-once
            node-minimatch
            node-inherits
            node-inflight
            node-fs-realpath))
    (home-page
      "https://github.com/isaacs/node-glob#readme")
    (synopsis "a little globber")
    (description "a little globber")
    (license license:isc)))

(define-public node-lazystream
  (package
    (name "node-lazystream")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lazystream/-/lazystream-1.0.1.tgz")
        (sha256
          (base32
           "16kdggfv5zks4cpx6afasqpfwpl7vf8rxyh2kvk17s3qs2120lh8"))
        (snippet
         (with-imported-modules `((guix build utils))
           #~(begin
               (use-modules (guix build utils))
               (substitute* "lib/lazystream.js"
                 (("var PassThrough = require[(]'readable-stream/passthrough'[)];")
                  "var { PassThrough } = require('readable-stream');")))))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream))
    (home-page
      "https://github.com/jpommerening/node-lazystream")
    (synopsis "Open Node Streams on demand.")
    (description "Open Node Streams on demand.")
    (license license:expat)))

(define-public node-lodash-defaults
  (package
    (name "node-lodash-defaults")
    (version "4.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.defaults/-/lodash.defaults-4.2.0.tgz")
        (sha256
          (base32
            "1rpssxhnp61g6nz9nfvlc90i23rbj08ryvcl6g3vlcyvbh5266h8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.defaults` exported as a module.")
    (description
      "The lodash method `_.defaults` exported as a module.")
    (license license:expat)))

(define-public node-lodash-difference
  (package
    (name "node-lodash-difference")
    (version "4.5.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.difference/-/lodash.difference-4.5.0.tgz")
        (sha256
          (base32
            "0nbsibfwgiaja5x0mf46diyl51v42z4hj0dslxpk6a1wh3566rwy"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.difference` exported as a module.")
    (description
      "The lodash method `_.difference` exported as a module.")
    (license license:expat)))

(define-public node-lodash-flatten
  (package
    (name "node-lodash-flatten")
    (version "4.4.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.flatten/-/lodash.flatten-4.4.0.tgz")
        (sha256
          (base32
            "1z112mf1hlshmqs37w8x1yfm13dxjd7l9yf7y5csjj2jig6snfww"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.flatten` exported as a module.")
    (description
      "The lodash method `_.flatten` exported as a module.")
    (license license:expat)))

(define-public node-lodash-isplainobject
  (package
    (name "node-lodash-isplainobject")
    (version "4.0.6")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.isplainobject/-/lodash.isplainobject-4.0.6.tgz")
        (sha256
          (base32
            "1l3jmn7j4nzmv7pq24nawmjarb1gj647p786hjpjg5qkrvhj0r4q"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.isPlainObject` exported as a module.")
    (description
      "The lodash method `_.isPlainObject` exported as a module.")
    (license license:expat)))

(define-public node-lodash-union
  (package
    (name "node-lodash-union")
    (version "4.6.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.union/-/lodash.union-4.6.0.tgz")
        (sha256
          (base32
            "03i0ka26sdfaw5vc2grvqcf5r9l6lsyl4wlqhkgqp1pkc5s8681g"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.union` exported as a module.")
    (description
      "The lodash method `_.union` exported as a module.")
    (license license:expat)))

(define-public node-archiver-utils
  (package
    (name "node-archiver-utils")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/archiver-utils/-/archiver-utils-2.1.0.tgz")
        (sha256
          (base32
            "0i3ba81mik977a91nmhq5ziabzd6azn4id4qim8wpfnrwjn7vxr8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream
            node-normalize-path
            node-lodash-union
            node-lodash-isplainobject
            node-lodash-flatten
            node-lodash-difference
            node-lodash-defaults
            node-lazystream
            node-graceful-fs
            node-glob))
    (home-page
      "https://github.com/archiverjs/archiver-utils#readme")
    (synopsis "utility functions for archiver")
    (description "utility functions for archiver")
    (license license:expat)))

(define-public node-buffer-crc32
  (package
    (name "node-buffer-crc32")
    (version "0.2.13")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/buffer-crc32/-/buffer-crc32-0.2.13.tgz")
        (sha256
          (base32
            "0vf6adpqimbz921y729mf91pvghihj44js2ck5ci44zh4x21bl97"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/brianloveswords/buffer-crc32")
    (synopsis
      "A pure javascript CRC32 algorithm that plays nice with binary data")
    (description
      "A pure javascript CRC32 algorithm that plays nice with binary data")
    (license license:expat)))

(define-public node-printj
  (package
    (name "node-printj")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/printj/-/printj-1.3.0.tgz")
        (sha256
          (base32
            "174q1vkjhpgzvgsihihzccwbyps3xaxmazx3pa2q1ashiak8l2an"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://sheetjs.com/")
    (synopsis "Pure-JS printf")
    (description "Pure-JS printf")
    (license license:asl2.0)))

(define-public node-exit-on-epipe
  (package
    (name "node-exit-on-epipe")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/exit-on-epipe/-/exit-on-epipe-1.0.1.tgz")
        (sha256
          (base32
            "0sfb0xwj9giar420zdlnzaldqpdvq14xdq24zmkr4j5yk30bkb03"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/SheetJS/node-exit-on-epipe#readme")
    (synopsis "Cleanly exit process on EPIPE")
    (description "Cleanly exit process on EPIPE")
    (license license:asl2.0)))

(define-public node-crc-32
  (package
    (name "node-crc-32")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/crc-32/-/crc-32-1.2.0.tgz")
        (sha256
          (base32
            "0qndjw0lw3axa2n4vfp4i58gdd6bb0c9inpmzlsqb7zn8n4yn1c6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-exit-on-epipe node-printj))
    (home-page "http://sheetjs.com/opensource")
    (synopsis "Pure-JS CRC-32")
    (description "Pure-JS CRC-32")
    (license license:asl2.0)))

(define-public node-crc32-stream
  (package
    (name "node-crc32-stream")
    (version "4.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/crc32-stream/-/crc32-stream-4.0.2.tgz")
        (sha256
          (base32
            "0li16jmvcjp3wdpm2pdi5vp1ks3q8wmw2hix1l2nypfdg00qv478"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream node-crc-32))
    (home-page
      "https://github.com/archiverjs/node-crc32-stream")
    (synopsis "a streaming CRC32 checksumer")
    (description "a streaming CRC32 checksumer")
    (license license:expat)))

(define-public node-normalize-path
  (package
    (name "node-normalize-path")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/normalize-path/-/normalize-path-3.0.0.tgz")
        (sha256
          (base32
            "1zl49w6pdbgi96sgxpzr0gizcj261v6mxv8rx70csmf5ap53r60b"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jonschlinkert/normalize-path")
    (synopsis
      "Normalize slashes in a file path to be posix/unix-like forward slashes. Also condenses repeat slashes to a single slash and removes and trailing slashes, unless disabled.")
    (description
      "Normalize slashes in a file path to be posix/unix-like forward slashes. Also condenses repeat slashes to a single slash and removes and trailing slashes, unless disabled.")
    (license license:expat)))

(define-public node-compress-commons
  (package
    (name "node-compress-commons")
    (version "4.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/compress-commons/-/compress-commons-4.1.1.tgz")
        (sha256
          (base32
            "0qc5kkmzyk79h1j79c6zs7r1f4r7idgkyf96cqn1g7ghipz1945s"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream node-normalize-path node-crc32-stream
            node-buffer-crc32))
    (home-page
      "https://github.com/archiverjs/node-compress-commons")
    (synopsis
      "a library that defines a common interface for working with archive formats within node")
    (description
      "a library that defines a common interface for working with archive formats within node")
    (license license:expat)))

(define-public node-zip-stream
  (package
    (name "node-zip-stream")
    (version "4.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/zip-stream/-/zip-stream-4.1.0.tgz")
        (sha256
          (base32
            "1kg76yn7n2v59v72blpn3b4x4mbv42c6c51r93wpvxqpgv66yl6y"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-readable-stream node-compress-commons node-archiver-utils))
    (home-page
      "https://github.com/archiverjs/node-zip-stream")
    (synopsis "a streaming zip archive generator.")
    (description
      "a streaming zip archive generator.")
    (license license:expat)))

(define-public node-archiver
  (package
    (name "node-archiver")
    (version "5.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/archiver/-/archiver-5.3.0.tgz")
        (sha256
          (base32
            "15kzjbr4iy9zazkwhn7viq4q9gkbfvfzc2yvirrki6w3s474s5qw"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-zip-stream
            node-tar-stream
            node-readdir-glob
            node-readable-stream
            node-buffer-crc32
            node-async
            node-archiver-utils))
    (home-page
      "https://github.com/archiverjs/node-archiver")
    (synopsis
      "a streaming interface for archive generation")
    (description
      "a streaming interface for archive generation")
    (license license:expat)))

(define-public node-bn-js
  (package
    (name "node-bn-js")
    (version "5.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/bn.js/-/bn.js-5.2.0.tgz")
        (sha256
          (base32
            "1jmjj4hpvxzhl0g09zcyb7wvb8gc151l35b9kz155rrvajrc2x4w"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/indutny/bn.js")
    (synopsis
      "Big number implementation in pure javascript")
    (description
      "Big number implementation in pure javascript")
    (license license:expat)))

(define-public node-minimalistic-assert
  (package
    (name "node-minimalistic-assert")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/minimalistic-assert/-/minimalistic-assert-1.0.1.tgz")
        (sha256
          (base32
            "187k0gdixs2zqkfvv6lm72w90c15rin2kx2zkyly7nyn8z4j4rgi"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/calvinmetcalf/minimalistic-assert")
    (synopsis "minimalistic-assert ===")
    (description "minimalistic-assert ===")
    (license license:isc)))

(define-public node-safer-buffer
  (package
    (name "node-safer-buffer")
    (version "2.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/safer-buffer/-/safer-buffer-2.1.2.tgz")
        (sha256
          (base32
            "1cx383s7vchfac8jlg3mnb820hkgcvhcpfn9w4f0g61vmrjjz0bq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/ChALkeR/safer-buffer#readme")
    (synopsis
      "Modern Buffer API polyfill without footguns")
    (description
      "Modern Buffer API polyfill without footguns")
    (license license:expat)))

(define-public node-asn1-js
  (package
    (name "node-asn1-js")
    (version "5.4.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/asn1.js/-/asn1.js-5.4.1.tgz")
        (sha256
          (base32
            "18f2z3cg0gljsv2ycv8gl6r8cbdhdx6mizn9xkl4w9aza45rxicw"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-safer-buffer node-minimalistic-assert node-inherits
            node-bn-js))
    (home-page "https://github.com/indutny/asn1.js")
    (synopsis "ASN.1 encoder and decoder")
    (description "ASN.1 encoder and decoder")
    (license license:expat)))

(define-public node-bcryptjs
  (package
    (name "node-bcryptjs")
    (version "2.4.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/bcryptjs/-/bcryptjs-2.4.3.tgz")
        (sha256
          (base32
            "1qxjsxln82j30gikyz85qjpic3p5z7dqh4xnx3akbdcn5xpa8vqk"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/dcodeIO/bcrypt.js#readme")
    (synopsis
      "Optimized bcrypt in plain JavaScript with zero dependencies. Compatible to 'bcrypt'.")
    (description
      "Optimized bcrypt in plain JavaScript with zero dependencies. Compatible to 'bcrypt'.")
    (license license:expat)))

(define-public node-content-type
  (package
    (name "node-content-type")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/content-type/-/content-type-1.0.4.tgz")
        (sha256
          (base32
            "0j0rkv7yvpdyk4hfgklx95g75aksljaxraz9lhkb5chhvxqkygnv"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/content-type#readme")
    (synopsis
      "Create and parse HTTP Content-Type header")
    (description
      "Create and parse HTTP Content-Type header")
    (license license:expat)))

(define-public node-ee-first
  (package
    (name "node-ee-first")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ee-first/-/ee-first-1.1.1.tgz")
        (sha256
          (base32
            "175r500n567a04qmswzw5hkgdnika3dvn63n284jlar2gvmyhj2i"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jonathanong/ee-first")
    (synopsis
      "return the first event in a set of ee/event pairs")
    (description
      "return the first event in a set of ee/event pairs")
    (license license:expat)))

(define-public node-on-finished
  (package
    (name "node-on-finished")
    (version "2.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/on-finished/-/on-finished-2.3.0.tgz")
        (sha256
          (base32
            "0sv1js3fk0ag46ln3a7cwhannacqxyl694zkb1qy53fdd630sr59"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-ee-first))
    (home-page
      "https://github.com/jshttp/on-finished")
    (synopsis
      "Execute a callback when a request closes, finishes, or errors")
    (description
      "Execute a callback when a request closes, finishes, or errors")
    (license license:expat)))

(define-public node-call-bind
  (package
    (name "node-call-bind")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/call-bind/-/call-bind-1.0.2.tgz")
        (sha256
          (base32
            "06il2ki0bprw4s0a12bwnzwjc8gwwcw6f0cda0jyvj46sj56z5f3"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-get-intrinsic node-function-bind))
    (home-page
      "https://github.com/ljharb/call-bind#readme")
    (synopsis "Robustly `.call.bind()` a function")
    (description
      "Robustly `.call.bind()` a function")
    (license license:expat)))

(define-public node-function-bind
  (package
    (name "node-function-bind")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/function-bind/-/function-bind-1.1.1.tgz")
        (sha256
          (base32
            "10p0s9ypggwmazik4azdhywjnnayagnjxk10cjzsrhxlk1y2wm9d"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/Raynos/function-bind")
    (synopsis
      "Implementation of Function.prototype.bind")
    (description
      "Implementation of Function.prototype.bind")
    (license license:expat)))

(define-public node-has
  (package
    (name "node-has")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/has/-/has-1.0.3.tgz")
        (sha256
          (base32
            "0wsmn2vcbqb23xpbzxipjd7xcdljid2gwnwl7vn5hkp0zkpgk363"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-function-bind))
    (home-page "https://github.com/tarruda/has")
    (synopsis
      "Object.prototype.hasOwnProperty.call shortcut")
    (description
      "Object.prototype.hasOwnProperty.call shortcut")
    (license license:expat)))

(define-public node-has-symbols
  (package
    (name "node-has-symbols")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/has-symbols/-/has-symbols-1.0.2.tgz")
        (sha256
          (base32
            "1pb5n4zcrq9xzhjc867902af7p9hvdb7p0imlp40gj4kg6j4wp00"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/inspect-js/has-symbols#readme")
    (synopsis
      "Determine if the JS environment has Symbol support. Supports spec, or shams.")
    (description
      "Determine if the JS environment has Symbol support. Supports spec, or shams.")
    (license license:expat)))

(define-public node-get-intrinsic
  (package
    (name "node-get-intrinsic")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/get-intrinsic/-/get-intrinsic-1.1.1.tgz")
        (sha256
          (base32
            "06vvw0pyv22bv1ryb5hjwhrfxjsi2rj220z46cw2qv2smy7rc6ss"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-has-symbols node-has node-function-bind))
    (home-page
      "https://github.com/ljharb/get-intrinsic#readme")
    (synopsis
      "Get and robustly cache all JS language-level intrinsics at first require time")
    (description
      "Get and robustly cache all JS language-level intrinsics at first require time")
    (license license:expat)))

(define-public node-object-inspect
  (package
    (name "node-object-inspect")
    (version "1.11.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/object-inspect/-/object-inspect-1.11.0.tgz")
        (sha256
          (base32
            "00w6bhqrlxab2s1fin6nq6r5yhlz24z0dwi538rlxczdavlay6bm"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/inspect-js/object-inspect")
    (synopsis
      "string representations of objects in node and the browser")
    (description
      "string representations of objects in node and the browser")
    (license license:expat)))

(define-public node-side-channel
  (package
    (name "node-side-channel")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/side-channel/-/side-channel-1.0.4.tgz")
        (sha256
          (base32
            "0dfp0r5j0i847w1q88riq2yp4mc0wrs0a6ys57j1m21nlny88arm"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-object-inspect node-get-intrinsic node-call-bind))
    (home-page
      "https://github.com/ljharb/side-channel#readme")
    (synopsis
      "Store information about any JS value in a side channel. Uses WeakMap if available.")
    (description
      "Store information about any JS value in a side channel. Uses WeakMap if available.")
    (license license:expat)))

(define-public node-qs
  (package
    (name "node-qs")
    (version "6.10.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/qs/-/qs-6.10.1.tgz")
        (sha256
          (base32
            "16l858c1hk25sp8d93kcsh9a44fqjggmx596df705p4v0ap73zs8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-side-channel))
    (home-page "https://github.com/ljharb/qs")
    (synopsis
      "A querystring parser that supports nesting and arrays, with a depth limit")
    (description
      "A querystring parser that supports nesting and arrays, with a depth limit")
    (license license:bsd-3)))

(define-public node-bytes
  (package
    (name "node-bytes")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/bytes/-/bytes-3.1.1.tgz")
        (sha256
          (base32
            "1p5bancsfyjqvr07rj1npxab07qdrww08ds049kcjr458b6b80n1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/visionmedia/bytes.js#readme")
    (synopsis
      "Utility to parse a string bytes to bytes and vice-versa")
    (description
      "Utility to parse a string bytes to bytes and vice-versa")
    (license license:expat)))

(define-public node-depd
  (package
    (name "node-depd")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/depd/-/depd-2.0.0.tgz")
        (sha256
          (base32
            "19yl2piwl0ci2lvn5j5sk0z4nbldj6apsrqds3ql2d09aqh8m998"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/dougwilson/nodejs-depd#readme")
    (synopsis "Deprecate all the things")
    (description "Deprecate all the things")
    (license license:expat)))

(define-public node-setprototypeof
  (package
    (name "node-setprototypeof")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/setprototypeof/-/setprototypeof-1.2.0.tgz")
        (sha256
          (base32
            "1qnzx8bl8h1vga28pf59mjd52wvh1hf3ma18d4zpwmijlrpcqfy8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/wesleytodd/setprototypeof")
    (synopsis
      "A small polyfill for Object.setprototypeof")
    (description
      "A small polyfill for Object.setprototypeof")
    (license license:isc)))

(define-public node-statuses
  ;; http-errors says "statuses": ">= 1.5.0 < 2"
  (package
    (name "node-statuses")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/statuses/-/statuses-1.5.0.tgz")
        (sha256
          (base32
            "0g6ydb53k8b5rhll1z667riba9454ipkl4hgkc5vzc62l4h10g18"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/statuses#readme")
    (synopsis "HTTP status utility")
    (description "HTTP status utility")
    (license license:expat)))

(define-public node-toidentifier
  (package
    (name "node-toidentifier")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/toidentifier/-/toidentifier-1.0.1.tgz")
        (sha256
          (base32
            "021fp42m51qbqbqabwhxky8bkfkkwza65lqiz7d2gqwd91vwqvqq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/component/toidentifier#readme")
    (synopsis
      "Convert a string of words to a JavaScript identifier")
    (description
      "Convert a string of words to a JavaScript identifier")
    (license license:expat)))

(define-public node-http-errors
  (package
    (name "node-http-errors")
    (version "1.8.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/http-errors/-/http-errors-1.8.1.tgz")
        (sha256
          (base32
            "14kydkrxnvpc267g3d1vjzsxbzq6lcrc8dh1crdvykcx59iwi12n"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-toidentifier node-statuses node-setprototypeof
            node-inherits node-depd))
    (home-page
      "https://github.com/jshttp/http-errors#readme")
    (synopsis "Create HTTP error objects")
    (description "Create HTTP error objects")
    (license license:expat)))

(define-public node-iconv-lite
  (package
    (name "node-iconv-lite")
    (version "0.6.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/iconv-lite/-/iconv-lite-0.6.3.tgz")
        (sha256
          (base32
            "1x681ziwavjjn09j4858fl3h3xi90vf512k5zwg06kwriwafq9vi"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-safer-buffer))
    (home-page
      "https://github.com/ashtuchkin/iconv-lite")
    (synopsis
      "Convert character encodings in pure javascript.")
    (description
      "Convert character encodings in pure javascript.")
    (license license:expat)))

(define-public node-unpipe
  (package
    (name "node-unpipe")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/unpipe/-/unpipe-1.0.0.tgz")
        (sha256
          (base32
            "1dnzbqfmchls4jyvkw0wnkc09pig98y66zzsy3lizgyls435xyrd"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/stream-utils/unpipe")
    (synopsis
      "Unpipe a stream from all destinations")
    (description
      "Unpipe a stream from all destinations")
    (license license:expat)))

(define-public node-raw-body
  (package
    (name "node-raw-body")
    (version "2.4.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/raw-body/-/raw-body-2.4.2.tgz")
        (sha256
          (base32
            "1pml6rga7f8i751qdzfjzprzgdc7v61zfjqvx62z5fmp17hgmqx2"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-unpipe node-iconv-lite node-http-errors node-bytes))
    (home-page
      "https://github.com/stream-utils/raw-body#readme")
    (synopsis
      "Get and validate the raw body of a readable stream.")
    (description
      "Get and validate the raw body of a readable stream.")
    (license license:expat)))

(define-public node-media-typer
  (package
    (name "node-media-typer")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/media-typer/-/media-typer-1.1.0.tgz")
        (sha256
          (base32
            "1ghrgjcv59qna3h37himz6p7qsby9vki3gjrnv7r5z0y3lg57p5m"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/media-typer#readme")
    (synopsis
      "Simple RFC 6838 media type parser and formatter")
    (description
      "Simple RFC 6838 media type parser and formatter")
    (license license:expat)))

(define-public node-mime-db
  (package
    (name "node-mime-db")
    (version "1.51.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/mime-db/-/mime-db-1.51.0.tgz")
        (sha256
          (base32
            "1gx7hgl85s8hvkmiwbxpqpanxnpk0cm83a99j9f87h7p7s412vmp"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/mime-db#readme")
    (synopsis "Media Type Database")
    (description "Media Type Database")
    (license license:expat)))

(define-public node-mime-types
  (package
    (name "node-mime-types")
    (version "2.1.34")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/mime-types/-/mime-types-2.1.34.tgz")
        (sha256
          (base32
            "1picz3whrjz6453qjxyb4lkjfk7jrdjcy092nfkmqw0f3mjncnk6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-mime-db))
    (home-page
      "https://github.com/jshttp/mime-types#readme")
    (synopsis
      "The ultimate javascript content-type utility.")
    (description
      "The ultimate javascript content-type utility.")
    (license license:expat)))

(define-public node-type-is
  (package
    (name "node-type-is")
    (version "1.6.18")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/type-is/-/type-is-1.6.18.tgz")
        (sha256
          (base32
            "1bn3gl9vd67cq3wl2cvq686zskl2xx6lxz5kp9w47qc06f2vbnll"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-mime-types node-media-typer))
    (home-page
      "https://github.com/jshttp/type-is#readme")
    (synopsis "Infer the content-type of a request.")
    (description
      "Infer the content-type of a request.")
    (license license:expat)))

(define-public node-body-parser
  (package
    (name "node-body-parser")
    (version "1.19.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/body-parser/-/body-parser-1.19.0.tgz")
        (sha256
          (base32
            "18vy9ymfhmp3sisf3nxvxi0rl2xll8jp3wd84imnj8swyny5jm02"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-type-is
            node-raw-body
            node-qs
            node-on-finished
            node-iconv-lite
            node-http-errors
            node-depd
            node-debug
            node-content-type
            node-bytes))
    (home-page
      "https://github.com/expressjs/body-parser#readme")
    (synopsis "Node.js body parsing middleware")
    (description "Node.js body parsing middleware")
    (license license:expat)))

(define-public node-callsites
  ;; revert to 3.1.0 to avoid ERR_REQUIRE_ESM
  (package
    (name "node-callsites")
    (version "3.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/callsites/-/callsites-3.1.0.tgz")
        (sha256
          (base32
            "17f8wf2bxv2s4k36ld1x4y2rbkh9a8vsmbhwab470vmz6ayl40hp"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sindresorhus/callsites#readme")
    (synopsis
      "Get callsites from the V8 stack trace API")
    (description
      "Get callsites from the V8 stack trace API")
    (license license:expat)))

(define-public node-negotiator
  (package
    (name "node-negotiator")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/negotiator/-/negotiator-0.6.2.tgz")
        (sha256
          (base32
            "1zpx97aamn044id45ljzalxpa9l1dm0gn260w5hwjg7998vxjz68"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/negotiator#readme")
    (synopsis "HTTP content negotiation")
    (description "HTTP content negotiation")
    (license license:expat)))

(define-public node-accepts
  (package
    (name "node-accepts")
    (version "1.3.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/accepts/-/accepts-1.3.7.tgz")
        (sha256
          (base32
            "1s550j2wkqhsgpj841fww4bdck0w67rk80qb859nwqy8x7khsycs"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-negotiator node-mime-types))
    (home-page
      "https://github.com/jshttp/accepts#readme")
    (synopsis "Higher-level content negotiation")
    (description "Higher-level content negotiation")
    (license license:expat)))

(define-public node-compressible
  (package
    (name "node-compressible")
    (version "2.0.18")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/compressible/-/compressible-2.0.18.tgz")
        (sha256
          (base32
            "1mjr4010mlv5qim0n2by0gz5wiayscy7anaf712q19i4ha79syx5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-mime-db))
    (home-page
      "https://github.com/jshttp/compressible#readme")
    (synopsis
      "Compressible Content-Type / mime checking")
    (description
      "Compressible Content-Type / mime checking")
    (license license:expat)))

(define-public node-on-headers
  (package
    (name "node-on-headers")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/on-headers/-/on-headers-1.0.2.tgz")
        (sha256
          (base32
            "0scxilslkpb6mssm9znmd78wddn4l1d6pvyx5223y9bcn188pgd1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/on-headers#readme")
    (synopsis
      "Execute a listener when a response is about to write headers")
    (description
      "Execute a listener when a response is about to write headers")
    (license license:expat)))

(define-public node-vary
  (package
    (name "node-vary")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/vary/-/vary-1.1.2.tgz")
        (sha256
          (base32
            "0wbf4kmfyzc23dc0vjcmymkd1ks50z5gvv23lkkkayipf438cy3k"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/vary#readme")
    (synopsis "Manipulate the HTTP Vary header")
    (description "Manipulate the HTTP Vary header")
    (license license:expat)))

(define-public node-compression
  (package
    (name "node-compression")
    (version "1.7.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/compression/-/compression-1.7.4.tgz")
        (sha256
          (base32
            "0g0m4p94mdnm9jdxd8nsfrsqsb0zrb81czyx81di0iy0dv10q9nw"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-vary
            node-safe-buffer
            node-on-headers
            node-debug
            node-compressible
            node-bytes
            node-accepts))
    (home-page
      "https://github.com/expressjs/compression#readme")
    (synopsis "Node.js compression middleware")
    (description "Node.js compression middleware")
    (license license:expat)))

(define-public node-minimist
  (package
    (name "node-minimist")
    (version "1.2.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/minimist/-/minimist-1.2.5.tgz")
        (sha256
          (base32
            "0l23rq2pam1khc06kd7fv0ys2cq0mlgs82dxjxjfjmlksgj0r051"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/substack/minimist")
    (synopsis "parse argument options")
    (description "parse argument options")
    (license license:expat)))

(define-public node-json5
  (package
    (name "node-json5")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/json5/-/json5-2.2.0.tgz")
        (sha256
          (base32
            "1zmiq12f6wb8d98l87si6ys2qfnv16wwib27ymcdy5r4s4mmm1rp"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-minimist))
    (home-page "http://json5.org/")
    (synopsis "JSON for humans.")
    (description "JSON for humans.")
    (license license:expat)))

(define-public node-config
  (package
    (name "node-config")
    (version "3.3.6")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/config/-/config-3.3.6.tgz")
        (sha256
          (base32
            "0x5w5y6ssd3rfw6d7xd546a3z3zjf8ngn8i17rf8w8zv9j4axwch"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-json5))
    (home-page
      "http://lorenwest.github.com/node-config")
    (synopsis
      "Configuration control for production node deployments")
    (description
      "Configuration control for production node deployments")
    (license license:expat)))

(define-public node-country-list
  (package
    (name "node-country-list")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/country-list/-/country-list-2.2.0.tgz")
        (sha256
          (base32
            "0750mk2x82vx4sygnf93l3pf9k6qfpci7ckvj270f0vk7i746rhl"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/fannarsh/country-list")
    (synopsis
      "Maps ISO 3166-1-alpha-2 codes to English country names and vice versa.")
    (description
      "Maps ISO 3166-1-alpha-2 codes to English country names and vice versa.")
    (license license:expat)))

(define-public node-csv-parse
  (package
    (name "node-csv-parse")
    (version "4.9.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/csv-parse/-/csv-parse-4.9.1.tgz")
        (sha256
          (base32
            "1vd21p6958kfz5gqqcggm0ysiixm5ghqsxz2v2irfarbxdqwva7c"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://csv.js.org/parse/")
    (synopsis
      "CSV parsing implementing the Node.js `stream.Transform` API")
    (description
      "CSV parsing implementing the Node.js `stream.Transform` API")
    (license license:expat)))

(define-public node-array-flatten
  ;; specific version needed for node-express
  (package
    (name "node-array-flatten")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/array-flatten/-/array-flatten-1.1.1.tgz")
        (sha256
          (base32
            "1v96cj6w6f7g61c7fjfkxpkbbfkxl2ksh5zm7y5mfp96xivi5jhs"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/blakeembrey/array-flatten")
    (synopsis "Flatten nested arrays")
    (description "Flatten nested arrays")
    (license license:expat)))

(define-public node-content-disposition
  (package
    (name "node-content-disposition")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/content-disposition/-/content-disposition-0.5.3.tgz")
        (sha256
          (base32
            "1gnpp7mvy8r2k8a4kx43rhg8l85n2g0rfvyfn7wai2k42zk08q4y"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-safe-buffer))
    (home-page
      "https://github.com/jshttp/content-disposition#readme")
    (synopsis
      "Create and parse Content-Disposition header")
    (description
      "Create and parse Content-Disposition header")
    (license license:expat)))

(define-public node-cookie
  (package
    (name "node-cookie")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/cookie/-/cookie-0.4.1.tgz")
        (sha256
          (base32
            "056dgn3k3h4fg5k55d7g1qdbhyic74153mx70zbjknilyr8b7j4c"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/cookie#readme")
    (synopsis
      "HTTP server cookie parsing and serialization")
    (description
      "HTTP server cookie parsing and serialization")
    (license license:expat)))

(define-public node-cookie-signature
  (package
    (name "node-cookie-signature")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/cookie-signature/-/cookie-signature-1.1.0.tgz")
        (sha256
          (base32
            "1h7mc58ia4wznkhkss2ii2gxj70sslbxhrhwpxm0kj9fm7dlxd83"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/visionmedia/node-cookie-signature#readme")
    (synopsis "Sign and unsign cookies")
    (description "Sign and unsign cookies")
    (license license:expat)))

(define-public node-finalhandler
  (package
    (name "node-finalhandler")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/finalhandler/-/finalhandler-1.1.2.tgz")
        (sha256
          (base32
            "1kq09av23a28ig4kd727librz6dmiwjycsr7nh5cg0vjy0bk31pj"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-unpipe
            node-statuses
            node-parseurl
            node-on-finished
            node-escape-html
            node-encodeurl
            node-debug))
    (home-page
      "https://github.com/pillarjs/finalhandler#readme")
    (synopsis "Node.js final http responder")
    (description "Node.js final http responder")
    (license license:expat)))

(define-public node-merge-descriptors
  (package
    (name "node-merge-descriptors")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/merge-descriptors/-/merge-descriptors-1.0.1.tgz")
        (sha256
          (base32
            "02d4fqgiz4cc33vbcdlah9rafj5vc2z0iy05sc9wpfpzri3kn2l8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/component/merge-descriptors")
    (synopsis "Merge objects using descriptors")
    (description "Merge objects using descriptors")
    (license license:expat)))

(define-public node-methods
  (package
    (name "node-methods")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/methods/-/methods-1.1.2.tgz")
        (sha256
          (base32
            "0g50ci0gc8r8kq1i06q078gw7azkakp7j3yw5qfi6gq2qk8hdlnz"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/jshttp/methods")
    (synopsis "HTTP methods that node supports")
    (description "HTTP methods that node supports")
    (license license:expat)))

(define-public node-path-to-regexp
  ;; v4 changes to esm
  (package
    (name "node-path-to-regexp")
    (version "3.2.0")
    (source
      (origin
        (method url-fetch)
        (uri
         "https://registry.npmjs.org/path-to-regexp/-/path-to-regexp-3.2.0.tgz")
        (sha256
          (base32
            "0vqcmzp8hd8b7qn92jwzal7igdagpig9aj2gfc5vb5jhhs52amnp"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/pillarjs/path-to-regexp#readme")
    (synopsis "Express style path to RegExp utility")
    (description
      "Express style path to RegExp utility")
    (license license:expat)))

(define-public node-forwarded
  (package
    (name "node-forwarded")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/forwarded/-/forwarded-0.2.0.tgz")
        (sha256
          (base32
            "168w8dhfqp12llh2w802dm3z1fcarsacsksyvdccnpxqbzlmsnlv"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/forwarded#readme")
    (synopsis "Parse HTTP X-Forwarded-For header")
    (description "Parse HTTP X-Forwarded-For header")
    (license license:expat)))

(define-public node-ipaddr-js
  (package
    (name "node-ipaddr-js")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ipaddr.js/-/ipaddr.js-2.0.1.tgz")
        (sha256
          (base32
            "1appz2l2jhwfaidcayz1f2d6s7axv4fsypnwpcl689rvhs430crj"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/whitequark/ipaddr.js#readme")
    (synopsis
      "A library for manipulating IPv4 and IPv6 addresses in JavaScript.")
    (description
      "A library for manipulating IPv4 and IPv6 addresses in JavaScript.")
    (license license:expat)))

(define-public node-proxy-addr
  (package
    (name "node-proxy-addr")
    (version "2.0.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/proxy-addr/-/proxy-addr-2.0.7.tgz")
        (sha256
          (base32
            "1na6xrmlga7qjd55gfhnp7m8qg43nynzg5ds54s76kkd9zrvdld0"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-ipaddr-js node-forwarded))
    (home-page
      "https://github.com/jshttp/proxy-addr#readme")
    (synopsis "Determine address of proxied request")
    (description
      "Determine address of proxied request")
    (license license:expat)))

(define-public node-parseurl
  (package
    (name "node-parseurl")
    (version "1.3.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/parseurl/-/parseurl-1.3.3.tgz")
        (sha256
          (base32
            "06h2bx1rilkdir3v9jlg94r1q2fn895s0vxjjs0wx5z027x4pvsn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/pillarjs/parseurl#readme")
    (synopsis "parse a url with memoization")
    (description "parse a url with memoization")
    (license license:expat)))

(define-public node-destroy
  (package
    (name "node-destroy")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/destroy/-/destroy-1.0.4.tgz")
        (sha256
          (base32
            "0miy8a2wfc77l4j08hv4qk5v9a7566igql71zw7ds3v5yhi9cmsv"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/stream-utils/destroy")
    (synopsis "destroy a stream if possible")
    (description "destroy a stream if possible")
    (license license:expat)))

(define-public node-encodeurl
  (package
    (name "node-encodeurl")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/encodeurl/-/encodeurl-1.0.2.tgz")
        (sha256
          (base32
            "13afvicx42ha4k29571sg0i4b76xjggyxvmmmibm258ipf6mjinb"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/pillarjs/encodeurl#readme")
    (synopsis
      "Encode a URL to a percent-encoded form, excluding already-encoded sequences")
    (description
      "Encode a URL to a percent-encoded form, excluding already-encoded sequences")
    (license license:expat)))

(define-public node-escape-html
  (package
    (name "node-escape-html")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/escape-html/-/escape-html-1.0.3.tgz")
        (sha256
          (base32
            "0rh35dvab1wbp87dy1m6rynbcb9rbs5kry7jk17ixyxx7if1a0d1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/component/escape-html")
    (synopsis "Escape string for use in HTML")
    (description "Escape string for use in HTML")
    (license license:expat)))

(define-public node-etag
  (package
    (name "node-etag")
    (version "1.8.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/etag/-/etag-1.8.1.tgz")
        (sha256
          (base32
            "1bqgznlsrqcmxnhmnqkhwzcrqfaalxmfxzly1ikaplkkm5w6ragn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/etag#readme")
    (synopsis "Create simple HTTP ETags")
    (description "Create simple HTTP ETags")
    (license license:expat)))

(define-public node-fresh
  (package
    (name "node-fresh")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fresh/-/fresh-0.5.2.tgz")
        (sha256
          (base32
            "0k44badcxkwy202kz404w078l660f65jaijg473xv38ay3wpdri5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/fresh#readme")
    (synopsis "HTTP response freshness testing")
    (description "HTTP response freshness testing")
    (license license:expat)))

(define-public node-mime
  (package
    (name "node-mime")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/mime/-/mime-1.6.0.tgz")
        (sha256
          (base32
            "16iprk4h6nh780mvfv0p93k3yvj7jrq2qs92niaw6yk11qwi0li1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/broofa/mime#readme")
    (synopsis
      "A comprehensive library for mime-type mapping")
    (description
      "A comprehensive library for mime-type mapping")
    (license license:expat)))

(define-public node-range-parser
  (package
    (name "node-range-parser")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/range-parser/-/range-parser-1.2.1.tgz")
        (sha256
          (base32
            "09prs852snwqr9cfcrybm7ysl0z1wka9dh4dwc4v1415cvi6cllh"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jshttp/range-parser#readme")
    (synopsis "Range header field string parser")
    (description "Range header field string parser")
    (license license:expat)))

(define-public node-send
  (package
    (name "node-send")
    (version "0.17.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/send/-/send-0.17.1.tgz")
        (sha256
          (base32
            "08ds2hx476lhafl051sh03kaiilkariwa209d02gbcm6xygb8m6k"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-statuses
            node-range-parser
            node-on-finished
            node-ms
            node-mime
            node-http-errors
            node-fresh
            node-etag
            node-escape-html
            node-encodeurl
            node-destroy
            node-depd
            node-debug))
    (home-page
      "https://github.com/pillarjs/send#readme")
    (synopsis
      "Better streaming static file server with Range and conditional-GET support")
    (description
      "Better streaming static file server with Range and conditional-GET support")
    (license license:expat)))

(define-public node-serve-static
  (package
    (name "node-serve-static")
    (version "1.14.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/serve-static/-/serve-static-1.14.1.tgz")
        (sha256
          (base32
            "1pajpv2acavzq2bpj3rggrvik00k9wyav2fvg5yvmvgmwy3kbxh1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-send node-parseurl node-escape-html node-encodeurl))
    (home-page
      "https://github.com/expressjs/serve-static#readme")
    (synopsis "Serve static files")
    (description "Serve static files")
    (license license:expat)))

(define-public node-utils-merge
  (package
    (name "node-utils-merge")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/utils-merge/-/utils-merge-1.0.1.tgz")
        (sha256
          (base32
            "0djhmrfzxpdhscg4pkgnsd39cddpwpkkw1w2f8mp2xfsxn7mvnfy"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jaredhanson/utils-merge#readme")
    (synopsis "merge() utility function")
    (description "merge() utility function")
    (license license:expat)))

(define-public node-express
  (package
    (name "node-express")
    (version "4.17.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/express/-/express-4.17.1.tgz")
        (sha256
          (base32
            "1a82maaz62wcw1dsv863ikjp6gpyxka0b1g2sldnvxg3qi8va016"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-vary
            node-utils-merge
            node-type-is
            node-statuses
            node-setprototypeof
            node-serve-static
            node-send
            node-safe-buffer
            node-range-parser
            node-qs
            node-proxy-addr
            node-path-to-regexp
            node-parseurl
            node-on-finished
            node-methods
            node-merge-descriptors
            node-fresh
            node-finalhandler
            node-etag
            node-escape-html
            node-encodeurl
            node-depd
            node-debug
            node-cookie-signature
            node-cookie
            node-content-type
            node-content-disposition
            node-body-parser
            node-array-flatten
            node-accepts))
    (home-page "http://expressjs.com/")
    (synopsis
      "Fast, unopinionated, minimalist web framework")
    (description
      "Fast, unopinionated, minimalist web framework")
    (license license:expat)))

(define-public node-streamsearch
  (package
    (name "node-streamsearch")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/streamsearch/-/streamsearch-0.1.2.tgz")
        (sha256
          (base32
            "1sqjavakk11j04fnvl34zbsl1vqzlf6n8y2mmiz9mprpnrdcn2z3"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://www.npmjs.com/package/node-streamsearch")
    (synopsis
      "Streaming Boyer-Moore-Horspool searching for node.js")
    (description
      "Streaming Boyer-Moore-Horspool searching for node.js")
    (license #f)))

(define-public node-dicer
  (package
    (name "node-dicer")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/dicer/-/dicer-0.3.0.tgz")
        (sha256
          (base32
            "0xx9bgkqkl5n9wgmqs5wqyhbhcdw4x216a59bcq8sa15qxz5p2p9"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-streamsearch))
    (home-page
      "https://github.com/mscdex/dicer#readme")
    (synopsis
      "A very fast streaming multipart parser for node.js")
    (description
      "A very fast streaming multipart parser for node.js")
    (license #f)))

(define-public node-busboy
  (package
    (name "node-busboy")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/busboy/-/busboy-0.3.1.tgz")
        (sha256
          (base32
            "0xx6jy6crbrpymggkql98p1yr5laanyawklli6g3hc7slpkf0fw2"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-dicer))
    (home-page
      "https://github.com/mscdex/busboy#readme")
    (synopsis
      "A streaming parser for HTML form data for node.js")
    (description
      "A streaming parser for HTML form data for node.js")
    (license #f)))

(define-public node-express-fileupload
  (package
    (name "node-express-fileupload")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/express-fileupload/-/express-fileupload-1.2.1.tgz")
        (sha256
          (base32
            "0qsngymj73hm82azr0pacbx1llxwq3p3d9x0rv3bajrhn9pwv664"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-busboy))
    (home-page
      "https://github.com/richardgirges/express-fileupload#readme")
    (synopsis
      "Simple express file upload middleware that wraps around Busboy")
    (description
      "Simple express file upload middleware that wraps around Busboy")
    (license license:expat)))

(define-public node-neo-async
  (package
    (name "node-neo-async")
    (version "2.6.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/neo-async/-/neo-async-2.6.2.tgz")
        (sha256
          (base32
            "1x3dxkdflv3ibgab2aj91pkrqpzj62c38dk3x0zvv2qpyh6p18a3"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/suguru03/neo-async")
    (synopsis
      "Neo-Async is a drop-in replacement for Async, it almost fully covers its functionality and runs faster ")
    (description
      "Neo-Async is a drop-in replacement for Async, it almost fully covers its functionality and runs faster ")
    (license license:expat)))

(define-public node-source-map
  (package
    (name "node-source-map")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/source-map/-/source-map-0.7.3.tgz")
        (sha256
          (base32
            "0gl32nnsbimw7m9m7m2jc93gmcgp7m021szhwk1k6ivppa3vfsdd"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/mozilla/source-map")
    (synopsis "Generates and consumes source maps")
    (description
      "Generates and consumes source maps")
    (license license:bsd-3)))

(define-public node-wordwrap
  (package
    (name "node-wordwrap")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/wordwrap/-/wordwrap-1.0.0.tgz")
        (sha256
          (base32
            "0f08g3gb6si97h2faw175isf8k7hjbnw5rr2bm4wcr1141lzd0f3"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/substack/node-wordwrap#readme")
    (synopsis
      "Wrap those words. Show them at what columns to start and stop.")
    (description
      "Wrap those words. Show them at what columns to start and stop.")
    (license license:expat)))

(define-public node-uglify-js
  (package
    (name "node-uglify-js")
    (version "3.14.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/uglify-js/-/uglify-js-3.14.3.tgz")
        (sha256
          (base32
            "1c9hzv51n0x3giisa6g03ra8ih3pm5yw535418pam3gyhkp7h1s4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/mishoo/UglifyJS#readme")
    (synopsis
      "JavaScript parser, mangler/compressor and beautifier toolkit")
    (description
      "JavaScript parser, mangler/compressor and beautifier toolkit")
    (license #f)))

(define-public node-handlebars
  (package
    (name "node-handlebars")
    (version "4.7.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/handlebars/-/handlebars-4.7.7.tgz")
        (sha256
          (base32
            "1ywiw6l3lnf1h11018x1kjap03id9bab7saqr92cyxpb495k1nfn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-uglify-js node-wordwrap node-source-map node-neo-async
            node-minimist))
    (home-page "http://www.handlebarsjs.com/")
    (synopsis
      "Handlebars provides the power necessary to let you build semantic templates effectively with no frustration")
    (description
      "Handlebars provides the power necessary to let you build semantic templates effectively with no frustration")
    (license license:expat)))

(define-public node-express-handlebars
  (package
    (name "node-express-handlebars")
    (version "5.3.5")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/express-handlebars/-/express-handlebars-5.3.5.tgz")
       (sha256
        (base32
         "1r3ga3m02ni99axvylc4dwp4whvd4v65xz5rl4vpdwdlpd5902wy"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-handlebars node-graceful-fs node-glob))
    (home-page
      "https://github.com/express-handlebars/express-handlebars")
    (synopsis
      "A Handlebars view engine for Express which doesn't suck.")
    (description
      "A Handlebars view engine for Express which doesn't suck.")
    (license license:bsd-3)))

(define-public node-express-rate-limit
  (package
    (name "node-express-rate-limit")
    (version "5.5.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/express-rate-limit/-/express-rate-limit-5.5.1.tgz")
        (sha256
          (base32
            "0k2s5ppijj98y2q13pm3iyyhdxjs27hxlsfziah9sma8ds8nzj11"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/nfriedly/express-rate-limit")
    (synopsis
      "Basic IP rate-limiting middleware for Express. Use to limit repeated requests to public APIs and/or endpoints such as password reset.")
    (description
      "Basic IP rate-limiting middleware for Express. Use to limit repeated requests to public APIs and/or endpoints such as password reset.")
    (license license:expat)))

(define-public node-express-ws
  (package
    (name "node-express-ws")
    (version "5.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/express-ws/-/express-ws-5.0.2.tgz")
        (sha256
          (base32
            "0hdmmag1062r061ik3iky6m1z30gq0xz8r90b60hx8s8j9kqha7n"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
     (list node-express node-ws))
    (home-page
      "https://github.com/HenningM/express-ws")
    (synopsis
      "WebSocket endpoints for Express applications")
    (description
      "WebSocket endpoints for Express applications")
    (license #f)))

(define-public node-traverse-chain
  (package
    (name "node-traverse-chain")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/traverse-chain/-/traverse-chain-0.1.0.tgz")
        (sha256
          (base32
            "1dvf89ah34wxw8wlcs246vhfdq6naa187pcadl3i87jzh5j89a2g"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://www.npmjs.com/package/node-traverse-chain")
    (synopsis "A simple asynchronous tool")
    (description "A simple asynchronous tool")
    (license license:expat)))

(define-public node-find
  (package
    (name "node-find")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/find/-/find-0.3.0.tgz")
        (sha256
          (base32
            "1wgh4q3dccbj1miia8bbpfvfq0n08h28fgvj0c7f7ba70chx3pbc"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-traverse-chain))
    (home-page
      "https://github.com/yuanchuan/find#readme")
    (synopsis "Find files or directories by name")
    (description "Find files or directories by name")
    (license license:expat)))

(define-public node-glob-to-regexp
  (package
    (name "node-glob-to-regexp")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/glob-to-regexp/-/glob-to-regexp-0.4.1.tgz")
        (sha256
          (base32
            "1b6hmxj0hrqfrb9ymnliryndch81gmmb2pg8x0hg9kw98znkxd8z"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/fitzgen/glob-to-regexp#readme")
    (synopsis "Convert globs to regular expressions")
    (description
      "Convert globs to regular expressions")
    (license #f)))

(define-public node-eventemitter3
  (package
    (name "node-eventemitter3")
    (version "4.0.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/eventemitter3/-/eventemitter3-4.0.7.tgz")
        (sha256
          (base32
            "1s2xjw4qgdrpg1rf759drg9ssl2pi6ban5gnb399l7cmlv7xwg3h"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/primus/eventemitter3#readme")
    (synopsis
      "EventEmitter3 focuses on performance while maintaining a Node.js AND browser compatible interface.")
    (description
      "EventEmitter3 focuses on performance while maintaining a Node.js AND browser compatible interface.")
    (license license:expat)))

(define-public node-requires-port
  (package
    (name "node-requires-port")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/requires-port/-/requires-port-1.0.0.tgz")
        (sha256
          (base32
            "1qn60lbgy6apjdkcgk2rpda122xz5zx3ml8hlljxjq1042gpcl31"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/unshiftio/requires-port")
    (synopsis
      "Check if a protocol requires a certain port number to be added to an URL.")
    (description
      "Check if a protocol requires a certain port number to be added to an URL.")
    (license license:expat)))

(define-public node-http-proxy
  (package
    (name "node-http-proxy")
    (version "1.18.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/http-proxy/-/http-proxy-1.18.1.tgz")
        (sha256
          (base32
            "1rzjma8gsd2mq2jxldl0f3iqikxd0qzbsm4f9pb5cpspn5anjm74"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-follow-redirects node-requires-port node-eventemitter3))
    (home-page
      "https://github.com/http-party/node-http-proxy#readme")
    (synopsis "HTTP proxying for the masses")
    (description "HTTP proxying for the masses")
    (license license:expat)))

(define-public node-ip-regex
  (package
    (name "node-ip-regex")
    (version "4.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ip-regex/-/ip-regex-4.3.0.tgz")
        (sha256
          (base32
            "0m30v3giwl0f8d5569jc43yqlrmdbnh0mjq5bkmxij2lw1cl818r"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sindresorhus/ip-regex#readme")
    (synopsis
      "Regular expression for matching IP addresses (IPv4 & IPv6)")
    (description
      "Regular expression for matching IP addresses (IPv4 & IPv6)")
    (license license:expat)))

(define-public node-buffer-equal-constant-time
  (package
    (name "node-buffer-equal-constant-time")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/buffer-equal-constant-time/-/buffer-equal-constant-time-1.0.1.tgz")
        (sha256
          (base32
            "0np7kzq65a7yvs7ch5vrhm6i9ayv7v3lqspdaiw3w422wdcm2icg"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://www.npmjs.com/package/node-buffer-equal-constant-time")
    (synopsis "Constant-time comparison of Buffers")
    (description
      "Constant-time comparison of Buffers")
    (license license:bsd-3)))

(define-public node-ecdsa-sig-formatter
  (package
    (name "node-ecdsa-sig-formatter")
    (version "1.0.11")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ecdsa-sig-formatter/-/ecdsa-sig-formatter-1.0.11.tgz")
        (sha256
          (base32
            "1zj8r1gp6vg3as5d0qs2qsycn0qwwkjaaj5n7cn7f514zx6vjz28"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-safe-buffer))
    (home-page
      "https://github.com/Brightspace/node-ecdsa-sig-formatter#readme")
    (synopsis
      "Translate ECDSA signatures between ASN.1/DER and JOSE-style concatenation")
    (description
      "Translate ECDSA signatures between ASN.1/DER and JOSE-style concatenation")
    (license license:asl2.0)))

(define-public node-jwa
  (package
    (name "node-jwa")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/jwa/-/jwa-2.0.0.tgz")
        (sha256
          (base32
            "19f22qvbmnhgafsb2rl825glxadqf7xb67v4c47gyddx5si911ic"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-safe-buffer node-ecdsa-sig-formatter
            node-buffer-equal-constant-time))
    (home-page
      "https://github.com/brianloveswords/node-jwa#readme")
    (synopsis
      "JWA implementation (supports all JWS algorithms)")
    (description
      "JWA implementation (supports all JWS algorithms)")
    (license license:expat)))

(define-public node-jws
  (package
    (name "node-jws")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/jws/-/jws-4.0.0.tgz")
        (sha256
          (base32
            "0rva0b2wrkwjns8hx5b24h4bcm6czfdl9cvnbbvbn66bbdl60b8n"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-safe-buffer node-jwa))
    (home-page
      "https://github.com/brianloveswords/node-jws#readme")
    (synopsis
      "Implementation of JSON Web Signatures")
    (description
      "Implementation of JSON Web Signatures")
    (license license:expat)))

(define-public node-lodash-includes
  (package
    (name "node-lodash-includes")
    (version "4.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.includes/-/lodash.includes-4.3.0.tgz")
        (sha256
          (base32
            "1mq8r2bxji9n8av6449lrg61zmq4fi80dl2zgfmd4lrwmbvv67xd"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.includes` exported as a module.")
    (description
      "The lodash method `_.includes` exported as a module.")
    (license license:expat)))

(define-public node-lodash-isboolean
  (package
    (name "node-lodash-isboolean")
    (version "3.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.isboolean/-/lodash.isboolean-3.0.3.tgz")
        (sha256
          (base32
            "17gfvibwm5bk7g0z4mrc3gmrl85qal1x37x7b05xqfykyla2198i"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.isBoolean` exported as a module.")
    (description
      "The lodash method `_.isBoolean` exported as a module.")
    (license license:expat)))

(define-public node-lodash-isinteger
  (package
    (name "node-lodash-isinteger")
    (version "4.0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.isinteger/-/lodash.isinteger-4.0.4.tgz")
        (sha256
          (base32
            "0z9vhzz2yw7yfvpdw0z3syjkrcmadagr59a2pxs04h6lg0x491bq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.isInteger` exported as a module.")
    (description
      "The lodash method `_.isInteger` exported as a module.")
    (license license:expat)))

(define-public node-lodash-isnumber
  (package
    (name "node-lodash-isnumber")
    (version "3.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.isnumber/-/lodash.isnumber-3.0.3.tgz")
        (sha256
          (base32
            "0qwvq81xy3k0w8q0g17rcw42qc4h0pdaz5hgmp5rdx063rz1drbk"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.isNumber` exported as a module.")
    (description
      "The lodash method `_.isNumber` exported as a module.")
    (license license:expat)))

(define-public node-lodash-isstring
  (package
    (name "node-lodash-isstring")
    (version "4.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.isstring/-/lodash.isstring-4.0.1.tgz")
        (sha256
          (base32
            "0pydamqjx3a487m89aqim3ky2wn4akr72d21zm25zw21rap4iza5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.isString` exported as a module.")
    (description
      "The lodash method `_.isString` exported as a module.")
    (license license:expat)))

(define-public node-lodash-once
  (package
    (name "node-lodash-once")
    (version "4.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.once/-/lodash.once-4.1.1.tgz")
        (sha256
          (base32
            "1d93rscawgjzadvf5j3cc4kjgq1d1agc2d0fbv33ak0xdy7q0rqd"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis
      "The lodash method `_.once` exported as a module.")
    (description
      "The lodash method `_.once` exported as a module.")
    (license license:expat)))

(define-public node-yallist
  (package
    (name "node-yallist")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/yallist/-/yallist-4.0.0.tgz")
        (sha256
          (base32
            "0jadz9mh1lzfk19bvqqlrg40ggfk2yyfyrpgj5c62dk54ym7h358"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/isaacs/yallist#readme")
    (synopsis "Yet Another Linked List")
    (description "Yet Another Linked List")
    (license license:isc)))

(define-public node-lru-cache
  (package
    (name "node-lru-cache")
    (version "6.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lru-cache/-/lru-cache-6.0.0.tgz")
        (sha256
          (base32
            "0pnziizgv8jpg708ykywcjby0syjz1l2ll1j727rdxhw0gmhvr2w"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-yallist))
    (home-page
      "https://github.com/isaacs/node-lru-cache#readme")
    (synopsis
      "A cache object that deletes the least-recently-used items.")
    (description
      "A cache object that deletes the least-recently-used items.")
    (license license:isc)))
#;
(define-public node-semver
  (package
    (name "node-semver")
    (version "7.3.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/semver/-/semver-7.3.5.tgz")
        (sha256
          (base32
            "17jrlrwq1fm80pl54lmra4zpj8crr3y7kj031fhq02g274j807xg"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs `(("node-lru-cache" ,node-lru-cache)))
    (home-page
      "https://github.com/npm/node-semver#readme")
    (synopsis
      "The semantic version parser used by npm.")
    (description
      "The semantic version parser used by npm.")
    (license license:isc)))

(define-public node-jsonwebtoken
  (package
    (name "node-jsonwebtoken")
    (version "8.5.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/jsonwebtoken/-/jsonwebtoken-8.5.1.tgz")
        (sha256
          (base32
            "0cfazln6lxr77kbmw2hc0rnrxj9aik2i6pss3nk1g6ihrzv9wnar"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-semver
            node-ms
            node-lodash-once
            node-lodash-isstring
            node-lodash-isplainobject
            node-lodash-isnumber
            node-lodash-isinteger
            node-lodash-isboolean
            node-lodash-includes
            node-jws))
    (home-page
      "https://github.com/auth0/node-jsonwebtoken#readme")
    (synopsis
      "JSON Web Token implementation (symmetric and asymmetric)")
    (description
      "JSON Web Token implementation (symmetric and asymmetric)")
    (license license:expat)))

(define-public node-ncp
  (package
    (name "node-ncp")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/ncp/-/ncp-2.0.0.tgz")
        (sha256
          (base32
            "1chad02wwxhvfkk0dk33pv98vxzwys2w7n8cr597j644ll35rfgh"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/AvianFlu/ncp")
    (synopsis
      "Asynchronous recursive file copy utility.")
    (description
      "Asynchronous recursive file copy utility.")
    (license license:expat)))

(define-public node-nocache
  (package
    (name "node-nocache")
    (version "3.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/nocache/-/nocache-3.0.1.tgz")
        (sha256
          (base32
            "1d5npz3fh43vdb6l3k7ycpmawzf7mgaarpmfvd7vcyzgpz62mbib"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/helmetjs/nocache")
    (synopsis "Middleware to destroy caching")
    (description "Middleware to destroy caching")
    (license license:expat)))

(define-public node-data-uri-to-buffer
  (package
    (name "node-data-uri-to-buffer")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/data-uri-to-buffer/-/data-uri-to-buffer-4.0.0.tgz")
        (sha256
          (base32
            "0rx47y251p13n0lfyyn05120rv6ib2l99a55d11cd384zmirrx4j"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/TooTallNate/node-data-uri-to-buffer")
    (synopsis
      "Generate a Buffer instance from a Data URI string")
    (description
      "Generate a Buffer instance from a Data URI string")
    (license license:expat)))

(define-public node-formdata-polyfill
  (package
    (name "node-formdata-polyfill")
    (version "4.0.10")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/formdata-polyfill/-/formdata-polyfill-4.0.10.tgz")
        (sha256
          (base32
            "1sc7hip8lwxbz2jg2k0snyqqwb4s8087kdj11zyz0cza710kpxqz"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-fetch-blob))
    (home-page
      "https://github.com/jimmywarting/FormData#readme")
    (synopsis
      "HTML5 `FormData` for Browsers and Node.")
    (description
      "HTML5 `FormData` for Browsers and Node.")
    (license license:expat)))

(define-public node-web-streams-polyfill
  (package
    (name "node-web-streams-polyfill")
    (version "3.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/web-streams-polyfill/-/web-streams-polyfill-3.2.0.tgz")
        (sha256
          (base32
            "0ikpxjrx628msn63mcxv1dlknrk9x66yyd3660cx36s9lkp2cj4c"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/MattiasBuelens/web-streams-polyfill#readme")
    (synopsis
      "Web Streams, based on the WHATWG spec reference implementation")
    (description
      "Web Streams, based on the WHATWG spec reference implementation")
    (license license:expat)))

(define-public node-fetch-blob
  (package
    (name "node-fetch-blob")
    (version "3.1.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fetch-blob/-/fetch-blob-3.1.3.tgz")
        (sha256
          (base32
            "1fidwmgybghwxikb0knk2i3jz2891464bqb7gqbzqjg16nxpzsc1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-web-streams-polyfill))
    (home-page
      "https://github.com/node-fetch/fetch-blob#readme")
    (synopsis
      "Blob & File implementation in Node.js, originally from node-fetch.")
    (description
      "Blob & File implementation in Node.js, originally from node-fetch.")
    (license license:expat)))



(define-public node-tr46
  ;; for node-node-fetch@2
  (package
    (name "node-tr46")
    (version "0.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/tr46/-/tr46-0.0.3.tgz")
        (sha256
          (base32 "02ia19bsjr545jlkgv35psmzzr5avic96zxw3dam78yf6bmy2jhn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-punycode))
    (home-page "https://github.com/jsdom/tr46#readme")
    (synopsis
      "Unicode IDNA Compatibility Processing")
    (description
      "An implementation of the Unicode UTS #46: Unicode IDNA Compatibility
Processing")
    (license license:expat)))

(define-public node-webidl-conversions
  ;; for node-node-fetch@2
  (package
    (name "node-webidl-conversions")
    (version "3.0.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/webidl-conversions/-/webidl-conversions-3.0.1.tgz")
        (sha256
          (base32 "1a1dwb1ga1cj2s7av9r46b4xmx11vsk5zncc0gq2qz4l815w7pz4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/jsdom/webidl-conversions#readme")
    (synopsis
      "Convert WebIDL to and from JavaScript values")
    (description
      "Implements the WebIDL algorithms for converting to and from JavaScript
values")
    (license #f)))

(define-public node-whatwg-url
  ;; for node-node-fetch@2
  (package
    (name "node-whatwg-url")
    (version "5.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/whatwg-url/-/whatwg-url-5.0.0.tgz")
        (sha256
          (base32 "1lvyrf4ry4bgl2jgpim2pkdmrbv2vb0vh0irmkp7da3kymqw97dh"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-webidl-conversions node-tr46))
    (home-page "https://github.com/jsdom/whatwg-url#readme")
    (synopsis
      "Implementation of the WHATWG URL Standard")
    (description
      "An implementation of the WHATWG URL Standard's URL API and parsing
machinery")
    (license license:expat)))




(define-public node-node-fetch
  ;; avoid ERR_REQUIRE_ESM
  (package
    (name "node-node-fetch")
    (version "2.6.6")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/node-fetch/-/node-fetch-2.6.6.tgz")
        (sha256
          (base32
            "1g4ydjfgxx27b8yp6p9yki3bdg0sxvlsh81bmv2hdzg7gvsl0gh2"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
     (list node-fetch-blob node-whatwg-url node-formdata-polyfill
           node-data-uri-to-buffer))
    (home-page
      "https://github.com/node-fetch/node-fetch")
    (synopsis
      "A light-weight module that brings Fetch API to node.js")
    (description
      "A light-weight module that brings Fetch API to node.js")
    (license license:expat)))

(define-public node-node-getopt
  (package
    (name "node-node-getopt")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/node-getopt/-/node-getopt-0.3.2.tgz")
        (sha256
          (base32
            "1psmqs1qhw181lpay065sbdm8f7pi5zhxwyw3449fydd6n1158x5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/jiangmiao/node-getopt")
    (synopsis "featured command line args parser")
    (description "featured command line args parser")
    (license license:expat)))

(define-public node-promisepipe
  (package
    (name "node-promisepipe")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/promisepipe/-/promisepipe-3.0.0.tgz")
        (sha256
          (base32
            "0zc6x3arvzc7rg6036gkyb8l1b8lzlj59apky7ap34r9m2ak19xq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/epeli/node-promisepipe#readme")
    (synopsis
      "Pipe node.js streams safely with Promises")
    (description
      "Pipe node.js streams safely with Promises")
    (license license:expat)))

(define-public node-rimraf
  (package
    (name "node-rimraf")
    (version "3.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/rimraf/-/rimraf-3.0.2.tgz")
        (sha256
          (base32
            "0lkzjyxjij6ssh5h2l3ncp0zx00ylzhww766dq2vf1s7v07w4xjq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-glob))
    (home-page
      "https://github.com/isaacs/rimraf#readme")
    (synopsis
      "A deep deletion module for node (like `rm -rf`)")
    (description
      "A deep deletion module for node (like `rm -rf`)")
    (license license:isc)))
#;
(define-public node-file-uri-to-path
  (package
    (name "node-file-uri-to-path")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/file-uri-to-path/-/file-uri-to-path-2.0.0.tgz")
        (sha256
          (base32
            "1jjshwn4ar9bs13lvjmnrdkdjdkbmifkv4jp3vr93vlw452sw90v"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/TooTallNate/file-uri-to-path")
    (synopsis "Convert a file: URI to a file path")
    (description
      "Convert a file: URI to a file path")
    (license license:expat)))
#;
(define-public node-bindings
  (package
    (name "node-bindings")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/bindings/-/bindings-1.5.0.tgz")
        (sha256
          (base32
            "01q9xfgc5jl9h6ny44zb4xcin6sirmm5bignn68rmn2vihbq2xyp"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      `(("node-file-uri-to-path" ,node-file-uri-to-path)))
    (home-page
      "https://github.com/TooTallNate/node-bindings")
    (synopsis
      "Helper module for loading your native module's .node file")
    (description
      "Helper module for loading your native module's .node file")
    (license license:expat)))
#;
(define-public node-segfault-handler
  (package
    (name "node-segfault-handler")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/segfault-handler/-/segfault-handler-1.3.0.tgz")
        (sha256
          (base32
            "1idgs1wifzw47lcinjp4ilnyxb5lxv23bha09y99p1gxjkq3cwxz"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      `(("node-nan" ,node-nan)
        ("node-bindings" ,node-bindings)))
    (home-page
      "https://github.com/ddopson/node-segfault-handler#readme")
    (synopsis
      "catches SIGSEGV and prints diagnostic information")
    (description
      "catches SIGSEGV and prints diagnostic information")
    (license license:bsd-3)))

(define-public node-base32-js
  (package
    (name "node-base32-js")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/base32.js/-/base32.js-0.1.0.tgz")
        (sha256
          (base32
            "0glwcjjcd3417k95072da4fapgiwisd32rj54zgymxbynmgiki6f"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/mikepb/base32.js#readme")
    (synopsis "Base 32 encodings for JavaScript")
    (description "Base 32 encodings for JavaScript")
    (license license:expat)))

(define-public node-speakeasy
  (package
    (name "node-speakeasy")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/speakeasy/-/speakeasy-2.0.0.tgz")
        (sha256
          (base32
            "1yq7vcs8klwnw2jnshvi6gcl2wfhqy42mk41qvy6askkdf7gb0rh"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-base32-js))
    (home-page
      "http://github.com/speakeasyjs/speakeasy")
    (synopsis
      "Two-factor authentication for Node.js. One-time passcode generator (HOTP/TOTP) with support for Google Authenticator.")
    (description
      "Two-factor authentication for Node.js. One-time passcode generator (HOTP/TOTP) with support for Google Authenticator.")
    (license license:expat)))

(define-public node-string-format
  (package
    (name "node-string-format")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/string-format/-/string-format-2.0.0.tgz")
        (sha256
          (base32
            "189r374h4q1kiscchn0q1x1ga4a94wl9hnj5q3jg8jly9zqmrahz"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/davidchambers/string-format")
    (synopsis
      "String formatting inspired by Python's str.format()")
    (description
      "String formatting inspired by Python's str.format()")
    (license #f)))

(define-public node-chownr
  (package
    (name "node-chownr")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/chownr/-/chownr-2.0.0.tgz")
        (sha256
          (base32
            "177wsdfmn1d2f12wy8m875b5y9a74ibfdh33jarlv3a0zrbmvqlv"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/isaacs/chownr#readme")
    (synopsis "like `chown -R`")
    (description "like `chown -R`")
    (license license:isc)))

(define-public node-fs-minipass
  (package
    (name "node-fs-minipass")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fs-minipass/-/fs-minipass-2.1.0.tgz")
        (sha256
          (base32
            "029kdjb6h8gp0gfx7rx6yzwbv7pnd7i119gn563ynv0dqx02p5gx"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-minipass))
    (home-page
      "https://github.com/npm/fs-minipass#readme")
    (synopsis
      "fs read and write streams based on minipass")
    (description
      "fs read and write streams based on minipass")
    (license license:isc)))

(define-public node-minipass
  (package
    (name "node-minipass")
    (version "3.1.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/minipass/-/minipass-3.1.5.tgz")
        (sha256
          (base32
            "0050rbjr8vb8cjpzprkrr8j86rwz1qvw3w7npz2pc9wkcv6cxn0b"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-yallist))
    (home-page
      "https://github.com/isaacs/minipass#readme")
    (synopsis
      "minimal implementation of a PassThrough stream")
    (description
      "minimal implementation of a PassThrough stream")
    (license license:isc)))

(define-public node-minizlib
  (package
    (name "node-minizlib")
    (version "2.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/minizlib/-/minizlib-2.1.2.tgz")
        (sha256
          (base32
            "1vffn3i5ys3w74s8m8n4l7vvzijddi44flpaxdfv96q85n513va4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-yallist node-minipass))
    (home-page
      "https://github.com/isaacs/minizlib#readme")
    (synopsis
      "A small fast zlib stream built on [minipass](http://npm.im/minipass) and Node.js's zlib binding.")
    (description
      "A small fast zlib stream built on [minipass](http://npm.im/minipass) and Node.js's zlib binding.")
    (license license:expat)))

(define-public node-tar
  (package
    (name "node-tar")
    (version "6.1.11")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/tar/-/tar-6.1.11.tgz")
        (sha256
          (base32
            "131l7k28di5w0a17kvy7gzqp1mm8vlya38k21wmbmmqji77jcfv5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-yallist
            node-mkdirp
            node-minizlib
            node-minipass
            node-fs-minipass
            node-chownr))
    (home-page
      "https://github.com/npm/node-tar#readme")
    (synopsis "tar for node")
    (description "tar for node")
    (license license:isc)))

(define-public node-tmp
  (package
    (name "node-tmp")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/tmp/-/tmp-0.2.1.tgz")
        (sha256
          (base32
            "16llcfa2ic24ahfz5y3bp858p78r589gx3vggpzy4zxwdx7xfv3x"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-rimraf))
    (home-page "http://github.com/raszi/node-tmp")
    (synopsis "Temporary file and directory creator")
    (description
      "Temporary file and directory creator")
    (license license:expat)))

(define-public node-uuid
  (package
    (name "node-uuid")
    (version "8.3.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/uuid/-/uuid-8.3.2.tgz")
        (sha256
          (base32
            "0jljg4g6m0znnqm93ng9xwj9g63csbwdqbjlb9rir0m58wv70c7n"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/uuidjs/uuid#readme")
    (synopsis "RFC4122 (v1, v4, and v5) UUIDs")
    (description "RFC4122 (v1, v4, and v5) UUIDs")
    (license license:expat)))

(define-public node-http-ece
  (package
    (name "node-http-ece")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/http_ece/-/http_ece-1.1.0.tgz")
        (sha256
          (base32
            "19clxkqbsh0rywqizfqv63y6iljql6kpsvslinrgpws0ddqk329w"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-urlsafe-base64))
    (home-page
      "https://github.com/martinthomson/encrypted-content-encoding")
    (synopsis "Encrypted Content-Encoding for HTTP")
    (description
      "Encrypted Content-Encoding for HTTP")
    (license license:expat)))

(define-public node-agent-base
  (package
    (name "node-agent-base")
    (version "6.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/agent-base/-/agent-base-6.0.2.tgz")
        (sha256
          (base32
            "0cg85gngrap12xzz8ibdjw98hcfhgcidg2w7ll7whsrf59ps0vdw"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-debug))
    (home-page
      "https://github.com/TooTallNate/node-agent-base#readme")
    (synopsis
      "Turn a function into an `http.Agent` instance")
    (description
      "Turn a function into an `http.Agent` instance")
    (license license:expat)))

(define-public node-https-proxy-agent
  (package
    (name "node-https-proxy-agent")
    (version "5.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/https-proxy-agent/-/https-proxy-agent-5.0.0.tgz")
        (sha256
          (base32
            "1a7mbscj5g23bcprp7psn5f82dhqz09sdris931dwkazq8gfparq"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-debug node-agent-base))
    (home-page
      "https://github.com/TooTallNate/node-https-proxy-agent#readme")
    (synopsis
      "An HTTP(s) proxy `http.Agent` implementation for HTTPS")
    (description
      "An HTTP(s) proxy `http.Agent` implementation for HTTPS")
    (license license:expat)))

(define-public node-urlsafe-base64
  (package
    (name "node-urlsafe-base64")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/urlsafe-base64/-/urlsafe-base64-1.0.0.tgz")
        (sha256
          (base32
            "1x70gdgrwvsp50384i9ymlqqlysmv91bw0d7g4yw578amiqji8f0"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/RGBboy/urlsafe-base64")
    (synopsis "URL Safe Base64 encoding")
    (description "URL Safe Base64 encoding")
    (license #f)))

(define-public node-web-push
  (package
    (name "node-web-push")
    (version "3.4.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/web-push/-/web-push-3.4.5.tgz")
        (sha256
          (base32
            "1j5l23pidrdy96843aixbkwz9qrw7c6chj3pi9ib2yq976siwxf3"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-urlsafe-base64
            node-minimist
            node-jws
            node-https-proxy-agent
            node-http-ece
            node-asn1-js))
    (home-page
      "https://github.com/web-push-libs/web-push#readme")
    (synopsis "Web Push library for Node.js")
    (description "Web Push library for Node.js")
    (license license:mpl2.0)))

(define-public node-text-hex
  (package
    (name "node-text-hex")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/text-hex/-/text-hex-1.0.0.tgz")
        (sha256
          (base32
            "00zhmsmjknbn075js9wq91887fh690cymgckbjjiymv4bnmsjqaa"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/3rd-Eden/text-hex")
    (synopsis
      "Generate a hex color from the given text")
    (description
      "Generate a hex color from the given text")
    (license license:expat)))

(define-public node-colorspace
  (package
    (name "node-colorspace")
    (version "1.1.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/colorspace/-/colorspace-1.1.4.tgz")
        (sha256
          (base32
            "0ds19v89jhifba9hzgm001zwhawcyzwjmbjla7plkk8433641crw"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-text-hex node-color))
    (home-page
      "https://github.com/3rd-Eden/colorspace")
    (synopsis
      "Generate HEX colors for a given namespace.")
    (description
      "Generate HEX colors for a given namespace.")
    (license license:expat)))

(define-public node-enabled
  (package
    (name "node-enabled")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/enabled/-/enabled-2.0.0.tgz")
        (sha256
          (base32
            "0g6dqx906cmj5m3mk6q24bkg8bf9i8cxqyzc0cx1i4n02qrz19kg"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/3rd-Eden/enabled#readme")
    (synopsis
      "Check if a certain debug flag is enabled.")
    (description
      "Check if a certain debug flag is enabled.")
    (license license:expat)))

(define-public node-kuler
  (package
    (name "node-kuler")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/kuler/-/kuler-2.0.0.tgz")
        (sha256
          (base32
            "1x16hvz5fvdvp0mkjp5zz1270k5r38is5f6wdwcpyhzhjin934b6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/3rd-Eden/kuler")
    (synopsis
      "Color your terminal using CSS/hex color codes")
    (description
      "Color your terminal using CSS/hex color codes")
    (license license:expat)))

(define-public node-dabh-diagnostics
  (package
    (name "node-dabh-diagnostics")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@dabh/diagnostics/-/diagnostics-2.0.2.tgz")
        (sha256
          (base32
            "1qldxjyrjkdhsc2kvf4b092w1b7c8lnqnpwjj50pwd9w4xgbc9k6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-kuler node-enabled node-colorspace))
    (home-page
      "https://github.com/3rd-Eden/diagnostics")
    (synopsis
      "Tools for debugging your node.js modules and event loop")
    (description
      "Tools for debugging your node.js modules and event loop")
    (license license:expat)))

(define-public node-is-stream
  ;; roll back from 3.0.0 to avoid ERR_REQUIRE_ESM
  (package
    (name "node-is-stream")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/is-stream/-/is-stream-2.0.0.tgz")
        (sha256
          (base32
            "0m6y8fxz5g0kng125l8p4pkqlkhhlfd7z8bcg6qszjiiww8jchfn"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sindresorhus/is-stream#readme")
    (synopsis
      "Check if something is a Node.js stream")
    (description
      "Check if something is a Node.js stream")
    (license license:expat)))

(define-public node-colors
  (package
    (name "node-colors")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/colors/-/colors-1.4.0.tgz")
        (sha256
          (base32
            "1604c734ar0mskkbynwld58n53pdzlw0694yh0cpbhvwv8zbh3mm"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/Marak/colors.js")
    (synopsis "get colors in your node.js console")
    (description
      "get colors in your node.js console")
    (license license:expat)))

(define-public node-safe-stable-stringify
  (package
    (name "node-safe-stable-stringify")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/safe-stable-stringify/-/safe-stable-stringify-2.2.0.tgz")
        (sha256
          (base32
            "0y1qsmjsaw6pxxcm9za0gryiqm7751dflmzqlx05p8ykbv38k8q8"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/BridgeAR/safe-stable-stringify#readme")
    (synopsis
      "Deterministic and safely JSON.stringify to quickly serialize JavaScript objects")
    (description
      "Deterministic and safely JSON.stringify to quickly serialize JavaScript objects")
    (license license:expat)))

(define-public node-fecha
  (package
    (name "node-fecha")
    (version "4.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fecha/-/fecha-4.2.1.tgz")
        (sha256
          (base32
            "1nsigv4qsqzryzbxsggz7k3x6bg39j8js0v9x0gc2m2x7z36c57y"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/taylorhakes/fecha")
    (synopsis "Date formatting and parsing")
    (description "Date formatting and parsing")
    (license license:expat)))

(define-public node-logform
  (package
    (name "node-logform")
    (version "2.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/logform/-/logform-2.3.0.tgz")
        (sha256
          (base32
            "17p93zbv2i3ri1j2cvk74wp1lvykhskbvi8yjwb44zl5fkx8ywpf"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-triple-beam node-ms node-fecha
            node-safe-stable-stringify node-colors))
    (home-page
      "https://github.com/winstonjs/logform#readme")
    (synopsis
      "An mutable object-based log format designed for chaining & objectMode streams.")
    (description
      "An mutable object-based log format designed for chaining & objectMode streams.")
    (license license:expat)))

(define-public node-fn-name
  (package
    (name "node-fn-name")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/fn.name/-/fn.name-1.1.0.tgz")
        (sha256
          (base32
            "1mz826ygqynqwanf0l8bgsacwmh9cqb3943gd8fyn2gl08g5f91z"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://github.com/3rd-Eden/fn.name")
    (synopsis "Extract names from functions")
    (description "Extract names from functions")
    (license license:expat)))

(define-public node-one-time
  (package
    (name "node-one-time")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/one-time/-/one-time-1.0.0.tgz")
        (sha256
          (base32
            "0ksxyjp47hdvggawwws6q5a29lsn3asrfpaij5p2345a8pgms06v"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-fn-name))
    (home-page
      "https://github.com/3rd-Eden/one-time#readme")
    (synopsis
      "Run the supplied function exactly one time (once)")
    (description
      "Run the supplied function exactly one time (once)")
    (license license:expat)))

(define-public node-stack-trace
  ;; avoid ERR_REQUIRE_ESM
  (package
    (name "node-stack-trace")
    (version "0.0.10")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/stack-trace/-/stack-trace-0.0.10.tgz")
        (sha256
          (base32
            "0pszfwxxiy0171s9vm53x8hmav7g4yhlqcqw91l4q8sxc8x8kq3i"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/felixge/node-stack-trace")
    (synopsis
      "Get v8 stack traces as an array of CallSite objects.")
    (description
      "Get v8 stack traces as an array of CallSite objects.")
    (license license:expat)))

(define-public node-triple-beam
  (package
    (name "node-triple-beam")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/triple-beam/-/triple-beam-1.3.0.tgz")
        (sha256
          (base32
            "1k2j7xih9yzgd0qfxwjp9cajw7rklz41rz450w3ja0dz4ckjk5a1"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/winstonjs/triple-beam#readme")
    (synopsis
      "Definitions of levels for logging purposes & shareable Symbol constants.")
    (description
      "Definitions of levels for logging purposes & shareable Symbol constants.")
    (license license:expat)))

(define-public node-winston-transport
  (package
    (name "node-winston-transport")
    (version "4.4.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/winston-transport/-/winston-transport-4.4.0.tgz")
        (sha256
          (base32
            "09zir3xyryk3656mvmi3l880y1xx21v9j4gkf5c1jcrraa93g38d"))
        (snippet
         (with-imported-modules `((guix build utils))
           #~(begin
               (use-modules (guix build utils))
               (substitute* "index.js"
                 (("const Writable = require[(]'readable-stream/writable'[)];")
                  "const { Writable } = require('readable-stream');")))))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
     (list node-triple-beam node-logform node-readable-stream))
    (home-page
      "https://github.com/winstonjs/winston-transport#readme")
    (synopsis
      "Base stream implementations for winston@@3 and up.")
    (description
      "Base stream implementations for winston@@3 and up.")
    (license license:expat)))

(define-public node-winston
  (package
    (name "node-winston")
    (version "3.3.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/winston/-/winston-3.3.3.tgz")
        (sha256
          (base32
           "0x12fh52f8i95zvsg811kvdwdk2jsq348abgyr04fd82sk711wmh"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-winston-transport
            node-triple-beam
            node-stack-trace
            node-readable-stream
            node-one-time
            node-logform
            node-is-stream
            node-dabh-diagnostics
            node-async))
    (home-page
      "https://github.com/winstonjs/winston#readme")
    (synopsis "A logger for just about everything.")
    (description
      "A logger for just about everything.")
    (license license:expat)))

(define-public node-moment
  (package
    (name "node-moment")
    (version "2.29.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/moment/-/moment-2.29.1.tgz")
        (sha256
          (base32
            "0m9c1i494f6yxmb1md5s7idrhqwqbk35zz5fv539hzh2cjd0y622"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://momentjs.com")
    (synopsis
      "Parse, validate, manipulate, and display dates")
    (description
      "Parse, validate, manipulate, and display dates")
    (license license:expat)))

(define-public node-file-stream-rotator
  (package
    (name "node-file-stream-rotator")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/file-stream-rotator/-/file-stream-rotator-0.5.7.tgz")
        (sha256
          (base32
            "0zdvivaikh9733pf5jy69p7nmp8dgb571ch9n6sdm0xlw0lmk92j"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-moment))
    (home-page
      "https://github.com/rogerc/file-stream-rotator#readme")
    (synopsis
      "Automated stream rotation useful for log files")
    (description
      "Automated stream rotation useful for log files")
    (license license:expat)))

(define-public node-object-hash
  (package
    (name "node-object-hash")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/object-hash/-/object-hash-2.2.0.tgz")
        (sha256
          (base32
            "12kf2hxx3z2fsv4zskn8ghvhyxd9n9bv784kmqbk5kd7kk8ldv96"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/puleos/object-hash")
    (synopsis
      "Generate hashes from javascript objects in node and the browser.")
    (description
      "Generate hashes from javascript objects in node and the browser.")
    (license license:expat)))

(define-public node-winston-daily-rotate-file
  (package
    (name "node-winston-daily-rotate-file")
    (version "4.5.5")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/winston-daily-rotate-file/-/winston-daily-rotate-file-4.5.5.tgz")
        (sha256
          (base32
            "066gh2is7gc2y5an7mxq4qkdp7h9cy49haa8jfs5g24iskp57bzz"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
     (list node-winston node-winston-transport node-triple-beam
           node-object-hash node-file-stream-rotator))
    (home-page
      "https://github.com/winstonjs/winston-daily-rotate-file#readme")
    (synopsis
      "A transport for winston which logs to a rotating file each day.")
    (description
      "A transport for winston which logs to a rotating file each day.")
    (license license:expat)))

















(define-public node-highlight-js
  (package
    (name "node-highlight-js")
    (version "11.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/highlight.js/-/highlight.js-11.3.1.tgz")
        (sha256
          (base32
            "1lx9cs9xi50648dhnbdfgimngg51i61hvklq9vanh0g1d9p8wcif"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page "https://highlightjs.org/")
    (synopsis
      "Syntax highlighting with language autodetection.")
    (description
      "Syntax highlighting with language autodetection.")
    (license license:bsd-3)))

(define-public node-page
  (package
    (name "node-page")
    (version "1.11.6")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/page/-/page-1.11.6.tgz")
        (sha256
          (base32
            "0dikk9jlw2ac3j8zjiyy1xbjdhki9dwl8wb1xdhcg5y2as8fkyb5"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-path-to-regexp))
    (home-page
      "https://github.com/visionmedia/page.js#readme")
    (synopsis "Tiny client-side router")
    (description "Tiny client-side router")
    (license license:expat)))

(define-public node-qrcode-svg
  ;; easy to make non-"binary"?
  (package
    (name "node-qrcode-svg")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/qrcode-svg/-/qrcode-svg-1.1.0.tgz")
        (sha256
          (base32
            "0yxdxqdddjd06brq0v751bryfk4qryrhp925iy7w8swp7prl864z"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/papnkukn/qrcode-svg")
    (synopsis
      "A simple QR Code generator in pure JavaScript")
    (description
      "A simple QR Code generator in pure JavaScript")
    (license license:expat)))

(define-public node-eme-encryption-scheme-polyfill
  (package
    (name "node-eme-encryption-scheme-polyfill")
    (version "2.0.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/eme-encryption-scheme-polyfill/-/eme-encryption-scheme-polyfill-2.0.3.tgz")
        (sha256
          (base32
            "18p8gfymvb2yp26kl659qd52yl3c1mshmvm477qapqi5q1nfiqr4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/google/eme-encryption-scheme-polyfill#readme")
    (synopsis
      "A polyfill for the encryptionScheme field in EME")
    (description
      "A polyfill for the encryptionScheme field in EME")
    (license license:asl2.0)))

(define-public node-shaka-player
  (package
    (name "node-shaka-player")
    (version "3.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/shaka-player/-/shaka-player-3.2.1.tgz")
        (sha256
          (base32
            "06xqn3d0f9a3431jr091lg735gpzl9i8v3bvm93b2n7pmqkjrwz0"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-eme-encryption-scheme-polyfill))
    (home-page
      "https://github.com/google/shaka-player")
    (synopsis "DASH/EME video player library")
    (description "DASH/EME video player library

FIXME: DRM/EME issues
")
    (license license:asl2.0)))

(define-public node-hapi-bourne
  (package
    (name "node-hapi-bourne")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@hapi/bourne/-/bourne-2.0.0.tgz")
        (sha256
          (base32
            "1k1g58cs9cjg8jjn3fd813rwla3rlw3frzsgkgasy04vkiwaywr7"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/hapijs/bourne#readme")
    (synopsis
      "JSON parse with prototype poisoning protection")
    (description
      "JSON parse with prototype poisoning protection")
    (license license:bsd-3)))

(define-public node-hapi-boom
  (package
    (name "node-hapi-boom")
    (version "9.1.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@hapi/boom/-/boom-9.1.4.tgz")
        (sha256
          (base32
            "0s823lbd1237m9xgl5jz8cr52pxmgkrbpbblrqcm3fy1wvspp6z2"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-hapi-hoek))
    (home-page
      "https://github.com/hapijs/boom#readme")
    (synopsis "HTTP-friendly error objects")
    (description "HTTP-friendly error objects")
    (license license:bsd-3)))

(define-public node-hapi-wreck
  (package
    (name "node-hapi-wreck")
    (version "17.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@hapi/wreck/-/wreck-17.1.0.tgz")
        (sha256
          (base32
            "033dd0kr8cflghpf7xrswaqz9qbbbgr54546yb21xq5s6h7x6nyj"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-hapi-hoek node-hapi-boom node-hapi-bourne))
    (home-page
      "https://github.com/hapijs/wreck#readme")
    (synopsis "HTTP Client Utilities")
    (description "HTTP Client Utilities")
    (license license:bsd-3)))
#;
(define-public node-debug
  (package
    (name "node-debug")
    (version "4.3.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/debug/-/debug-4.3.3.tgz")
        (sha256
          (base32
            "1vfcmxn26acv3fdggrhzs5fv4qbw5av82ma4lzrkvjjvqr1dbaaz"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs `(("node-ms" ,node-ms)))
    (home-page
      "https://github.com/debug-js/debug#readme")
    (synopsis
      "Lightweight debugging utility for Node.js and the browser")
    (description
      "Lightweight debugging utility for Node.js and the browser")
    (license license:expat)))

(define-public node-hapi-topo
  (package
    (name "node-hapi-topo")
    (version "5.1.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@hapi/topo/-/topo-5.1.0.tgz")
        (sha256
          (base32
            "0fwngxzvjwpzkh1rrpixlnj2dcdxvkywidagg7np4n3wcm0fqgl6"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-hapi-hoek))
    (home-page
      "https://github.com/hapijs/topo#readme")
    (synopsis
      "Topological sorting with grouping support")
    (description
      "Topological sorting with grouping support")
    (license license:bsd-3)))

(define-public node-hapi-hoek
  (package
    (name "node-hapi-hoek")
    (version "9.2.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@hapi/hoek/-/hoek-9.2.1.tgz")
        (sha256
          (base32
            "0vbc5ph2hrw9vckr4sgbz8sb741vxlcv8brh2np59yy3ci5clcs7"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/hapijs/hoek#readme")
    (synopsis "General purpose node utilities")
    (description "General purpose node utilities")
    (license license:bsd-3)))

(define-public node-sideway-address
  (package
    (name "node-sideway-address")
    (version "4.1.3")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@sideway/address/-/address-4.1.3.tgz")
        (sha256
          (base32
            "14pf88h3wwhs081qwkjpirv9b694jf19shsaibxalq5b10qsx8m4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs (list node-hapi-hoek))
    (home-page
      "https://github.com/sideway/address#readme")
    (synopsis "Email address and domain validation")
    (description
      "Email address and domain validation")
    (license license:bsd-3)))

(define-public node-sideway-formula
  (package
    (name "node-sideway-formula")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@sideway/formula/-/formula-3.0.0.tgz")
        (sha256
          (base32
            "0xyxfs1hfcmglad94bk7b2mpnzck0ml8zcpmzdncnyvx0n8iwip2"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sideway/formula#readme")
    (synopsis "Math and string formula parser.")
    (description "Math and string formula parser.")
    (license license:bsd-3)))

(define-public node-sideway-pinpoint
  (package
    (name "node-sideway-pinpoint")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@sideway/pinpoint/-/pinpoint-2.0.0.tgz")
        (sha256
          (base32
            "0riaqzj5kl6zw4k4haqhpsy3vdv5wvpvg5x363r0z3ib1qqdyh8q"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (home-page
      "https://github.com/sideway/pinpoint#readme")
    (synopsis
      "Return the filename and line number of the calling function")
    (description
      "Return the filename and line number of the calling function")
    (license license:bsd-3)))

(define-public node-joi
  (package
    (name "node-joi")
    (version "17.5.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/joi/-/joi-17.5.0.tgz")
        (sha256
          (base32
            "04nrgjc85460c9gbx4bmplym4g6r0as8gmcw3286b6bqlvhypwpc"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-sideway-pinpoint node-sideway-formula
            node-sideway-address node-hapi-topo node-hapi-hoek))
    (home-page
      "https://github.com/sideway/joi#readme")
    (synopsis "Object schema validation")
    (description "Object schema validation")
    (license license:bsd-3)))

(define-public node-simple-oauth2
  (package
    (name "node-simple-oauth2")
    (version "4.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/simple-oauth2/-/simple-oauth2-4.2.0.tgz")
        (sha256
          (base32
            "1s3fh34hk907q5f2r5586rr7ky75ip59rr211ycf9js0igkax58n"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases
          %standard-phases
          (delete 'configure)
          (delete 'build))))
    (inputs
      (list node-joi node-debug node-hapi-wreck node-hapi-hoek))
    (home-page
      "https://github.com/lelylan/simple-oauth2")
    (synopsis "Node.js client for OAuth2")
    (description "Node.js client for OAuth2")
    (license license:asl2.0)))

(define-public node-mobile-drag-drop
  (package
    (name "node-mobile-drag-drop")
    (version "2.3.0-rc.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/mobile-drag-drop/-/mobile-drag-drop-2.3.0-rc.2.tgz")
        (sha256
          (base32 "0ian94q0a9blg5473hnc3f48jrfnmm8zmkbx2sqbpsh1ira71x2s"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "http://timruffles.github.io/ios-html5-drag-drop-shim/demo/")
    (synopsis
      "Polyfill for making HTML5 drag and drop possible in all browsers.")
    (description
      "Polyfill for making HTML5 drag and drop possible in all browsers.")
    (license license:expat)))

(define-public node-intl-pluralrules
  (package
    (name "node-intl-pluralrules")
    (version "1.3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/intl-pluralrules/-/intl-pluralrules-1.3.1.tgz")
        (sha256
          (base32 "07kbkncy749c5pw0p7a5267pvj6h1cjak59dljw8s46zn7s372h4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/eemeli/intl-pluralrules#readme")
    (synopsis "Intl.PluralRules polyfill")
    (description "Intl.PluralRules polyfill")
    (license license:isc)))

(define-public node-webcomponents-webcomponentsjs
  (package
    (name "node-webcomponents-webcomponentsjs")
    (version "2.6.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/@webcomponents/webcomponentsjs/-/webcomponentsjs-2.6.0.tgz")
        (sha256
          (base32 "1l361fkaiib7qjx1f39cfxc0qagzh9i8i3zq6zwjapr9k1k1hbb4"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page
      "https://github.com/webcomponents/polyfills/tree/master/packages/webcomponentsjs")
    (synopsis "Web Components Polyfills")
    (description "Web Components Polyfills")
    (license license:bsd-3)))


#|


               1.0.0



|#

(define-public node-is-promise
  (package
    (name "node-is-promise")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/is-promise/-/is-promise-4.0.0.tgz")
        (sha256
          (base32 "19s5njn24k6ra9c4skkzjhjfaq0d1izkxxicfsw07ykn70br2f45"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://github.com/then/is-promise#readme")
    (synopsis "Test whether an object looks like a promises-a+ promise")
    (description "Test whether an object looks like a promises-a+ promise")
    (license license:expat)))

(define-public node-lodash-flattendeep
  (package
    (name "node-lodash-flattendeep")
    (version "4.4.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/lodash.flattendeep/-/lodash.flattendeep-4.4.0.tgz")
        (sha256
          (base32 "1j11kg6ypb2x1vfc9ww3d95dfzk5c7l36qy756qi69rzxgp23k82"))))
    (build-system node-build-system)
    (arguments
      `(#:tests?
        #f
        #:phases
        (modify-phases %standard-phases (delete 'configure) (delete 'build))))
    (home-page "https://lodash.com/")
    (synopsis "The lodash method `_.flattenDeep` exported as a module.")
    (description "The lodash method `_.flattenDeep` exported as a module.")
    (license license:expat)))

(define-public node-express-promise-router
  (package
    (name "node-express-promise-router")
    (version "4.1.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://registry.npmjs.org/express-promise-router/-/express-promise-router-4.1.1.tgz")
        (sha256
          (base32 "0b0w7dnzbkg4fcyn04bvlmdd82h73fb0abjswxf8m143ss1vzn43"))))
    (build-system node-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("@types/express"))))
         (delete 'configure) (delete 'build))
       #:tests? #f))
    (inputs
     (list node-methods node-express node-lodash-flattendeep
           node-is-promise))
    (home-page
      "https://github.com/express-promise-router/express-promise-router")
    (synopsis
      "A lightweight wrapper for Express 4's Router that allows middleware to return promises")
    (description
      "A lightweight wrapper for Express 4's Router that allows middleware to return promises")
    (license #f)))



