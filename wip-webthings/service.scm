;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings service)
  #:use-module (wip-webthings packages addon)
  #:use-module (wip-webthings packages gateway)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 exceptions)
  #:export (webthings-service-type
            webthings-configuration
            webthings-configuration?))

(define-record-type* <webthings-configuration>
  webthings-configuration make-webthings-configuration
  webthings-configuration?
  (package webthings-configuration-package
           (default webthings-gateway))
  (profile-dir webthings-configuration-profile-dir
               (default "/srv/webthings"))
  (addons webthings-configuration-addons
          (default '())
          (sanitize
           (lambda (value)
             (define (reject msg . irritants)
               (raise (make-exception
                       (make-programming-error)
                       (make-exception-with-origin
                        'webthings-configuration-addons)
                       (make-exception-with-message msg)
                       (make-exception-with-irritants irritants))))
             (unless (list? value)
               (reject "given value is not a list" value))
             (map (lambda (item)
                    (cond
                     ((webthings-addon? item)
                      item)
                     ((not (package? item))
                      (reject "expected a package or a webthings-addon"
                              item))
                     (else
                      (let* ((addon (assoc-ref (package-properties item)
                                               'webthings-addon))
                             (addon (if (promise? addon)
                                        (force addon)
                                        addon)))
                        (cond
                         ((webthings-addon? addon)
                          addon)
                         ((not addon)
                          (reject
                           "package does not have a 'webthings-addon property"
                           item))
                         (else
                          (reject
                           "package has an invalid 'webthings-addon property"
                           item addon)))))))
                  value))))
  (extra-config-file webthings-configuration-extra-config-file
                     (default #f)))

(define %webthings-accounts
  (list (user-group (name "webthings") (system? #t))
        (user-account
         (name "webthings")
         (group "webthings")
         (supplementary-groups '("dialout"))
         (system? #t)
         (comment "WebThings Gateway daemon")
         (home-directory "/var/empty") ;;"/var/run/webthings")
         (shell (file-append shadow "/sbin/nologin")))))
  
(define (webthings-shepherd-services cfg)
  (define package (webthings-configuration-package cfg))
  (define profile-dir (webthings-configuration-profile-dir cfg))
  (define extra-config-file (webthings-configuration-extra-config-file cfg))
  (define config-dir
    (computed-file
     "webthings-local-config"
     (with-imported-modules `((guix build utils)
                              (guix build json))
       #~(begin
           (use-modules (guix build utils)
                        (guix build json))
           (mkdir-p #$output)
           (call-with-output-file (string-append #$output "/local.json")
             (lambda (out)
               (write-json
                `(@ ("profileDir" . #$profile-dir))
                out)))
           #$(and extra-config-file
                  #~(let* ((extra #$extra-config-file)
                           (mtch (string-match "\\.[^.]+$" extra))
                           (ext (if mtch
                                    (match:substring mtch)
                                    "")))
                      ;; extension
                      (symlink #$extra-config-file
                               (string-append #$output "/local-guixextra" mtch))))))))
  (list
   (shepherd-service
    (documentation "WebThings Gateway server.")
    (requirement '(syslogd loopback udev)) ;; ???
    (provision '(webthings))
    ;; container?
    (start #~(make-forkexec-constructor
              (list #$(file-append package "/bin/webthings-gateway"))
              ;;#:user "webthings" ;; <--- FIXME!
              ;;#:group "webthings"
              ;;#:file-creation-mask #o026
              ;;#:directory #$(file-append package "/lib/node_modules/webthings-gateway")
              ;;#:log-file "/var/log/webthings"
              #:environment-variables
              (cons* (string-append "NODE_CONFIG_DIR=" #$config-dir)
                     #|
                     #$@(if extra-config-file
                            '("NODE_CONFIG_ENV=guixextra")
                            '())|#
                     (default-environment-variables))))
    (stop #~(make-kill-destructor)) ;; sends TERM
    (respawn? #f);;#t)
    (auto-start?
     ;; TODO: understand comment from <openssh-configuration>
     #t))))

(define (webthings-activation cfg)
  (define profile-dir (webthings-configuration-profile-dir cfg))
  (define addons (webthings-configuration-addons cfg))
  (define addons-dir
    (string-append profile-dir "/addons"))
  (with-imported-modules `((guix build utils)
                           (srfi srfi-34)
                           (srfi srfi-35))
    #~(begin
        (use-modules (guix build utils)
                     (srfi srfi-34)
                     (srfi srfi-35))
        (mkdir-p #$addons-dir)
        (for-each (lambda (dest store-pth)
                    (cond
                     ((or (not (stat dest #f))
                          (and (eq? 'symlink (stat:type (lstat dest)))
                               (store-file-name? (readlink dest))))
                      (let* ((pivot (string-append dest "-XXXXXX"))
                             (port (mkstemp! pivot)))
                        (close-port port)
                        (delete-file pivot)
                        (symlink store-pth pivot)
                        (rename-file pivot dest)))
                     (else
                      (raise (condition 
                              (&message
                               (message
                                (format #f
                                        "~a: ~a\n  addon: ~a"
                                        "webthings-service"
                                        "addon exists and is not a store link"
                                        dest))))))))
                  (list #$@(map (lambda (it)
                                  (string-append
                                   addons-dir "/" (webthings-addon-addon-name it)))
                                addons))
                  (list #$@addons))
        #;
        (mkdir-p "/var/run/webthings")
        (let* ((pw (getpwnam "webthings"))
               (uid (passwd:uid pw))
               (gid (passwd:gid pw)))
          (chown #$profile-dir uid gid)
          (chown #$addons-dir uid gid)
          #;
          (chown "/var/run/webthings" uid gid))
        #;
        (symlink #$profile-dir "/var/run/webthings/.webthings"))))

(define webthings-service-type
  (service-type
   (name 'webthings)
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        webthings-shepherd-services)
     (service-extension activation-service-type
                        webthings-activation)
     (service-extension account-service-type
                        (const %webthings-accounts))))
   (default-value
     (webthings-configuration))
   (description
    "Runs the WebThings Gateway server.")))
