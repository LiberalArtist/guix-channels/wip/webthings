;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings packages zwave)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages zwave)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (wip-webthings packages addon)
  #:use-module (wip-webthings deps-good)
  #:use-module (wip-webthings deps-bad for-zwave)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public webthings-addon-zwave-adapter
  (package
    (name "webthings-addon-zwave-adapter")
    (version "0.10.9")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/WebThingsIO/zwave-adapter")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0yxabc0xcwqsg5y9cmxlwl95xxg2cjgh4d83m1rr9v8by4s4qapf"))
       (file-name (git-file-name name version))))
    (inputs
     (list node-openzwave-shared
           node-mkdirp
           node-color
           node-serialport
           ;; devDependencies
           webthings-gateway-addon-node))
    (build-system node-build-system)
    (arguments
     `(#:modules
       ((guix build node-build-system)
        (ice-9 match)
        (guix build utils))
       #:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `("babel-eslint"
                                    "eslint"
                                    "jest"))))
         (add-before 'configure 'avoid-zwave-loader.sh
           (lambda args
             (with-atomic-json-file-replacement "manifest.json"
               (lambda (js)
                 (let loop ((js js)
                            (keys '("gateway_specific_settings"
                                    "webthings"
                                    "exec")))
                   (match keys
                     (()
                      "{nodeLoader} {path}")
                     ((this-key . keys)
                      (match js
                        (('@ . alist)
                         (cons '@ (map (match-lambda
                                         ((and (k . v) k+v)
                                          (if (equal? k this-key)
                                              (cons k (loop v keys))
                                              k+v)))
                                       alist))))))))))))
       #:tests? #t))
    (home-page "https://github.com/WebThingsIO/zwave-adapter")
    (synopsis "Z-Wave adapter plugin for WebThings Gateway")
    (description "Z-Wave adapter plugin for WebThings Gateway")
    (license license:mpl2.0)
    (properties
     `((webthings-addon
        . ,(delay (webthings-addon
                   (name "zwave-adapter")
                   (directory (file-append
                               webthings-addon-zwave-adapter
                               "/lib/node_modules/zwave-adapter")))))))))
