;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings packages addon)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (guix build-system copy)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages zwave)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (wip-webthings deps-good)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:export (webthings-addon
            make-webthings-addon
            webthings-addon?
            webthings-addon-addon-name
            webthings-addon-addon-directory
            node-ajv-for-gateway ;;  FIXME
            webthings-gateway-addon-ipc-schema
            webthings-gateway-addon-python
            webthings-gateway-addon-node))

(define-record-type* <webthings-addon>
  webthings-addon make-webthings-addon
  webthings-addon?
  (name webthings-addon-addon-name)
  (directory webthing-addon-addon-directory))

(define-gexp-compiler (webthings-addon-compiler
                       (addon <webthings-addon>) system target)
  (match addon
    (($ <webthings-addon> name directory)
     (gexp->derivation name
       (with-imported-modules `((guix build utils))
         #~(begin
             (use-modules (guix build utils)
                          (ice-9 ftw))
             (mkdir #$output)
             (chdir #$output)
             (for-each (lambda (name)
                         (symlink (string-append #$directory "/" name)
                                  name))
                       (scandir #$directory
                                (lambda (name)
                                  (not (member name '("." ".."))))))
             (define files
               (map (lambda (pth)
                      (substring pth 2))
                    (find-files
                     "."
                     #:stat
                     (lambda* (obj #:optional (exn? #t))
                       (let ((s (stat obj exn?)))
                         (if (and s (eq? 'directory (stat:type s)))
                             s
                             (lstat obj)))))))
             ;; ^ must do that before opening the output file
             (with-output-to-file "SHA256SUMS"
               (lambda ()
                 (apply invoke
                        #+(file-append coreutils "/bin/sha256sum")
                        files)))))
       #:local-build? #t))))

(define webthings-gateway-addon-ipc-schema
  (package
    (name "webthings-gateway-addon-ipc-schema")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/WebThingsIO/gateway-addon-ipc-schema")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "02j47ypqh2ws4m77kdqa0kidhd0d8r8ijs3srvzxnw4d9mv3lrca"))
       (file-name (git-file-name name version))))
    (native-inputs (list python python-jsonschema))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("."
          "share/webthings/gateway-addon-ipc-schema"
          #:include-regexp ("\\.json$")))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'check
           (lambda* (#:key native-inputs inputs tests? #:allow-other-keys)
             (if tests?
                 (invoke (search-input-file (or native-inputs inputs)
                                            "/bin/python")
                         "check-schemas.py")
                 (format #t "test suite not run~%")))))))
    (home-page "https://github.com/WebThingsIO/gateway-addon-ipc-schema")
    (synopsis
     "JSON-Schema for messages between WebThings Gateway and addons")
    (description
     "This package contains JSON-Schema definitions for all of the IPC
messages sent between the WebThings Gateway and add-ons.")
    (license license:mpl2.0)))

(define webthings-gateway-addon-python
  (package
    (name "webthings-gateway-addon-python")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/WebThingsIO/gateway-addon-python")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "0g6i9h55rvf1j3axp2kprdyrp7vh56b51srkbjisxx1py5r599aw"))
       (file-name (git-file-name name version))))
    (native-inputs
     (list webthings-gateway-addon-ipc-schema))
    (propagated-inputs
     (list python-jsonschema
           python-singleton-decorator
           python-websocket-client-0.57))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'ensure-no-mtimes-pre-1980 'unpack-schema-submodule
           (lambda* (#:key native-inputs inputs #:allow-other-keys)
             (rmdir "gateway_addon/schema")
             (symlink (search-input-directory
                       (or native-inputs inputs)
                       "share/webthings/gateway-addon-ipc-schema")
                      "gateway_addon/schema")
             (substitute* "setup.py"
               (("'git submodule init && git submodule update'")
                "'echo used Guix for schema git submodule'")))))
       ;; ???
       #:tests? #f))
    (home-page "https://github.com/WebThingsIO/gateway-addon-python")
    (synopsis "Bindings for developing Python add-ons for WebThings Gateway.")
    (description "The @code{gateway_addon} Python package handles the client
side of the Webthings Gateway Addon IPC protocol.")
    (license license:mpl2.0)))

(define node-ajv-for-gateway
  ;; This is the only obstacle to these packages being ready to go
  ;; upstream. We have it in (wip-webthings deps-good), but the
  ;; version we build doesn't actually work.
  (package
    (name "node-ajv-for-gateway")
    (version "8.8.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://registry.npmjs.org/ajv/-/ajv-8.8.2.tgz")
       (sha256
        (base32 "1w82x2a4xvcgvk46c5a6drviycaip78qz4xgkl9qnxmp0xbmhpmj"))))
    (build-system node-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'build))))
    (inputs
     (list node-uri-js node-require-from-string node-json-schema-traverse
           node-fast-deep-equal))
    (home-page "https://ajv.js.org")
    (synopsis "Another JSON Schema Validator")
    (description "Another JSON Schema Validator")
    (license license:expat)))

(define webthings-gateway-addon-node
  (package
    (name "webthings-gateway-addon-node")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/WebThingsIO/gateway-addon-node")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "1rxlxsnfmlbficf7708bmzv6wfyyp1vvkyw52qqzjy63fa8gj59k"))
       (file-name (git-file-name name version))))
    (native-inputs
     (list esbuild
           ;; NOTE: Upstream, in 1.0.1-alpha.1, the submodule refers to:
           ;;     755cd29fc992ad16b56300d581307c03c708238e
           ;; a slightly earlier commit than the one for used for
           ;; webthings-gateway-addon-python at 1.0.1-alpha.1.
           webthings-gateway-addon-ipc-schema))
    (inputs
     (list node-ajv-for-gateway node-ws node-sqlite3))
    (build-system node-build-system)
    (arguments
     `(#:modules
       ((guix build node-build-system)
        (guix build json)
        (srfi srfi-1)
        (ice-9 match)
        (ice-9 ftw)
        (guix build utils))
       #:phases
       (modify-phases %standard-phases
         (add-after 'patch-dependencies 'delete-dependencies
           (lambda args
             (delete-dependencies `(;; devDependencies
                                    "@types/node"
                                    "@types/sqlite3"
                                    "@types/ws"
                                    "@typescript-eslint/eslint-plugin"
                                    "@typescript-eslint/parser"
                                    "eslint"
                                    "eslint-config-prettier"
                                    "babel-eslint"
                                    "json-schema-to-typescript"
                                    "prettier"
                                    "typescript"))))
         (add-after 'unpack 'unpack-schema-submodule
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (rmdir "schema")
             (symlink (search-input-directory
                       (or native-inputs inputs)
                       "share/webthings/gateway-addon-ipc-schema")
                      "schema")))
         (add-before 'configure 'stub-json-schema-to-typescript
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (substitute* "generate-types.js"
               (("const [{] ?compileFromFile ?[}] = require[(]'json-schema-to-typescript'[)];")
                "const compileFromFile = async (file, opts = {}) => '';"))
             (let ((node (search-input-file (or native-inputs inputs)
                                            "/bin/node")))
               (if (file-exists? "generate-version.js")
                   (invoke node "generate-version.js")
                   (let ((version
                          (match (call-with-input-file "package.json"
                                   read-json)
                            (('@ . alist)
                             (assoc-ref alist "version")))))
                     (substitute* "src/index.ts"
                       (("import pkg from '\\./package\\.json';")
                        "")
                       (("return pkg\\.version;")
                        (format #f "return '~a';" version)))))
               (invoke node "generate-types.js"))))
         (replace 'build
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (substitute* "src/database.ts"
               (("import [{] verbose,")
                "import type {")
               (("const sqlite3 = verbose[(][)];")
                (string-append
                 "import * as sqlite3hack from 'sqlite3';\n"
                 "const sqlite3 = sqlite3hack.verbose();")))
             #;
             (substitute* "src/device.ts"
               (("import Ajv from 'ajv';")
                (string-append
                 "import * as AjvHack from 'ajv';\n"
               "const Ajv = AjvHack.default;")))
             #;
             (substitute* "src/ipc.ts"
               (("import Ajv, [{] ValidateFunction [}] from 'ajv';")
                (string-append
                 "import { ValidateFunction } from 'ajv';\n"
                 "import * as AjvHack from 'ajv';\n"
                 "const Ajv = AjvHack.default;")))
             (apply invoke
                    (search-input-file (or native-inputs inputs)
                                       "/bin/esbuild")
                    "--platform=node"
                    "--sourcemap"
                    "--format=cjs"
                    "--outdir=lib"
                    "--log-limit=0" ;; -> no limit
                    ;;"--log-level=verbose" ;; default: info
                    (find-files "src" "\\.ts$")))))))
    (home-page "https://github.com/WebThingsIO/gateway-addon-node")
    (synopsis "Bindings for developing Node add-ons for WebThings Gateway.")
    (description "The @code{gateway-addon} Node.js package handles the client
side of the Webthings Gateway Addon IPC protocol.

This should @strong{never} be included as an add-on dependency, as that will
cause breakages across gateway versions.  The gateway will provide this
dependency for every add-on.  This package only exists to facilitate building
the gateway itself.")
    (license license:mpl2.0)))

