;;; Copyright © 2021, 2022 Philip McGrath <philip@philipmcgrath.com>
;;;
;;; This file would like to be part of GNU Guix when it grows up.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (wip-webthings packages gateway)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system node)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages image)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages node-xyz)
  #:use-module (gnu packages zwave)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (wip-webthings deps-good)
  #:use-module (wip-webthings deps-bad for-gateway)
  #:use-module (wip-webthings packages addon)
  #:use-module ((wip-webthings deps-bad webpack)
                #:select (node-webpack
                          node-webpack-cli
                          node-copy-webpack-plugin
                          node-mini-css-extract-plugin
                          node-esbuild-loader
                          node-css-loader
                          #;node-html-loader
                          node-html-webpack-plugin))
  #:use-module ((wip-webthings deps-bad webpack)
                #:prefix wp-tmp:)
  #:use-module ((guix licenses)
                #:prefix license:))


;; See:
;; https://www.npmjs.com/package/config
;; https://github.com/lorenwest/node-config/wiki/Configuration-Files#config-directory
;; https://github.com/lorenwest/node-config/pull/486
;; https://github.com/lorenwest/node-config/wiki/Environment-Variables#node_config_dir
;; for more on the configuration system.

(define inputs:core
  (list bluez
        boost
        libffi
        libpng
        glib
        git
        curl
        libusb
        python
        webthings-gateway-addon-python
        webthings-gateway-addon-node))

(define inputs:dependencies
  (list node-fluent-bundle
        node-fluent-dom
        node-jest-console
        node-acme-client
        node-ajv-for-gateway
        node-archiver
        node-asn1-js
        node-bcryptjs
        node-body-parser
        node-callsites
        node-compression
        node-config
        node-country-list
        node-csv-parse
        node-express
        node-express-fileupload
        node-express-handlebars
        node-express-rate-limit
        node-express-ws
        node-find
        node-glob-to-regexp
        node-http-proxy
        node-ip-regex
        node-jsonwebtoken
        node-mkdirp
        node-ncp
        node-nocache
        node-node-fetch
        node-node-getopt
        node-promisepipe
        node-rimraf
        node-segfault-handler
        node-semver
        node-speakeasy
        node-sqlite3
        node-string-format
        node-tar
        node-tmp
        node-uuid
        node-web-push
        node-winston
        node-winston-daily-rotate-file
        node-ws))

(define inputs:devDependencies
  (list node-highlight-js
        node-page
        node-qrcode-svg
        node-shaka-player
        node-simple-oauth2
        node-mobile-drag-drop
        node-intl-pluralrules
        node-webcomponents-webcomponentsjs))

(define inputs:webpack
  (list node-webpack
        node-copy-webpack-plugin
        node-mini-css-extract-plugin
        node-html-webpack-plugin
        node-esbuild-loader
        node-css-loader
        node-webpack-cli))

(define-public inputs:all
  (append inputs:core
          inputs:dependencies
          inputs:devDependencies
          inputs:webpack))

(define-public absent-dependencies
  `("@babel/core"
    "@babel/polyfill"
    "@babel/preset-env"
    "@types/archiver"
    "@types/assert"
    "@types/bcryptjs"
    "@types/body-parser"
    "@types/chai"
    "@types/compression"
    "@types/config"
    "@types/country-list"
    "@types/event-to-promise"
    "@types/eventsource"
    "@types/express"
    "@types/express-fileupload"
    "@types/express-handlebars"
    "@types/express-rate-limit"
    "@types/express-ws"
    "@types/find"
    "@types/glob-to-regexp"
    "@types/http-proxy"
    "@types/jest"
    "@types/jsdom"
    "@types/jsonfile"
    "@types/jsonwebtoken"
    "@types/mkdirp"
    "@types/ncp"
    "@types/node"
    "@types/node-fetch"
    "@types/node-getopt"
    "@types/rimraf"
    "@types/selenium-standalone"
    "@types/semver"
    "@types/simple-oauth2"
    "@types/sinon"
    "@types/speakeasy"
    "@types/sqlite3"
    "@types/string-format"
    "@types/tar"
    "@types/tmp"
    "@types/uuid"
    "@types/web-push"
    "@types/ws"
    "@typescript-eslint/eslint-plugin"
    "@typescript-eslint/parser"
    "babel-eslint"
    "babel-loader"
    "chai"
    "chai-http"
    "clean-webpack-plugin"
    "codecov"
    "core-js"
    "eslint"
    "eslint-config-prettier"
    "eslint-plugin-html"
    "event-to-promise"
    "eventsource"
    "image-minimizer-webpack-plugin"
    "imagemin-gifsicle"
    "imagemin-jpegtran"
    "imagemin-optipng"
    "imagemin-svgo"
    "jest"
    "jsdom"
    "jsonfile"
    "jszip"
    "nock"
    "npm-run-all"
    "prettier"
    "selenium-standalone"
    "sinon"
    "stylelint"
    "stylelint-config-prettier"
    "stylelint-config-standard"
    "ts-jest"
    "ts-loader"
    "ts-node"
    "typescript"
    "webdriverio"))

(define-public webthings-gateway
  (let* ((version "1.0.0")
         (revision "1")
         (commit "6ce085adfb252f59c17db0ed70171af67d52fc93")
         (version (git-version version revision commit)))
    (package
      (name "webthings-gateway")
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/WebThingsIO/gateway")
               (commit commit)))
         (sha256
          (base32 "091h9ywd7in3i3i4nsamlsvmyw71hiyscprg1kd51388spfq80hw"))
         (patches
          (search-patches "webthings-gateway-webpack-config.patch"))
         (file-name (git-file-name name version))
         (snippet
          (with-imported-modules `((guix build utils))
            #~(begin
                (use-modules (guix build utils))
                (delete-file "pagekite.py"))))))
      (native-inputs (list esbuild))
      (inputs inputs:all)
      (build-system node-build-system)
      (arguments
       `(#:modules
         ((guix build node-build-system)
          (guix build json)
          (srfi srfi-1)
          (ice-9 regex)
          (ice-9 match)
          (ice-9 ftw)
          (guix build utils))
         #:tests? #f
         #:phases
         (modify-phases %standard-phases
           (add-after 'patch-dependencies 'delete-dependencies
             (lambda args
               (delete-dependencies ',absent-dependencies)))
           (add-before 'patch-dependencies 'patch-npm-build
             (lambda args
               (define (patch-script old)
                 (regexp-substitute
                  #f (string-match "tsc -p \\. && webpack" old)
                  'pre "echo 'Guix: skipping tsc and webpack'" 'post))
               (with-atomic-json-file-replacement "package.json"
                 (match-lambda
                   (('@ . alist)
                    `(@ ,@(map (match-lambda
                                 ;; patch the build script
                                 (("scripts" '@ . alist)
                                  `("scripts"
                                    @ ,@(map (match-lambda
                                               (("build" . script)
                                                (cons "build"
                                                      (patch-script script)))
                                               (other
                                                other))
                                             alist)))
                                 ;; workaround 6ce085a
                                 (("dependencies" '@ . alist)
                                  `("dependencies"
                                    @ ("semver" . "*") ,@alist))
                                 ;; else
                                 (other
                                  other))
                               alist)))))))
           (add-after 'patch-npm-build 'patch-command-paths
             (lambda* (#:key inputs #:allow-other-keys)
               (define node (search-input-file inputs "/bin/node"))
               (substitute* "src/plugin/plugin.ts"
                 (("nodeLoader: `node ")
                  (format #f "nodeLoader: `~a " node)))))
           (add-after 'build 'esbuild
           ;; BUG HERE
             (lambda* (#:key native-inputs inputs outputs #:allow-other-keys)
               ;; hack for node-sqlite3 esModuleInterop
               (substitute* '("src/db.ts"
                              "src/models/logs.ts")
                 (("import [{] verbose,")
                  "import type {")
                 (("const sqlite3 = verbose[(][)];")
                  (string-append
                   "import * as sqlite3hack from 'sqlite3';\n"
                   "const sqlite3 = sqlite3hack.verbose();")))
               ;; server
               (apply invoke
                      (search-input-file (or native-inputs inputs)
                                         "/bin/esbuild")
                      "--platform=node"
                      "--format=cjs"
                      "--sourcemap"
                      "--outdir=build"
                      "--log-limit=0" ;; -> no limit
                      ;;"--log-level=verbose" ;; default: info
                      (find-files "src" "\\.ts$"))
               ;; client
               (ftw "build/views"
                    (lambda (filename statinfo flag)
                      (make-file-writable filename)
                      #t))
               (substitute* "webpack.config.js"
                 (("GUIX_SUBSTITUTE_UUID_HERE")
                  (substring
                   (assoc-ref outputs "out")
                   ;; NOTE:
                   ;; Confusingly, uncommenting the following line:
                   ;;     (string-length "/gnu/store/")
                   ;; causes the derivation build to fail before
                   ;; starting, thus:
                   ;; $ guix build --keep-failed -L . webthings-gateway
                   ;; guix build: error: path `/gnu/store/' is not in the store
                   ;; ----
                   ;; so, hard-code the value:
                   11)))
               (invoke (search-input-file (or native-inputs inputs) "/bin/npx")
                       "webpack-cli")))
           (add-after 'install 'wrap-for-guix
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (module-dir
                       (string-append
                        out "/lib/node_modules/webthings-gateway"))
                      (bin (string-append out "/bin"))
                      (launcher
                       ;; used upstream for the .deb &c
                       (string-append bin "/webthings-gateway")))
                 (mkdir-p bin)
                 (with-output-to-file launcher
                   (lambda ()
                     (for-each
                      display
                      `("#!/bin/bash\n"
                        "export NODE_CONFIG_DIR=\""
                        ;; implicit default path from node-config
                        "${NODE_CONFIG_DIR:-./config}:"
                        ,(string-append module-dir "/config")
                        "\"\n"
                        "export NODE_CONFIG_ENV=\""
                        "${NODE_CONFIG_ENV:-${NODE_ENV:-development}}"
                        ",guix\"\n"
                        "exec -a $0 "
                        ,(search-input-file inputs "/bin/node")
                        " "
                        ,(string-append module-dir
                                        "/build/app.js")
                        #;" -- " ;; ????
                        ;; or options before, to pass to node?
                        " \"$@\"\n"))))
                 ;; make it executable
                 (chmod launcher (logior #o111 (stat:perms (lstat launcher))))
                 (call-with-output-file
                     (string-append module-dir "/config/guix.json")
                   (lambda (out)
                     (write-json
                      `(@ ("ssltunnel"
                           ;; because we have unbundled pagekite
                           @ ("enabled" . #f)
                             ;; #nil seems broken in gexp
                             ("pagekite_cmd" . #f)))
                      out)))
                 #t))))))
      (home-page "https://webthings.io")
      (synopsis "Software distribution for smart home gateways")
      (description "WebThings Gateway is a software distribution for
smart home gateways focused on privacy, security, and
interoperability.  It enables users to directly monitor and control
their smart home over the web, without a middleman.

It provides a web-based user interface to monitor and control smart
home devices, a rules engine to automate them, and an add-ons system to
extend the gateway with support for a wide range of existing smart
home devices.")
      (license license:mpl2.0))))
